from pathlib import Path
import pickle
import sqlite3

import numpy as np
import pandas as pd

from miditime.miditime import MIDITime

dataset_name = "dev_numu_train_l2_2020_02"
transformers_path = (
    Path()
    .home()
    .joinpath("work")
    .joinpath("datasets")
    .joinpath(dataset_name)
    .joinpath("meta")
    .joinpath("transformers.pkl")
)
sql_db = Path().home().joinpath("data").joinpath(dataset_name + ".db")

query = "select * from features where event_no = 1"
with sqlite3.connect(sql_db) as con:
    data = pd.read_sql(query, con)

with open(transformers_path, "rb") as f:
    transformers = pickle.load(f)

for key, value in transformers["features"].items():
    if key in data.columns.values:
        data[key] = value.inverse_transform(data[key].values.reshape(-1, 1))

data["dom_charge"] = np.power(10, data["dom_charge"].values)
data["round_time"] = data["dom_time"].apply(lambda x: int(x / 1000))
data["round_charge"] = data["dom_charge"].apply(lambda x: int(x * 30))

print(data)

# Instantiate the class with a tempo (120bpm is the default) and an output file destination.
mymidi = MIDITime(120, "/mnt/c/Users/MadsEhrhorn/Downloads/myfile.mid")


# Create a list of notes. Each note is a list: [time, pitch, attack, duration]
midinotes = [
    [data["round_time"].values[i], data["round_charge"].values[i], 200, 1]
    for i in range(len(data))
]
# midinotes = [
#     [0, 60, 200, 3],  # At 0 beats (the start), Middle C with attack 200, for 3 beats
#     [
#         10,
#         61,
#         200,
#         4,
#     ],  # At 10 beats (12 seconds from start), C#5 with attack 200, for 4 beats
# ]

# Add a track with those notes
mymidi.add_track(midinotes)

# Output the .mid file
mymidi.save_midi()