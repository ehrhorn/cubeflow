import math
import pickle
import sqlite3

import numpy as np
from tensorflow import keras


class DataGenerator(keras.utils.Sequence):
    def __init__(
        self,
        params,
        paths,
        list_ids,
        batch_size,
        to_numpy,
        shuffle=True,
        scramble=False,
    ):
        self.paths = paths
        self.list_ids = list_ids
        self.features = params["features"]
        self.targets = params["targets"]
        self.time_idx = params["features"].index("time")
        self.pad_type = params["pad_type"]
        self.max_length = params["max_length"]
        self.weight_name = params["weight_name"]
        self.cleaning = params["cleaning"]
        self.pmt_types = params["pmt_types"]
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.scramble = scramble
        if self.weight_name:
            weight_path = paths["meta"] / f"{self.weight_name}.pkl"
            with open(str(weight_path), "rb") as f:
                self.weights = pickle.load(f)
        else:
            self.weights = None
        if to_numpy:
            self.db = self.paths["fast_db"]
        else:
            self.db = self.paths["fast_db"]
        self.on_epoch_end()

    def __len__(self):
        return math.ceil(len(self.list_ids) / self.batch_size)

    def __getitem__(self, index):
        indexes = self.indexes[index * self.batch_size : (index + 1) * self.batch_size]
        list_ids_batch = [self.list_ids[k] for k in indexes]
        X, y, weights = self.__data_generation(list_ids_batch)
        return X, y, weights

    def on_epoch_end(self):
        self.indexes = np.arange(len(self.list_ids))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def _coerce_batch(self, in_X):
        in_X = in_X[in_X[:, self.time_idx].argsort()]
        out_X = np.zeros((self.max_length, in_X.shape[1]))
        event_length = in_X.shape[0]
        if self.pad_type == "middle":
            insert_point = int((self.max_length - event_length) // 2)
        elif self.pad_type == "end":
            insert_point = 0
        if event_length > self.max_length:
            random_idx = np.sort(
                np.random.choice(
                    np.arange(event_length), self.max_length, replace=False
                )
            )
            out_X = in_X[random_idx, :]
            out_X = out_X[out_X[:, self.time_idx].argsort()]
            if self.scramble:
                np.random.shuffle(out_X[:, self.scramble])
        else:
            if self.scramble:
                np.random.shuffle(in_X[:, self.scramble])
            out_X[insert_point : insert_point + event_length, :] = in_X
        return out_X

    def __data_generation(self, list_ids_batch):
        X = np.zeros((len(list_ids_batch), self.max_length, len(self.features)))
        y = np.zeros((len(list_ids_batch), len(self.targets)))
        weights = np.zeros(len(list_ids_batch))
        features, targets, weights = self._retrieve_from_sqlite(
            str(self.db),
            list_ids_batch,
            self.features,
            self.targets,
            self.weight_name,
            self.cleaning,
        )
        for i, list_id in enumerate(list_ids_batch):
            in_X = features[features[:, 0] == list_id][:, 1::]
            in_y = targets[targets[:, 0] == list_id][:, 1::]
            X[i, :, :] = self._coerce_batch(in_X)
            y[i, :] = in_y
        return X, y, weights

    def _retrieve_from_sqlite(
        self, db, event_ids, feature_list, target_list, weight_name, cleaning
    ):
        features_query = "select {} from features where event_no in ({}) and {} = 1 and pmt_type in ({})".format(
            ", ".join(["event_no"] + [x for x in feature_list]),
            ", ".join([str(event_id) for event_id in event_ids]),
            cleaning,
            ", ".join([str(pmt_type) for pmt_type in self.pmt_types]),
        )
        targets_query = "select {} from truth where event_no in ({})".format(
            ", ".join(["event_no"] + [x for x in target_list]),
            ", ".join([str(event_id) for event_id in event_ids]),
        )
        with sqlite3.connect("file:" + str(db) + "?mode=ro", uri=True) as con:
            cur = con.cursor()
            cur.execute(features_query)
            features = np.array(cur.fetchall())
            cur.execute(targets_query)
            targets = np.array(cur.fetchall())
        if self.weights is not None:
            idxs = np.searchsorted(self.weights[:, 0], event_ids)
            weights = self.weights[idxs, 1]
        else:
            weights = np.array([1] * len(event_ids))
        return features, targets, weights
