import math
import sqlite3

import numpy as np
from tensorflow import keras


class DataGenerator(keras.utils.Sequence):
    def __init__(self, params, paths, set_dict, batch_size, to_numpy, shuffle=True):
        self.paths = paths
        self.set_dict = set_dict
        self.list_ids = list(self.set_dict.keys())
        self.features = params["features"]
        self.targets = params["targets"]
        self.pad_type = params["pad_type"]
        self.max_length = params["max_length"]
        self.weight_name = params["weight_name"]
        self.cleaning = params["cleaning"]
        self.batch_size = batch_size
        self.shuffle = shuffle
        if to_numpy:
            self.db = self.paths["fast_db"]
        else:
            self.db = self.paths["fast_db"]
        self.on_epoch_end()

    def __len__(self):
        return math.ceil(len(self.list_ids) / self.batch_size)

    def __getitem__(self, index):
        indexes = self.indexes[index * self.batch_size : (index + 1) * self.batch_size]
        list_ids_batch = [self.list_ids[k] for k in indexes]
        features = self.__data_generation(list_ids_batch)
        return features

    def on_epoch_end(self):
        self.indexes = np.arange(len(self.list_ids))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_ids_batch):
        rows = []
        for event in list_ids_batch:
            rows.extend(self.set_dict[event])
        features = self._retrieve_from_sqlite(
            str(self.db),
            list_ids_batch,
            rows,
            self.features,
            self.targets,
            self.weight_name,
            self.cleaning,
        )
        return features

    def _retrieve_from_sqlite(
        self, db, event_ids, rows, feature_list, target_list, weight_name, cleaning
    ):
        features_query = "select {} from features where row in ({})".format(
            ", ".join(["event_no"] + [x for x in feature_list]),
            ", ".join([str(row) for row in rows]),
        )
        with sqlite3.connect(str(db)) as con:
            cur = con.cursor()
            cur.execute(features_query)
            features = cur.fetchall()
        return features
