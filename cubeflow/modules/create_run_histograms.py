from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np

import cubeflow.modules.helper_functions as helper_functions


def create_run_histograms(dataset_name: str, run_name: str, run_type: str):
    now = datetime.now().strftime("%H:%M:%S")
    print("{}: Calculating histograms for {}".format(now, run_name))
    if run_name == "reconstruction":
        save_run_name = dataset_name + "." + run_type + ".reconstruction"
    else:
        save_run_name = run_name
    if run_name == "reconstruction":
        pickle_path = (
            Path()
            .home()
            .joinpath("work")
            .joinpath("runs")
            .joinpath("reconstructions")
            .joinpath(
                "reconstruction."
                + run_type
                + ".None.None.None.None.None.None.None.None.reconstruction"
            )
        )
        pickle_path.mkdir(exist_ok=True)
        pickle_file = pickle_path.joinpath(
            "reconstruction."
            + run_type
            + ".None.None.None.None.None.None.None.None.reconstruction_histograms.pkl"
        )
    else:
        pickle_file = (
            Path()
            .home()
            .joinpath("work")
            .joinpath("runs")
            .joinpath("done")
            .joinpath(run_name)
            .joinpath(save_run_name + "_histograms.pkl")
        )
    if pickle_file.is_file():
        hist_dict = helper_functions.open_pickle_file(pickle_file)
    else:
        hist_dict = {}
    if run_type == "energy":
        hist_dict[dataset_name] = {}
        hist_dict[dataset_name] = create_energy_histograms(dataset_name, run_name, 100)
    elif run_type == "direction" or run_type == "angle" or run_type == "zenith":
        hist_dict[dataset_name] = {}
        hist_dict[dataset_name] = create_angular_histograms(
            dataset_name, run_name, run_type, 100
        )
    now = datetime.now().strftime("%H:%M:%S")
    print("{}: Calculated histograms for {}".format(now, run_name))
    helper_functions.save_pickle_file(pickle_file, hist_dict)


def create_energy_histograms(
    dataset_name: str, run_name: str, n_bootstraps: int = 1000
) -> dict:
    paths = helper_functions.get_dataset_paths(dataset_name)
    hist_dict = {}
    hist_dict["energy_log10"] = {}
    hist_dict["energy_log10"][
        "resolution_1d_histogram"
    ] = helper_functions.histogram_resolution(
        paths["errors"],
        paths["predictions"],
        "energy_log10",
        "energy_log10",
        run_name,
        "truth",
        hist_range=([-2, 2], [0, 4]),
        n_bootstraps=n_bootstraps,
    )
    hist_dict["energy_log10"][
        "prediction_1d_histogram"
    ] = helper_functions.histogram_prediction(
        paths["predictions"],
        paths["predictions"],
        "energy_log10",
        "energy_log10",
        run_name,
        "truth",
        hist_range=([0, 4], [0, 4]),
        n_bootstraps=n_bootstraps,
    )
    hist_dict["energy_log10"]["error_2d_histogram"] = helper_functions.histogram_2d(
        paths["errors"],
        paths["predictions"],
        "energy_log10",
        "energy_log10",
        run_name,
        "truth",
        hist_range=([-2, 2], [0, 4]),
        n_bootstraps=n_bootstraps,
        transpose=True,
    )
    hist_dict["energy_log10"][
        "prediction_2d_histogram"
    ] = helper_functions.histogram_2d(
        paths["predictions"],
        paths["predictions"],
        "energy_log10",
        "energy_log10",
        run_name,
        "truth",
        hist_range=([0, 4], [0, 4]),
        n_bootstraps=n_bootstraps,
        transpose=False,
    )
    return hist_dict


def create_angular_histograms(
    dataset_name: str,
    run_name: str,
    run_type: str,
    n_bootstraps: int = 1000,
    energy_range: list = [0, 4],
) -> dict:
    paths = helper_functions.get_dataset_paths(dataset_name)
    hist_dict = {}
    if run_type == "direction" or run_type == "angle":
        metrics = ["azimuth", "zenith"]
        error_ranges = [[0, 180], [-180, 180]]
        prediction_ranges = [[0, 360], [0, 180]]
    elif run_type == "zenith":
        metrics = ["zenith"]
        error_ranges = [[-180, 180]]
        prediction_ranges = [[0, 180]]
    for metric, error_range, prediction_range in zip(
        metrics, error_ranges, prediction_ranges
    ):
        hist_dict[metric] = {}
        hist_dict[metric][
            "resolution_1d_histogram"
        ] = helper_functions.histogram_resolution(
            paths["errors"],
            paths["predictions"],
            metric,
            "energy_log10",
            run_name,
            "truth",
            hist_range=(error_range, energy_range),
            n_bootstraps=n_bootstraps,
        )
        hist_dict[metric][
            "prediction_1d_histogram"
        ] = helper_functions.histogram_prediction(
            paths["predictions"],
            paths["predictions"],
            metric,
            metric,
            run_name,
            "truth",
            hist_range=(prediction_range, prediction_range),
            n_bootstraps=n_bootstraps,
        )
        hist_dict[metric]["error_2d_histogram"] = helper_functions.histogram_2d(
            paths["errors"],
            paths["predictions"],
            metric,
            "energy_log10",
            run_name,
            "truth",
            hist_range=(error_range, energy_range),
            n_bootstraps=n_bootstraps,
            transpose=True,
        )
        hist_dict[metric]["prediction_2d_histogram"] = helper_functions.histogram_2d(
            paths["predictions"],
            paths["predictions"],
            metric,
            metric,
            run_name,
            "truth",
            hist_range=(prediction_range, prediction_range),
            n_bootstraps=n_bootstraps,
            transpose=False,
        )

    hist_dict["angle"] = {}
    hist_dict["angle"]["error_2d_histogram"] = helper_functions.histogram_2d(
        paths["errors"],
        paths["predictions"],
        metric,
        "energy_log10",
        run_name,
        "truth",
        hist_range=([-1, 1], energy_range),
        n_bootstraps=n_bootstraps,
        transpose=True,
    )
    return hist_dict
