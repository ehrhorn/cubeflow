def get_binning_label(selected_binning):
    if selected_binning == "energy_log10_truth":
        label = "$\\log_{10}(E_{\\text{true}}) \\ \\left(E / \\si{\\giga\\electronvolt} \\right)$"
    elif selected_binning == "azimuth_truth":
        label = "$\\phi_{\\text{truth}} \\ \\left( \\si{\\degree} \\right)$"
    elif selected_binning == "zenith_truth":
        label = "$\\theta_{\\text{truth}} \\ \\left( \\si{\\degree} \\right)$"
    elif selected_binning == "split_in_ice_pulses_event_length":
        label = "Split in ice pulses event length"
    elif selected_binning == "srt_in_ice_pulses_event_length":
        label = "SRT in ice pulses event length"
    elif selected_binning == "time_truth":
        label = "$t_{\\text{true}} \\ \\left( \\si{\\nano\\second} \\right)$"
    else:
        label = "NO LABEL SET"
    return label


def get_prediction_label(metric_name):
    if metric_name == "energy_log10":
        label = "$\\log_{10}(E_{\\text{reco}}) \\ \\left(E / \\si{\\giga\\electronvolt} \\right)$"
    elif metric_name == "azimuth":
        label = "$\\phi_{\\text{reco}} \\ \\left( \\si{\\degree} \\right)$"
    elif metric_name == "zenith":
        label = "$\\theta_{\\text{reco}} \\ \\left( \\si{\\degree} \\right)$"
    else:
        label = "UNKOWN PREDICTION"
    return label


def get_column_label(column, plotly):
    if column == "dom_string":
        label = "String no."
    elif column == "dom_pmt":
        label = "PMT no."
    elif column == "dom_om":
        label = "OM no."
    elif column == "dom_x":
        if plotly:
            label = "DOM x coordinate (m)"
        else:
            label = "DOM  $ x $-coordinate (m)"
    elif column == "dom_y":
        if plotly:
            label = "DOM y coordinate (m)"
        else:
            label = "DOM $ y $-coordinate (m)"
    elif column == "dom_z":
        if plotly:
            label = "DOM z coordinate (m)"
        else:
            label = "DOM $ z $-coordinate (m)"
    elif column == "dom_time":
        label = "Pulse time (ns)"
    elif column == "dom_charge":
        label = "Pulse charge (Photoelectrons)"
    elif column == "dom_lc":
        label = "DOM local coincidence"
    elif column == "dom_atwd":
        label = "ATWD DOM"
    elif column == "dom_fadc":
        label = "fADC DOM"
    elif column == "dom_pulse_width":
        label = "DOM pulse width"
    elif column == "SplitInIcePulses":
        label = "Event length (SplitInIcePulses)"
    elif column == "SRTInIcePulses":
        label = "Event length (SRTInIcePulses)"
    elif column == "energy_log10":
        label = "$\\log_{10}(E_{\\text{truth}}) \\ \\left(E / \\si{\\giga\\electronvolt} \\right)$"
    elif column == "azimuth":
        label = "$\\phi_{\\text{truth}} \\ \\left( \\si{\\degree} \\right)$"
    elif column == "zenith":
        label = "$\\theta_{\\text{truth}} \\ \\left( \\si{\\degree} \\right)$"
    else:
        label = "UNKOWN PREDICTION"
    return label


def get_metric_label(selected_metric):
    if selected_metric == "energy_log10":
        label = "$\\log_{10}\\left( E_{\\text{reco}} / E_{\\text{true}} \\right) \\ \\left(E / \\si{\\giga\\electronvolt} \\right)$"
    elif selected_metric == "time":
        label = "$t_{\\text{reco}} - t_{\\text{truth}} \\ \\left( \\si{\\nano\\second} \\right)$"
    elif selected_metric == "position_x":
        label = "$\\bm{p}_{x, \\text{reco}} - \\bm{p}_{x, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "position_y":
        label = "$\\bm{p}_{y, \\text{reco}} - \\bm{p}_{y, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "position_z":
        label = "$\\bm{p}_{z, \\text{reco}} - \\bm{p}_{z, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "direction_x":
        label = "$\\bm{r}_{x, \\text{reco}} - \\bm{r}_{x, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "direction_y":
        label = "$\\bm{r}_{y, \\text{reco}} - \\bm{r}_{y, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "direction_z":
        label = "$\\bm{r}_{z, \\text{reco}} - \\bm{r}_{z, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "azimuth":
        label = "$\\phi_{\\text{reco}} - \\phi_{\\text{truth}} \\ \\left( \\si{\\degree} \\right)$"
    elif selected_metric == "zenith":
        label = "$\\theta_{\\text{reco}} - \\theta_{\\text{truth}} \\ \\left( \\si{\\degree} \\right)$"
    else:
        label = "NO LABEL SET"
    return label
