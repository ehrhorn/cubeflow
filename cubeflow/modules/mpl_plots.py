import copy

from astropy.coordinates import SkyCoord
import healpy as hp
from matplotlib import cm
from matplotlib.lines import Line2D
from matplotlib.offsetbox import AnchoredText
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import stats


def convert_data_for_step(bin_edges, hist):
    widths = bin_edges[1:] - bin_edges[:-1]
    bin_edges = np.append(bin_edges, (bin_edges[-1] + widths[-1]))
    hist = np.insert(hist, 0, 0)
    hist = np.append(hist, 0)
    return bin_edges, hist


def calculate_centers_and_widths(bin_edges):
    widths = bin_edges[1:] - bin_edges[:-1]
    centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    return centers, widths


def get_unit(selected_metric):
    if "energy" in selected_metric:
        unit = "$\\left(E / \\si{\\giga\\electronvolt} \\right)$"
    elif "time" in selected_metric:
        unit = "$\\left( \\si{\\nano\\second} \\right)$"
    elif "azimuth" in selected_metric or "zenith" in selected_metric:
        unit = "$\\left( \\si{\\degree} \\right)$"
    return unit


def get_binning_label(selected_binning):
    if selected_binning == "energy_log10_truth":
        label = "$\\log_{10}(E_{\\text{true}}) \\ \\left(E / \\si{\\giga\\electronvolt} \\right)$"
    elif selected_binning == "azimuth_truth":
        label = "$\\phi_{\\text{truth}} \\ \\left( \\si{\\degree} \\right)$"
    elif selected_binning == "zenith_truth":
        label = "$\\theta_{\\text{truth}} \\ \\left( \\si{\\degree} \\right)$"
    elif selected_binning == "split_in_ice_pulses_event_length":
        label = "Split in ice pulses event length"
    elif selected_binning == "srt_in_ice_pulses_event_length":
        label = "SRT in ice pulses event length"
    elif selected_binning == "time_truth":
        label = "$t_{\\text{true}} \\ \\left( \\si{\\nano\\second} \\right)$"
    else:
        label = "NO LABEL SET"
    return label


def get_prediction_label(metric_name):
    if metric_name == "energy_log10":
        label = "$\\log_{10}(E_{\\text{reco}}) \\ \\left(E / \\si{\\giga\\electronvolt} \\right)$"
    elif metric_name == "azimuth":
        label = "$\\phi_{\\text{reco}} \\ \\left( \\si{\\degree} \\right)$"
    elif metric_name == "zenith":
        label = "$\\theta_{\\text{reco}} \\ \\left( \\si{\\degree} \\right)$"
    else:
        label = "UNKOWN PREDICTION"
    return label


def get_truth_label(metric_name):
    if metric_name == "azimuth":
        label = "$\\phi_{\\text{truth}} \\ \\left( \\si{\\degree} \\right)$"
    elif metric_name == "zenith":
        label = "$\\theta_{\\text{truth}} \\ \\left( \\si{\\degree} \\right)$"
    elif metric_name == "energy_log10":
        label = "$\\log_{10}(E_{\\text{truth}}) \\ \\left(E / \\si{\\giga\\electronvolt} \\right)$"
    else:
        label = "UNKOWN PREDICTION"
    return label


def get_metric_label(selected_metric):
    if selected_metric == "energy_log10":
        label = "$\\log_{10}\\left( E_{\\text{reco}} / E_{\\text{true}} \\right) \\ \\left(E / \\si{\\giga\\electronvolt} \\right)$"
    elif selected_metric == "time":
        label = "$t_{\\text{reco}} - t_{\\text{truth}} \\ \\left( \\si{\\nano\\second} \\right)$"
    elif selected_metric == "position_x":
        label = "$\\bm{p}_{x, \\text{reco}} - \\bm{p}_{x, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "position_y":
        label = "$\\bm{p}_{y, \\text{reco}} - \\bm{p}_{y, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "position_z":
        label = "$\\bm{p}_{z, \\text{reco}} - \\bm{p}_{z, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "direction_x":
        label = "$\\bm{r}_{x, \\text{reco}} - \\bm{r}_{x, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "direction_y":
        label = "$\\bm{r}_{y, \\text{reco}} - \\bm{r}_{y, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "direction_z":
        label = "$\\bm{r}_{z, \\text{reco}} - \\bm{r}_{z, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "azimuth":
        label = "$\\phi_{\\text{reco}} - \\phi_{\\text{truth}} \\ \\left( \\si{\\degree} \\right)$"
    elif selected_metric == "zenith":
        label = "$\\theta_{\\text{reco}} - \\theta_{\\text{truth}} \\ \\left( \\si{\\degree} \\right)$"
    else:
        label = "NO LABEL SET"
    return label


def calculate_bins_and_quantiles(
    selected_run_data,
    selected_comparison_data,
    selected_binning,
    selected_metric,
    bins,
    quantiles,
    x_clips,
):
    selected_run_grouping = selected_run_data.groupby(
        pd.cut(
            x=np.clip(
                selected_run_data[selected_binning].values, x_clips[0], x_clips[1]
            ),
            bins=bins,
        )
    ).quantile(q=quantiles)
    selected_comparison_grouping = selected_comparison_data.groupby(
        pd.cut(
            x=np.clip(
                selected_comparison_data[selected_binning].values,
                x_clips[0],
                x_clips[1],
            ),
            bins=bins,
        )
    ).quantile(q=quantiles)
    return selected_run_grouping, selected_comparison_grouping


def calculate_2d_histograms(
    selected_run_data,
    selected_comparison_data,
    selected_metric,
    selected_binning,
    percentiles,
):
    merged_selected_data = selected_run_data.append(selected_comparison_data)
    y_clip_min = np.percentile(
        merged_selected_data[selected_metric].values, percentiles[0]
    )
    y_clip_max = np.percentile(merged_selected_data[selected_metric], percentiles[1])
    x_clip_min = np.percentile(
        merged_selected_data[selected_binning].values, percentiles[0]
    )
    x_clip_max = np.percentile(merged_selected_data[selected_binning], percentiles[1])
    _, x_bins = np.histogram(
        np.clip(merged_selected_data[selected_binning].values, x_clip_min, x_clip_max),
        bins="auto",
    )
    _, y_bins = np.histogram(
        np.clip(
            merged_selected_data[selected_metric].values,
            y_clip_min,
            y_clip_max,
        ),
        bins="auto",
    )
    H1, _, _ = np.histogram2d(
        x=selected_run_data[selected_binning].values,
        y=np.clip(selected_run_data[selected_metric].values, y_clip_min, y_clip_max),
        bins=[x_bins, y_bins],
        density=True,
    )
    H2, _, _ = np.histogram2d(
        x=selected_comparison_data[selected_binning].values,
        y=np.clip(
            selected_comparison_data[selected_metric].values, y_clip_min, y_clip_max
        ),
        bins=[x_bins, y_bins],
        density=True,
    )
    H_max = np.amax(np.array(np.amax(H1), np.amax(H2)))
    return [x_bins, y_bins], [x_clip_min, x_clip_max], [y_clip_min, y_clip_max], H_max


def error_2d_histogram(
    run_data,
    comparison_data,
    run_name,
    comparison_name,
    metric_name,
):
    run_data = run_data[metric_name]["error_2d_histogram"]
    comparison_data = comparison_data[metric_name]["error_2d_histogram"]

    H_max = np.amax(
        np.array(np.amax(run_data["hist"]), np.amax(comparison_data["hist"]))
    )
    x = run_data["bin_edges"][1]
    y = run_data["bin_edges"][0]
    xlim = [min(x), max(x)]
    ylim = [min(y), max(y)]

    fig, axes = plt.subplots(nrows=1, ncols=2, sharex=True, sharey=True)
    fig.set_size_inches(w=5.78853, h=4)

    for i, (ax, data, name) in enumerate(
        zip(axes, [run_data, comparison_data], [run_name, comparison_name])
    ):
        H = ax.pcolormesh(
            x,
            y,
            data["hist"],
            vmin=0,
            vmax=H_max,
            cmap="Oranges",
        )
        ax.set(xlim=xlim, ylim=ylim)
        if i == 0:
            ylabel = get_metric_label(metric_name)
        else:
            ylabel = None
        ax.set(
            title=name.replace("reconstruction", "Retro Reco"),
            xlabel=get_binning_label("energy_log10_truth"),
            ylabel=ylabel,
        )
        anchored_text = AnchoredText(
            "Events: {:,d}".format(int(data["events"])).replace(",", " "),
            loc="upper right",
        )
        ax.add_artist(anchored_text)
    for ax in axes:
        for data, color in zip(
            [run_data, comparison_data],
            [
                plt.rcParams["axes.prop_cycle"].by_key()["color"][0],
                plt.rcParams["axes.prop_cycle"].by_key()["color"][1],
            ],
        ):
            for percentile, linestyle in zip(
                ["lower", "mid", "upper"], ["dashed", "solid", "dashdot"]
            ):
                ax.plot(
                    data["percentile_bin_mids"],
                    data[percentile + "_percentiles"],
                    color=color,
                    marker="",
                    linestyle=linestyle,
                )
                # ax.fill_between(
                #     data["percentile_bin_mids"],
                #     data[percentile + "_percentiles"]
                #     - data[percentile + "_percentile_errors"],
                #     data[percentile + "_percentiles"]
                #     + data[percentile + "_percentile_errors"],
                #     color=color,
                #     alpha=0.2,
                # )
                ax.axhline(
                    y=0,
                    c=plt.rcParams["axes.prop_cycle"].by_key()["color"][2],
                    linestyle="dotted",
                )
    cbar = fig.colorbar(H, ax=axes[0:2], shrink=1.0, location="bottom", pad=0.2)
    cbar.set_label("Counts")

    for i, color in enumerate(
        [
            plt.rcParams["axes.prop_cycle"].by_key()["color"][0],
            plt.rcParams["axes.prop_cycle"].by_key()["color"][1],
        ]
    ):
        custom_lines = [
            Line2D([0], [0], color=color, linestyle="dashed"),
            Line2D([0], [0], color=color, linestyle="solid"),
            Line2D([0], [0], color=color, linestyle="dashdot"),
        ]
        axes[i].legend(
            custom_lines,
            ["16th\%", "50\%", "84\%"],
            loc="lower center",
            bbox_to_anchor=(0.5, 0.0),
            ncol=3,
            fancybox=True,
            fontsize=6,
        )
    return fig


def resolution_1d_histogram(
    run_data, comparison_data, run_name, comparison_name, metric_name, title
):
    run_data = run_data[metric_name]["resolution_1d_histogram"]
    comparison_data = comparison_data[metric_name]["resolution_1d_histogram"]
    xlabel = get_binning_label("energy_log10_truth")
    ylabel = "Resolution" + " " + get_unit(metric_name)

    run_means = run_data["resolution"]
    comparison_means = comparison_data["resolution"]
    rel_imp = (np.array(comparison_means) - np.array(run_means)) / np.array(
        comparison_means
    )

    fig, (reso_ax, ratio_ax) = plt.subplots(
        2, 1, sharex=True, gridspec_kw={"height_ratios": [3, 1], "hspace": 0.05}
    )
    fig.set_size_inches(w=5.78853, h=4)
    hist_ax = reso_ax.twinx()

    # bin_edges, hist = convert_data_for_step(run_data["bin_edges"], run_data["hist"])
    bin_edges, hist = run_data["bin_edges"], run_data["hist"]
    hist_ax.step(
        x=bin_edges,
        y=hist,
        where="pre",
        marker="",
        color="grey",
        alpha=0.4,
        label="Train data",
    )

    for data, name in zip([run_data, comparison_data], [run_name, comparison_name]):
        bin_mids, bin_widths = calculate_centers_and_widths(data["bin_edges"])
        bin_means = data["resolution"]
        bin_stds = data["resolution_sigma"]
        reso_ax.errorbar(
            x=bin_mids,
            y=bin_means,
            xerr=bin_widths / 2,
            yerr=bin_stds,
            ls="none",
            label=name,
            alpha=0.8,
        )
    color = plt.rcParams["axes.prop_cycle"].by_key()["color"][2]
    marker = plt.rcParams["axes.prop_cycle"].by_key()["marker"][2]
    ratio_ax.errorbar(
        x=bin_mids,
        y=rel_imp,
        xerr=bin_widths / 2,
        ls="none",
        label="Rel. imp.",
        alpha=0.8,
        color="#50514F",
        marker=marker,
    )

    reso_ax.set(xlim=[0, 3])
    ratio_ax.axhline(y=0, linestyle="dashed", linewidth=0.5, color="black")
    reso_ax.xaxis.set_ticks_position("none")
    reso_ax.set_ylim(bottom=0)
    reso_ax.set(ylabel=ylabel)
    hist_ax.set_yscale("log")
    hist_ax.set(ylabel="Events")
    hist_ax.set_ylim(bottom=5)
    ratio_ax.set(xlabel=xlabel, ylabel="Rel. imp.")
    reso_lines, reso_labels = reso_ax.get_legend_handles_labels()
    hist_lines, hist_labels = hist_ax.get_legend_handles_labels()
    ratio_ines, ration_labels = ratio_ax.get_legend_handles_labels()
    plt.legend(
        reso_lines + hist_lines + ratio_ines,
        reso_labels + hist_labels + ration_labels,
        bbox_to_anchor=(0, 1.02, 1, 0.2),
        loc="lower left",
        mode="expand",
        borderaxespad=0,
        ncol=4,
    )
    anchored_text = AnchoredText(
        "Events: {:,d}".format(int(np.sum(hist))).replace(",", " "),
        loc="lower left",
    )
    hist_ax.add_artist(anchored_text)
    reso_ax.yaxis.get_major_ticks()[0].label1.set_visible(False)
    fig.align_ylabels([reso_ax, ratio_ax])
    fig.suptitle(title)
    return fig


def prediction_2d_histogram(
    run_data,
    comparison_data,
    run_name,
    comparison_name,
    metric_name,
):
    run_data = run_data[metric_name]["prediction_2d_histogram"]
    comparison_data = comparison_data[metric_name]["prediction_2d_histogram"]

    x = run_data["bin_edges"][1]
    y = run_data["bin_edges"][0]
    xs = np.linspace(min(x), max(x), 100)
    ys = np.linspace(min(y), max(y), 100)

    H_max = np.amax(
        np.array(np.amax(run_data["hist"]), np.amax(comparison_data["hist"]))
    )
    xlim = [min(x), max(x)]
    ylim = [min(y), max(y)]

    fig, axes = plt.subplots(nrows=1, ncols=2, sharex=True, sharey=True)
    fig.set_size_inches(w=5.78853, h=4)

    for i, (ax, data, name) in enumerate(
        zip(axes, [run_data, comparison_data], [run_name, comparison_name])
    ):
        C = np.ma.masked_where(data["hist"] == 0, data["hist"])
        err_minus = abs(data["mid_percentiles"]) - abs(data["lower_percentiles"])
        err_plus = abs(data["upper_percentiles"]) - abs(data["mid_percentiles"])
        xerr = np.concatenate((err_minus, err_plus), axis=0).reshape(2, -1)
        cmap = copy.copy(cm.get_cmap("viridis"))
        H = ax.pcolormesh(x, y, C, vmin=0, vmax=H_max, cmap=cmap)
        ax.plot(xs, ys, marker="", color="r", linestyle="dashed")
        ax.errorbar(
            data["mid_percentiles"],
            data["percentile_bin_mids"],
            xerr=xerr,
            ls="none",
            color="black",
            marker=".",
            capsize=1,
        )
        ax.set(xlim=xlim, ylim=ylim)
        if i == 0:
            ylabel = get_truth_label(metric_name)
        else:
            ylabel = None
        ax.set(
            title=name.replace("reconstruction", "Retro Reco"),
            xlabel=get_prediction_label(metric_name),
            ylabel=ylabel,
        )
        H.cmap.set_bad("white")
        anchored_text = AnchoredText(
            "Events: {:,d}".format(int(data["events"])).replace(",", " "),
            loc="lower right",
        )
        ax.add_artist(anchored_text)

    cbar = fig.colorbar(H, ax=axes[0:2], shrink=1.0, location="bottom", pad=0.2)
    cbar.set_label("Counts")

    return fig


def angular_2d_histogram(
    run_data,
    comparison_data,
    run_name,
    comparison_name,
    metric_name,
):
    run_data = run_data["angular_2d_histogram"]
    comparison_data = comparison_data["angular_2d_histogram"]

    x = run_data["y_bin_edges"]
    y = run_data["x_bin_edges"]

    H_max = np.amax(np.array(np.amax(run_data["H"]), np.amax(comparison_data["H"])))
    xlim = [min(run_data["y_bin_edges"]), max(run_data["y_bin_edges"])]
    ylim = [min(run_data["x_bin_edges"]), max(run_data["x_bin_edges"])]

    fig, axes = plt.subplots(nrows=1, ncols=2, sharex=True, sharey=True)
    fig.set_size_inches(w=5.78853, h=4)

    for i, (ax, data, name) in enumerate(
        zip(axes, [run_data, comparison_data], [run_name, comparison_name])
    ):
        C = np.ma.masked_where(data["H"] == 0, data["H"])
        cmap = copy.copy(cm.get_cmap("viridis"))
        H = ax.pcolormesh(x, y, C, vmin=0, vmax=H_max, cmap=cmap)
        ax.set(xlim=xlim, ylim=ylim)
        if i == 0:
            ylabel = get_prediction_label("azimuth")
        else:
            ylabel = None
        ax.set(
            title=name.replace("reconstruction", "Retro Reco"),
            xlabel=get_prediction_label("zenith"),
            ylabel=ylabel,
        )
        H.cmap.set_bad("white")
        anchored_text = AnchoredText(
            "Events: {:,d}".format(int(np.sum(data["H"]))).replace(",", " "),
            loc="lower right",
        )
        ax.add_artist(anchored_text)

    cbar = fig.colorbar(H, ax=axes[0:2], shrink=1.0, location="bottom", pad=0.2)
    cbar.set_label("Counts")

    return fig


def pull_plot(run_data, comparison_data, run_name, comparison_name, metric_name):
    run_data = run_data[metric_name]["pull_plot"]

    data = run_data
    name = run_name
    xs = data["xs"]
    mu = data["mu"]
    sigma = data["sigma"]
    pdf = stats.norm.pdf(xs, 0, 1)
    mean_text = "$\\mu = {}$".format(str(round(mu, 3)))
    sigma_text = "$\\sigma = {}$" + str(round(sigma, 3))

    fig, ax = plt.subplots()
    fig.set_size_inches(w=5.78853, h=4)
    bin_edges, hist = convert_data_for_step(run_data["bin_edges"], run_data["hist"])
    ax.step(x=bin_edges, y=hist, where="pre", marker="", label="Data")
    ax.plot(xs, pdf, marker="", label="Unit Gauss")
    ax.set(
        xlabel="$ z $-score",
        ylabel="Density",
        xlim=[-5, 5],
        title=name.replace("reconstruction", "Retro Reco"),
    )
    ax.set_ylim(bottom=0)
    text = mean_text + "\n" + sigma_text
    anchored_text = AnchoredText(
        text,
        loc="upper left",
    )
    ax.add_artist(anchored_text)
    ax.legend()
    return fig


def residual_histogram(
    run_data, comparison_data, run_name, comparison_name, metric_name
):
    fig, ax = plt.subplots()
    fig.set_size_inches(w=5.78853, h=4)
    for data, color, name in zip(
        [run_data, comparison_data],
        [
            plt.rcParams["axes.prop_cycle"].by_key()["color"][0],
            plt.rcParams["axes.prop_cycle"].by_key()["color"][1],
        ],
        [run_name, comparison_name],
    ):
        bin_edges, hist = convert_data_for_step(data["bin_edges"], data["hist"])
        ax.step(x=bin_edges, y=hist, where="pre", marker="", label=name)
        for percentile, linestyle in zip(
            ["low", "median", "high"], ["dashed", "solid", "dashdot"]
        ):
            ax.axvline(x=data[percentile][0], color=color, linestyle=linestyle)
    ax.legend()
    return fig


def cat2hpx(lon, lat, nside, radec=True):
    """
    Convert a catalogue to a HEALPix map of number counts per resolution
    element.

    Parameters
    ----------
    lon, lat : (ndarray, ndarray)
        Coordinates of the sources in degree. If radec=True, assume input is in the icrs
        coordinate system. Otherwise assume input is glon, glat

    nside : int
        HEALPix nside of the target map

    radec : bool
        Switch between R.A./Dec and glon/glat as input coordinate system.

    Return
    ------
    hpx_map : ndarray
        HEALPix map of the catalogue number counts in Galactic coordinates

    """

    npix = hp.nside2npix(nside)

    if radec:
        eq = SkyCoord(lon, lat, "icrs", unit="deg")
        l, b = eq.galactic.l.value, eq.galactic.b.value
    else:
        l, b = lon, lat

    # conver to theta, phi
    theta = np.radians(90.0 - b)
    phi = np.radians(l)

    # convert to HEALPix indices
    indices = hp.ang2pix(nside, theta, phi)

    idx, counts = np.unique(indices, return_counts=True)

    # fill the fullsky map
    hpx_map = np.zeros(npix, dtype=int)
    hpx_map[idx] = counts

    return hpx_map


def histogram_1d_single(data):
    fig, ax = plt.subplots()
    for key, value in data.items():
        bin_edges, hist = convert_data_for_step(value["bins"], value["hist"])
        ax.step(
            x=bin_edges,
            y=hist,
            where="pre",
            marker="",
            color="grey",
            alpha=0.4,
            label=key,
        )
    return fig
