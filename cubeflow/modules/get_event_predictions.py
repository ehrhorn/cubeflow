import argparse
from datetime import datetime
from pathlib import Path
import re
import sqlite3

import numpy as np

from I3Tray import I3Tray
from icecube import icetray
from icecube import dataclasses
from icecube import simclasses
from icecube import recclasses
from icecube import dataio
from icecube import millipede, photonics_service
from icecube.common_variables import time_characteristics

from modules.helper_functions import get_i3_file_number
from modules.helper_functions import match_i3_file_name
from modules.helper_functions import get_dataset_paths
from modules.helper_functions import get_i3_and_internal_event_nos
from modules.helper_functions import get_i3_file_names


def create_db(path):
    print("{}: creating DB".format(datetime.now().strftime("%H:%M:%S")))
    with sqlite3.connect(str(path)) as con:
        cursor = con.cursor()
        cursor.execute(sql_create_reconstruction_table)


def match_event_predictions(dataset_name, match_level, particle_type):
    file_names = get_i3_file_names(dataset_name)
    match_dataset_name = dataset_name.replace("l2", "l" + str(match_level))
    paths = get_dataset_paths(dataset_name)
    match_dataset_root = (
        Path()
        .home()
        .joinpath("work")
        .joinpath("raw_files")
        .joinpath(match_dataset_name)
    )
    match_raw_file_name = [
        file.name
        for file in match_dataset_root.joinpath("i3").iterdir()
        if file.is_file()
    ][0]
    create_db(paths["predictions"])
    for file_name in file_names:
        file_number = get_i3_file_number(file_name)
        event_nos = get_i3_and_internal_event_nos(dataset_name, file_name)
        match_file_name = match_i3_file_name(match_raw_file_name, file_number)
        get_event_predictions(
            match_dataset_root, paths, match_file_name, particle_type, event_nos
        )
    now = datetime.now().strftime("%H:%M:%S")
    print("{}: Done!".format(now))


def get_event_predictions(match_dataset_root, paths, in_file, particle_type, event_nos):
    dataset_root = match_dataset_root
    db = paths["predictions"]
    gcd_file = [
        file for file in dataset_root.joinpath("gcd").iterdir() if file.is_file
    ][0]
    i3_files_root = dataset_root.joinpath("i3")
    i3_file = i3_files_root.joinpath(in_file)

    now = datetime.now().strftime("%H:%M:%S")
    print("{}: Matching {}".format(now, i3_file.name))
    inputs = (i3_file, gcd_file, particle_type, event_nos)
    reconstruction = i3_to_list_of_tuples(inputs)
    with sqlite3.connect(str(db)) as con:
        cur = con.cursor()
        print(
            "{}: inserting {} into DB".format(
                datetime.now().strftime("%H:%M:%S"), i3_file.name
            )
        )
        cur.executemany(sql_update_reconstruction, reconstruction)
        con.commit()


def i3_to_list_of_tuples(inputs):
    i3_file = inputs[0]
    gcd_file = inputs[1]
    reconstruction = []
    particle_type = inputs[2]
    event_nos = inputs[3]
    tray = I3Tray()
    tray.AddModule("I3Reader", "reader", FilenameList=[str(gcd_file)] + [str(i3_file)])
    tray.Add(
        fetch_events, "fetch_events", inputs=(reconstruction, particle_type, event_nos),
    )
    tray.Execute()
    tray.Finish()
    return reconstruction


def fetch_events(frame, inputs):
    reconstruction = inputs[0]
    particle_type = inputs[1]
    event_nos = inputs[2]
    idx = frame["I3EventHeader"].event_id
    itemindex = np.where(event_nos[:, 1] == idx)

    if len(itemindex) == 0:
        return False

    event_no = event_nos[itemindex, 0][0]
    if len(event_no) > 1:
        return False

    try:
        if particle_type == "neutrino":
            reconstruction_result = frame["retro_crs_prefit__median__neutrino"]
        elif particle_type == "upgrade":
            reconstruction_result = frame["MonopodFit4"]
        else:
            return False
    except Exception:
        return False

    reconstruction_temp = np.zeros(len(reconstruction_columns))

    reconstruction_position = reconstruction_result.pos
    reconstruction_direction = reconstruction_result.dir
    reconstruction_temp[0] = event_no
    reconstruction_temp[1] = np.log10(reconstruction_result.energy)
    reconstruction_temp[2] = reconstruction_result.time
    reconstruction_temp[3] = reconstruction_position.x
    reconstruction_temp[4] = reconstruction_position.y
    reconstruction_temp[5] = reconstruction_position.z
    reconstruction_temp[6] = reconstruction_direction.azimuth
    reconstruction_temp[7] = reconstruction_direction.zenith
    reconstruction_temp[8] = reconstruction_result.pdg_encoding
    reconstruction_temp[np.isinf(reconstruction_temp)] = np.nan
    if np.isnan(reconstruction_temp).any():
        return False
    reconstruction.append(
        tuple([reconstruction_temp[i] for i in range(reconstruction_temp.shape[0])])
    )


reconstruction_columns = {
    "event_no": {"type": int, "nullable": False, "primary": True},
    "energy_log10": {"type": float, "nullable": True, "primary": False,},
    "time": {"type": float, "nullable": True, "primary": False},
    "position_x": {"type": float, "nullable": True, "primary": False},
    "position_y": {"type": float, "nullable": True, "primary": False},
    "position_z": {"type": float, "nullable": True, "primary": False},
    "azimuth": {"type": float, "nullable": True, "primary": False},
    "zenith": {"type": float, "nullable": True, "primary": False},
    "pid": {"type": int, "nullable": True, "primary": False},
}
sql_create_reconstruction_table = """
    CREATE TABLE reconstruction (
        event_no INTEGER PRIMARY KEY NOT NULL,
        energy_log10 REAL,
        time REAL,
        position_x REAL,
        position_y REAL,
        position_z REAL,
        azimuth REAL,
        zenith REAL,
        pid INTEGER
    );
"""
sql_update_reconstruction = """
    INSERT INTO reconstruction({}) VALUES ({})
""".format(
    ", ".join(list(reconstruction_columns.keys())),
    ", ".join(["?"] * len(list(reconstruction_columns.keys()))),
)
