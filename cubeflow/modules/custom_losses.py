import tensorflow as tf
import tensorflow.keras.backend as K

# import tensorflow_probability as tfp


# def negloglik(labels, logits):
#     neg_log_likelihood = tf.nn.softmax_cross_entropy_with_logits(
#         labels=labels, logits=logits
#     )
#     # kl = sum(model.losses)
#     # loss = neg_log_likelihood + kl
#     loss = neg_log_likelihood
#     return loss


def euclidean_distance_loss(y_true, y_pred):
    return K.sqrt(K.sum(K.square(y_pred - y_true), axis=-1))


def angle_between(y_true, y_pred):
    loss = tf.math.acos(K.dot(unit_vector(y_true), tf.transpose(unit_vector(y_pred))))
    return loss


def logcosh(y_true, y_pred):
    loss = tf.keras.losses.logcosh(y_true, y_pred)
    return loss


def huberloss(y_true, y_pred):
    loss = tf.compat.v1.losses.huber_loss(y_true, y_pred)
    return loss


def logcosh_unitpen(y_true, y_pred):
    len_pred = tf.math.sqrt(tf.reduce_sum(y_pred * y_pred, axis=-1))
    # print("Len")
    # print(len_pred.shape)
    pen = (1.0 - len_pred) * (1.0 - len_pred)
    # print("Pen")
    # print(pen.shape)
    loss = logcosh(y_true, y_pred)
    # print("Loss")
    # print(loss.shape)
    # mean_loss = K.mean((loss + pen), axis=-1)
    # print("Mean loss")
    # print(mean_loss.shape)
    return loss + pen


def cosinesim_unitpen(y_true, y_pred):
    len_pred = tf.math.sqrt(tf.reduce_sum(y_pred * y_pred, axis=-1))
    pen = (1.0 - len_pred) * (1.0 - len_pred)
    loss = tf.keras.losses.cosine_similarity(y_true, y_pred, axis=-1)
    return loss + pen


def cosinesim(y_true, y_pred):
    loss = tf.keras.losses.cosine_similarity(y_true, y_pred, axis=-1)
    return loss


def cosinesim_logcosh(y_true, y_pred):
    loss = tf.keras.losses.cosine_similarity(y_true, y_pred, axis=-1)
    lc = tf.keras.losses.LogCosh()
    return lc(loss)
