from pathlib import Path
import pickle

oscNext_pass2_root_path = Path("/groups/icecube/stuttard/data/oscNext/pass2")
gcd_file = oscNext_pass2_root_path.joinpath("gcd").joinpath(
    "GeoCalibDetectorStatus_AVG_55697-57531_PASS2_SPE_withScaledNoise.i3.gz"
)
output_file = Path().home().joinpath("files.pkl")
files = {}

data_root = oscNext_pass2_root_path.joinpath("data")
data_lvl2_path = data_root.joinpath("level2")
files["data_lvl2"] = {}
files["data_lvl2"]["data"] = {}
files["data_lvl2"]["data"]["files"] = [
    str(file) for file in data_lvl2_path.rglob("*/*.i3*") if file.is_file()
]
files["data_lvl2"]["data"]["gcd"] = str(gcd_file)

genie_root_path = oscNext_pass2_root_path.joinpath("genie")
genie_paths = {}
genie_paths["genie_lvl2"] = genie_root_path.joinpath("level2")
genie_paths["genie_lvl4"] = genie_root_path.joinpath("level4_v01.04")
genie_paths["genie_lvl5"] = genie_root_path.joinpath("level5_v01.01")
for key, value in genie_paths.items():
    files[key] = {}
    for particle_id in ["120000", "140000", "160000"]:
        files[key][particle_id[0:2]] = {}
        if key == "genie_lvl2":
            globber = value.joinpath(particle_id).glob("*.zst")
        else:
            globber = value.joinpath(particle_id).glob("*.i3*")
        files[key][particle_id[0:2]]["files"] = [
            str(file) for file in globber if file.is_file()
        ]
        files[key][particle_id[0:2]]["gcd"] = str(gcd_file)

muongun_paths = {}
muongun_root_path = oscNext_pass2_root_path.joinpath("muongun")
muongun_paths["muongun_lvl2"] = muongun_root_path.joinpath("level2")
for key, value in muongun_paths.items():
    files[key] = {}
    for particle_id in ["139008"]:
        files[key][particle_id[0:2]] = {}
        files[key][particle_id[0:2]]["files"] = [
            str(file)
            for file in value.joinpath(particle_id).glob("*.i3*")
            if file.is_file()
        ]
        files[key][particle_id[0:2]]["gcd"] = str(gcd_file)

with open(str(output_file), "wb") as f:
    pickle.dump(files, f)

update_root_path = Path("/groups/icecube/stuttard/data/upgrade/i3")
gcd_file = update_root_path.joinpath("gcd").joinpath(
    "GeoCalibDetectorStatus_ICUpgrade.v55.mixed_noGap.V4.i3.bz2"
)
data_root = update_root_path.joinpath("genie")
update_paths = {}
update_paths["upgrade_step4"] = data_root.joinpath("step4")
for key, value in update_paths.items():
    files[key] = {}
    for particle_id in ["1458"]:
        files[key][particle_id[0:2]] = {}
        files[key][particle_id[0:2]]["files"] = [
            str(file)
            for file in value.joinpath(particle_id).glob("*.i3*")
            if file.is_file()
        ]
        files[key][particle_id[0:2]]["gcd"] = str(gcd_file)

with open(str(output_file), "wb") as f:
    pickle.dump(files, f)
