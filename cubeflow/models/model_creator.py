import tensorflow as tf
# import tensorflow_probability as tfp
from tcn import TCN


def model_1(timesteps, input_dim, output_dim):
    inputs = tf.keras.Input(
        shape=(
            timesteps,
            input_dim,
        )
    )
    x = tf.keras.layers.Conv1D(filters=32, kernel_size=5)(inputs)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.MaxPool1D(
        pool_size=2, strides=None, padding="valid", data_format="channels_last"
    )(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Conv1D(filters=64, kernel_size=5)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.MaxPool1D(
        pool_size=2, strides=None, padding="valid", data_format="channels_last"
    )(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Conv1D(filters=128, kernel_size=5)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.MaxPool1D(
        pool_size=2, strides=None, padding="valid", data_format="channels_last"
    )(x)
    x = tf.keras.layers.Conv1D(filters=256, kernel_size=5)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.MaxPool1D(
        pool_size=2, strides=None, padding="valid", data_format="channels_last"
    )(x)
    x = tf.keras.layers.Conv1D(filters=512, kernel_size=5)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.MaxPool1D(
        pool_size=2, strides=None, padding="valid", data_format="channels_last"
    )(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Flatten()(x)
    x = tf.keras.layers.Dense(units=2048)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Dense(units=1024)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Dense(units=512)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Dense(units=256)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.BatchNormalization()(x)
    out = tf.keras.layers.Dense(units=output_dim)(x)
    model = tf.keras.models.Model(inputs, out)
    return model


# def tfp_model_1():
#     inputs = tf.keras.Input(shape=(200, 5,))
#     x = tfp.layers.Convolution1DFlipout(
#         filters=32, kernel_size=5, padding="SAME", activation=tf.nn.leaky_relu
#     )(inputs)
#     x = tf.keras.layers.MaxPool1D(
#         pool_size=2, strides=None, padding="valid", data_format="channels_last"
#     )(x)
#     x = tf.keras.layers.BatchNormalization()(x)
#     x = tfp.layers.Convolution1DFlipout(
#         filters=64, kernel_size=5, padding="SAME", activation=tf.nn.leaky_relu
#     )(x)
#     x = tf.keras.layers.MaxPool1D(
#         pool_size=2, strides=None, padding="valid", data_format="channels_last"
#     )(x)
#     x = tf.keras.layers.BatchNormalization()(x)
#     x = tfp.layers.Convolution1DFlipout(
#         filters=128, kernel_size=5, padding="SAME", activation=tf.nn.leaky_relu
#     )(x)
#     x = tf.keras.layers.MaxPool1D(
#         pool_size=2, strides=None, padding="valid", data_format="channels_last"
#     )(x)
#     x = tfp.layers.Convolution1DFlipout(
#         filters=256, kernel_size=5, padding="SAME", activation=tf.nn.leaky_relu
#     )(x)
#     x = tf.keras.layers.MaxPool1D(
#         pool_size=2, strides=None, padding="valid", data_format="channels_last"
#     )(x)
#     x = tfp.layers.Convolution1DFlipout(
#         filters=512, kernel_size=5, padding="SAME", activation=tf.nn.leaky_relu
#     )(x)
#     x = tf.keras.layers.MaxPool1D(
#         pool_size=2, strides=None, padding="valid", data_format="channels_last"
#     )(x)
#     x = tf.keras.layers.BatchNormalization()(x)
#     x = tf.keras.layers.Flatten()(x)
#     x = tfp.layers.DenseFlipout(units=2048)(x)
#     x = tf.keras.layers.LeakyReLU()(x)
#     x = tf.keras.layers.BatchNormalization()(x)
#     x = tfp.layers.DenseFlipout(units=1024)(x)
#     x = tf.keras.layers.LeakyReLU()(x)
#     x = tf.keras.layers.BatchNormalization()(x)
#     x = tfp.layers.DenseFlipout(units=512)(x)
#     x = tf.keras.layers.LeakyReLU()(x)
#     x = tf.keras.layers.BatchNormalization()(x)
#     x = tfp.layers.DenseFlipout(units=256)(x)
#     x = tf.keras.layers.LeakyReLU()(x)
#     x = tf.keras.layers.BatchNormalization()(x)
#     out = tfp.layers.DenseFlipout(units=1)(x)
#     model = tf.keras.models.Model(inputs, out)
#     return model


def tcn_model_1(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(return_sequences=False)(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_2(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=32,
        kernel_size=5,
        nb_stacks=1,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=True,
        dropout_rate=0.5,
        return_sequences=True,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=True,
    )(i)
    o = TCN(
        nb_filters=64,
        kernel_size=5,
        nb_stacks=1,
        dilations=[64, 128, 256, 512, 1024, 2048],
        padding="causal",
        use_skip_connections=True,
        dropout_rate=0.5,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=True,
    )(o)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_3(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=5,
        nb_stacks=1,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(1024)(o)
    o = tf.keras.layers.LeakyReLU()(o)
    o = tf.keras.layers.BatchNormalization()(o)
    o = tf.keras.layers.Dense(512)(o)
    o = tf.keras.layers.LeakyReLU()(o)
    o = tf.keras.layers.BatchNormalization()(o)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_4(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=5,
        nb_stacks=4,
        dilations=[1, 2, 4, 8, 16, 32, 64],
        padding="causal",
        use_skip_connections=True,
        dropout_rate=0.3,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=True,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_5(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=2,
        nb_stacks=1,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=True,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_6(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(return_sequences=False, padding="same")(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_7(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=5,
        nb_stacks=1,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="same",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_8(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=10,
        nb_stacks=1,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="same",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_9(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=20,
        nb_stacks=1,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="same",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_10(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=2,
        nb_stacks=2,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="same",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_11(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=20,
        nb_stacks=2,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="same",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_12(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=40,
        nb_stacks=1,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="same",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_13(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=40,
        nb_stacks=2,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="same",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_14(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=20,
        nb_stacks=2,
        dilations=[1, 2, 4, 8, 16, 32, 64],
        padding="same",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_15(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=32,
        kernel_size=20,
        nb_stacks=2,
        dilations=[2 ** i for i in range(8)],
        padding="same",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=True,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = TCN(
        nb_filters=64,
        kernel_size=20,
        nb_stacks=2,
        dilations=[2 ** i for i in range(8)],
        padding="same",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(o)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_16(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=20,
        nb_stacks=2,
        dilations=[1, 2, 4, 8, 16, 32, 64, 128],
        padding="same",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_17(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=128,
        kernel_size=20,
        nb_stacks=1,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="same",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def model_2(timesteps, input_dim, output_dim):
    inputs = tf.keras.Input(
        shape=(
            timesteps,
            input_dim,
        )
    )
    x = tf.keras.layers.Conv1D(filters=32, kernel_size=5)(inputs)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.MaxPool1D(
        pool_size=2, strides=None, padding="valid", data_format="channels_last"
    )(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Conv1D(filters=64, kernel_size=5)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.MaxPool1D(
        pool_size=2, strides=None, padding="valid", data_format="channels_last"
    )(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Flatten()(x)
    x = tf.keras.layers.Dense(units=512)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Dense(units=256)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Dense(units=128)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.BatchNormalization()(x)
    out = tf.keras.layers.Dense(units=output_dim)(x)
    model = tf.keras.models.Model(inputs, out)
    return model


def tcn_model_18(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=5,
        nb_stacks=1,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_19(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=10,
        nb_stacks=1,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_20(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=2,
        nb_stacks=1,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="same",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_21(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=2,
        nb_stacks=1,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_22(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=2,
        nb_stacks=1,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=True,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_23(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=2,
        nb_stacks=1,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=True,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=True,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_24(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=256,
        kernel_size=2,
        nb_stacks=1,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=True,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=True,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_25(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=512,
        kernel_size=2,
        nb_stacks=1,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=True,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=True,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_26(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=256,
        kernel_size=2,
        nb_stacks=2,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=True,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=True,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_27(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=256,
        kernel_size=3,
        nb_stacks=2,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=True,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=True,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_28(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=32,
        kernel_size=20,
        nb_stacks=1,
        dilations=[2 ** i for i in range(10)],
        padding="same",
        use_skip_connections=True,
        dropout_rate=0.0,
        return_sequences=True,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=True,
    )(i)
    o = TCN(
        nb_filters=48,
        kernel_size=20,
        nb_stacks=2,
        dilations=[2 ** i for i in range(10)],
        padding="same",
        use_skip_connections=True,
        dropout_rate=0.0,
        return_sequences=True,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=True,
    )(o)
    o = TCN(
        nb_filters=64,
        kernel_size=20,
        nb_stacks=2,
        dilations=[2 ** i for i in range(10)],
        padding="same",
        use_skip_connections=True,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=True,
    )(o)
    x = tf.keras.layers.Flatten()(o)
    x = tf.keras.layers.Dense(units=128)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.Dense(units=128)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    out = tf.keras.layers.Dense(units=output_dim)(x)
    model = tf.keras.models.Model(inputs=[i], outputs=[out])
    return model


def tcn_model_29(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=32,
        kernel_size=10,
        nb_stacks=1,
        dilations=[2 ** i for i in range(10)],
        padding="same",
        use_skip_connections=True,
        dropout_rate=0.0,
        return_sequences=True,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=True,
    )(i)
    o = TCN(
        nb_filters=64,
        kernel_size=5,
        nb_stacks=2,
        dilations=[2 ** i for i in range(10)],
        padding="same",
        use_skip_connections=True,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=True,
    )(o)
    x = tf.keras.layers.Flatten()(o)
    x = tf.keras.layers.Dense(units=64)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.Dense(units=32)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    out = tf.keras.layers.Dense(units=output_dim)(x)
    model = tf.keras.models.Model(inputs=[i], outputs=[out])
    return model


def tcn_model_30(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=256,
        kernel_size=2,
        nb_stacks=2,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=True,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=True,
    )(i)
    x = tf.keras.layers.Flatten()(o)
    x = tf.keras.layers.Dense(units=256)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    x = tf.keras.layers.Dense(units=128)(x)
    x = tf.keras.layers.LeakyReLU()(x)
    o = tf.keras.layers.Dense(output_dim)(x)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_31(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=256,
        kernel_size=2,
        nb_stacks=2,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_32(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=4,
        nb_stacks=2,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="same",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_33(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=4,
        nb_stacks=2,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_34(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=10,
        nb_stacks=4,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model


def tcn_model_35(timesteps, input_dim, output_dim):
    i = tf.keras.Input(batch_shape=(None, timesteps, input_dim))
    o = TCN(
        nb_filters=64,
        kernel_size=10,
        nb_stacks=8,
        dilations=[1, 2, 4, 8, 16, 32],
        padding="causal",
        use_skip_connections=False,
        dropout_rate=0.0,
        return_sequences=False,
        activation="relu",
        kernel_initializer="he_normal",
        use_batch_norm=False,
    )(i)
    o = tf.keras.layers.Dense(output_dim)(o)
    model = tf.keras.models.Model(inputs=[i], outputs=[o])
    return model
