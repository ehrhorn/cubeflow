from datetime import datetime
import sqlite3
from pathlib import Path
import pickle

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pandas.core.reshape.merge import merge

dataset_name = "dev_numu_train_l5_retro_001"
retro_db = Path().home() / "data" / "retro_reco_2.db"
db = Path().home() / "data" / (dataset_name + ".db")

transformers_path = (
    Path().home() / "work" / "datasets" / dataset_name / "meta" / "transformers.pkl"
)
with open(transformers_path, "rb") as f:
    transformers = pickle.load(f)

mpl.rcParams["lines.linewidth"] = 0.5

print(f"{datetime.now()}: Fetching truth")

query = "select event_no, energy_log10 from truth limit -1"
with sqlite3.connect(db) as con:
    truth_df = pd.read_sql(query, con)

print(f"{datetime.now()}: Fetched truth")

event_nos = truth_df["event_no"].values

print(f"{datetime.now()}: Fetching Retro")

query = "select event_no, energy_log10 from retro_reco where event_no in ({})".format(
    ", ".join([str(event_no) for event_no in event_nos])
)
with sqlite3.connect(retro_db) as con:
    retro_df = pd.read_sql(query, con)

print(f"{datetime.now()}: Fetched Retro")

merged_df = retro_df.merge(right=truth_df, on="event_no", suffixes=("_retro", "_truth"))

xs = np.linspace(0, 4, 1000)
ys = np.linspace(0, 4, 1000)

print(f"{datetime.now()}: Plotting...")

retro_energy = merged_df["energy_log10_retro"].values
true_energy = transformers["truth"]["energy_log10"].inverse_transform(
    merged_df["energy_log10_truth"].values.reshape(-1, 1)
)

print(f"{datetime.now()}: Minimum true energy: {min(true_energy)}")

fig, ax = plt.subplots()
ax.hist2d(
    retro_energy.flatten(),
    true_energy.flatten(),
    bins=[100, 100],
    range=([0, 4], [0, 4]),
    cmin=1,
)
ax.plot(xs, ys, "r-")
ax.set(
    xlabel="Energy Retro", ylabel="Energy truth", title=f"{len(retro_energy)} events"
)
fig.savefig(Path().home() / "hist.pdf")