import argparse
import os
import pickle
import shutil
import socket
import warnings
from argparse import HelpFormatter
from contextlib import redirect_stdout
from datetime import datetime
from pathlib import Path
from shutil import copyfile

import cubeflow.models.model_creator as model_creator
import cubeflow.modules.custom_losses as custom_losses
import cubeflow.modules.helper_functions as helper_functions
import numpy as np
import pandas as pd
import tensorflow as tf
from create_numpy_dataset import create_numpy_file
from cubeflow.modules.create_run_histograms import create_run_histograms
from cubeflow.modules.data_generator import DataGenerator
from cubeflow.modules.helper_functions import (get_dataset_paths,
                                               open_pickle_file)
from tcn import tcn_full_summary

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
os.environ["CUDA_VISIBLE_DEVICES"] = "6"
warnings.filterwarnings("ignore", category=UserWarning)
warnings.filterwarnings("ignore", category=RuntimeWarning)
warnings.filterwarnings("ignore", category=FutureWarning)


class LRWarmupCallback(tf.keras.callbacks.Callback):
    def __init__(self, min_lr, max_lr, batches):
        super(LRWarmupCallback, self).__init__()
        self.min_lr = min_lr
        self.batches = batches
        self.a = (max_lr - min_lr) / batches

    def on_train_batch_end(self, batch, logs=None):
        iteration = self.model.optimizer.iterations
        iteration = iteration.numpy()
        if iteration <= self.batches:
            new_lr = self.a * iteration + self.min_lr
            self.model.optimizer.lr = new_lr


class LRInverseTimeDecay(tf.keras.callbacks.Callback):
    def __init__(self, initial_learning_rate, decay_steps, decay_rate):
        super(LRInverseTimeDecay, self).__init__()
        self.initial_learning_rate = initial_learning_rate
        self.decay_steps = decay_steps
        self.decay_rate = decay_rate

    def on_epoch_end(self, epoch, logs=None):
        new_lr = self.initial_learning_rate / (
            1 + self.decay_rate * epoch / self.decay_steps
        )
        self.model.optimizer.lr = new_lr


class LRLogger(tf.keras.callbacks.Callback):
    def __init__(self, file):
        super(LRLogger, self).__init__()
        self.file = file

    def on_epoch_begin(self, epoch, logs=None):
        if self.file.is_file():
            with open(str(self.file), "rb") as f:
                lr_history = pickle.load(f)
        else:
            lr_history = []
        lr = self.model.optimizer.lr.numpy()
        lr_history.append(lr)
        with open(str(self.file), "wb") as f:
            pickle.dump(lr_history, f)


parser = argparse.ArgumentParser()
parser.add_argument("-p")
args = parser.parse_args()
params_path = Path(args.p)
params = helper_functions.open_json_file(params_path)

gpus = tf.config.experimental.list_physical_devices("GPU")

run_name = (
    params["train_dataset"]
    + "."
    + params["type"]
    + "."
    + params["model"]
    + "."
    + params["cleaning"]
    + "."
    + params["optimizer"]
    + "."
    + params["loss"]
    + "."
    + str(params["weight_name"])
    + "."
    + str(params["patience"])
    + "."
    + str(params["train_batch_size"])
    + "."
    + str(params["max_length"])
    + "."
    + datetime.now().strftime("%y%m%d%H%M")
)
paths = helper_functions.get_dataset_paths(params["train_dataset"])
run_paths = helper_functions.create_run_paths(run_name)
helper_functions.create_run_dir(run_paths["training"])


host_name = socket.gethostname()
# if host_name == "cubedev" or host_name == "air.local":
#     device = "/device:CPU:0"
# else:
#     device = "/device:GPU:0"

if params["type"] == "energy":
    params["targets"] = ["energy_log10"]
elif params["type"] == "time":
    params["targets"] = ["time"]
elif params["type"] == "direction":
    params["targets"] = ["direction_x", "direction_y", "direction_z"]
elif params["type"] == "vertex":
    params["targets"] = ["vertex_x", "vertex_y", "vertex_z"]
elif params["type"] == "angle":
    params["targets"] = ["azimuth", "zenith"]
elif params["type"] == "zenith":
    params["targets"] = ["zenith"]
elif params["type"] == "azimuth":
    params["targets"] = ["azimuth"]
elif params["type"] == "classification":
    params["targets"] = "pid"
sets = helper_functions.open_pickle_file(paths["sets"])
train_generator = DataGenerator(
    params,
    paths,
    sets["train"],
    params["train_batch_size"],
    to_numpy=False,
    shuffle=True,
)
val_generator = DataGenerator(
    params,
    paths,
    sets["val"],
    params["val_batch_size"],
    to_numpy=False,
    shuffle=True,
)

# mirrored_strategy = tf.distribute.MirroredStrategy(devices=["/gpu:0", "/gpu:1"])
device = "/device:GPU:0"
# now = datetime.now().strftime("%H:%M:%S")
# print("{}: GPU available: {}".format(now, tf.test.is_gpu_available()))
now = datetime.now().strftime("%H:%M:%S")
print("{}: GPUs available: {}".format(now, gpus))
now = datetime.now().strftime("%H:%M:%S")
print("{}: Using device {}".format(now, device))
now = datetime.now().strftime("%H:%M:%S")
print("{}: Training run {}".format(now, run_name))

workers = 8

with tf.device(device):
    model = getattr(model_creator, params["model"])(
        params["max_length"],
        len(params["features"]),
        len(params["targets"]),
    )
    try:
        loss = getattr(custom_losses, params["loss"])
    except Exception:
        loss = params["loss"]
    model.compile(optimizer=params["optimizer"], loss=loss)

    trainable_params = int(
        np.sum([tf.keras.backend.count_params(w) for w in model.trainable_weights])
    )
    non_trainable_params = int(
        np.sum([tf.keras.backend.count_params(w) for w in model.non_trainable_weights])
    )
    params["all_params"] = trainable_params + non_trainable_params
    params["trainable_params"] = trainable_params
    helper_functions.save_json_file(
        run_paths["training"]["root"].joinpath("params.json"), params
    )

    # if "tcn" in params["model"]:
    #     with open(str(run_paths["training"]["summary"]), "w") as f:
    #         with redirect_stdout(f):
    #             tcn_full_summary(model, expand_residual_blocks=True)
    #     tcn_full_summary(model, expand_residual_blocks=True)
    # else:
    with open(str(run_paths["training"]["summary"]), "w") as f:
        with redirect_stdout(f):
            model.summary()
        model.summary()

    model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
        filepath=str(run_paths["training"]["weights"]),
        monitor="val_loss",
        verbose=1,
        save_best_only=True,
        mode="min",
        save_weights_only=True,
        save_freq="epoch",
    )
    csv_file_callback = tf.keras.callbacks.CSVLogger(
        str(run_paths["training"]["csv"]), separator=",", append=True
    )
    early_stopping_callback = tf.keras.callbacks.EarlyStopping(
        monitor="val_loss", min_delta=1e-3, patience=params["patience"], mode="min"
    )
    lr_warmup_callback = LRWarmupCallback(
        params["min_lr"],
        params["max_lr"],
        len(train_generator),
    )
    lr_scheduler_callback = LRInverseTimeDecay(
        initial_learning_rate=params["max_lr"], decay_steps=1.0, decay_rate=0.5
    )
    lr_logger_callback = LRLogger(run_paths["training"]["lr"])

    if params["in_memory"]:
        numpy_dataset_file = create_numpy_file(params, train=True)
        numpy_dataset = open_pickle_file(numpy_dataset_file)
        model.fit(
            x=numpy_dataset["train"]["X"],
            y=numpy_dataset["train"]["y"],
            epochs=params["epochs"],
            verbose=1,
            shuffle=True,
            batch_size=params["train_batch_size"],
            validation_data=(numpy_dataset["val"]["X"], numpy_dataset["val"]["y"]),
            initial_epoch=0,
            workers=workers,
            use_multiprocessing=True,
            callbacks=[
                early_stopping_callback,
                model_checkpoint_callback,
                csv_file_callback,
                lr_warmup_callback,
                lr_scheduler_callback,
                lr_logger_callback,
            ],
        )
    else:
        model.fit_generator(
            generator=train_generator,
            epochs=params["epochs"],
            verbose=1,
            callbacks=[
                early_stopping_callback,
                model_checkpoint_callback,
                csv_file_callback,
                lr_warmup_callback,
                lr_scheduler_callback,
                lr_logger_callback,
            ],
            validation_data=val_generator,
            use_multiprocessing=True,
            workers=workers,
            max_queue_size=1024,
            shuffle=False,
            initial_epoch=0,
        )

    helper_functions.move_file(
        run_paths["training"]["root"], run_paths["done"]["root"].parent
    )
    predict_dataset_names = params["predict_datasets"]
    for predict_dataset_name in predict_dataset_names:
        predict_paths = get_dataset_paths(predict_dataset_name)
        events = open_pickle_file(predict_paths["sets"])["test"]
        predict_generator = DataGenerator(
            params,
            predict_paths,
            events,
            params["val_batch_size"],
            to_numpy=False,
            shuffle=False,
        )
        model.load_weights(str(run_paths["done"]["weights"]))
        if params["in_memory"]:
            numpy_dataset_file = create_numpy_file(
                params, predict_dataset_name=predict_dataset_name, train=True
            )
            numpy_dataset = open_pickle_file(numpy_dataset_file)
            now = datetime.now().strftime("%H:%M:%S")
            print(
                "{}: Starting prediction on {}, {}".format(
                    now, params_path.stem, predict_dataset_name
                )
            )
            predictions = model.predict(numpy_dataset["test"]["X"], verbose=0)
        else:
            now = datetime.now().strftime("%H:%M:%S")
            print(
                "{}: Starting prediction on {}, {}".format(
                    now, params_path.stem, predict_dataset_name
                )
            )
            predictions = model.predict_generator(
                generator=predict_generator,
                workers=workers,
                use_multiprocessing=True,
                verbose=1,
            )
        now = datetime.now().strftime("%H:%M:%S")
        print(
            "{}: Prediction done on {}, {}. Saving".format(
                now, params_path.stem, predict_dataset_name
            )
        )
        copyfile(
            str(predict_paths["predictions"]), str(Path().home() / "predictions.db")
        )
        helper_functions.save_prediction_artifacts(
            run_name, predict_dataset_name, params, events, predictions
        )
        copyfile(
            str(Path().home() / "predictions.db"), str(predict_paths["predictions"])
        )
        now = datetime.now().strftime("%H:%M:%S")
        print(
            "{}: Calculating errors on {}, {}".format(
                now, params_path.stem, predict_dataset_name
            )
        )
        try:
            copyfile(str(paths["errors"]), str(Path().home() / "errors.db"))
        except Exception:
            if (Path().home() / "errors.db").is_file():
                (Path().home() / "errors.db").unlink()
            print(f"{datetime.now()}: No error db, proceeding.")
        helper_functions.calculate_errors(
            run_name, predict_dataset_name, [params["type"]]
        )
        copyfile(str(Path().home() / "errors.db"), str(paths["errors"]))
        perm_imp = {}
        now = datetime.now().strftime("%H:%M:%S")
        print(f"{now}: Calculating permutation importance baseline")
        perm_imp_generator = DataGenerator(
            params,
            predict_paths,
            events,
            params["val_batch_size"],
            to_numpy=False,
            shuffle=False,
            scramble=False,
        )
        eval_history = model.evaluate_generator(
            generator=perm_imp_generator,
            steps=10,
            callbacks=None,
            max_queue_size=1024,
            workers=workers,
            use_multiprocessing=True,
            verbose=1,
        )
        perm_imp["baseline"] = eval_history
        for i, feature in enumerate(params["features"]):
            now = datetime.now().strftime("%H:%M:%S")
            print(f"{now}: Calculating permutation importance on {feature}")
            perm_imp_generator = DataGenerator(
                params,
                predict_paths,
                events,
                params["val_batch_size"],
                to_numpy=False,
                shuffle=False,
                scramble=i,
            )
            eval_history = model.evaluate_generator(
                generator=perm_imp_generator,
                steps=10,
                callbacks=None,
                max_queue_size=1024,
                workers=workers,
                use_multiprocessing=True,
                verbose=1,
            )
            perm_imp[feature] = eval_history
        perm_x = []
        perm_y = []
        for key, value in perm_imp.items():
            perm_x.append(key)
            perm_y.append(value / perm_imp["baseline"])
        perm_out = [perm_x, perm_y]
        perm_imp_file = run_paths["done"]["root"] / "perm_imp.pkl"
        with open(str(perm_imp_file), "wb") as f:
            pickle.dump(perm_out, f)
        # create_run_histograms(predict_dataset_name, run_name, params["type"])
        # try:
        #     create_run_histograms(
        #         predict_dataset_name, "reconstruction", params["type"]
        #     )
        # except Exception as e:
        #     print(str(e))
        #     continue


now = datetime.now().strftime("%H:%M:%S")
print("{}: Training done on {}".format(now, params_path.stem))
