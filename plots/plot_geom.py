from pathlib import Path
import sqlite3

import matplotlib.lines as mlines
import matplotlib.pyplot as plt
from numpy.core.defchararray import mod
import pandas as pd

dataset_name = "dev_upgrade_001"
db = Path().home() / "work" / "datasets" / dataset_name / "meta" / "gcd.db"
out_path = Path().home() / "geom_plots"

query = "select * from geometry"
with sqlite3.connect(str(db)) as con:
    dataframe = pd.read_sql(query, con)

pmt_types = dataframe["pmt_type"].unique().tolist()
for pmt_type in pmt_types + ["all"]:
    modified_dataframe = dataframe.copy()
    if pmt_type != "all":
        modified_dataframe = modified_dataframe[
            modified_dataframe["pmt_type"] == pmt_type
        ]
    strings = modified_dataframe["string"].unique()
    x = []
    y = []
    c = []
    for string in strings:
        x.append(
            modified_dataframe[modified_dataframe["string"] == string]["dom_x"].iloc[0]
        )
        y.append(
            modified_dataframe[modified_dataframe["string"] == string]["dom_y"].iloc[0]
        )
        if string < 80:
            c.append("b")
        elif string >= 80 and string < 87:
            c.append("g")
        else:
            c.append("r")

    markersize = 10
    fig, ax = plt.subplots()
    ax.scatter(x, y, c=c, s=markersize)
    for i, txt in enumerate(strings):
        ax.annotate(txt, (x[i], y[i]), ha="center", va="center", size=3)
    blue_patch = mlines.Line2D(
        [],
        [],
        color="white",
        marker="o",
        markerfacecolor="blue",
        markersize=markersize,
        label="IceCube",
    )
    green_patch = mlines.Line2D(
        [],
        [],
        color="white",
        marker="o",
        markerfacecolor="green",
        markersize=markersize,
        label="DeepCore",
    )
    red_patch = mlines.Line2D(
        [],
        [],
        color="white",
        marker="o",
        markerfacecolor="red",
        markersize=markersize,
        label="Upgrade",
    )
    ax.legend(
        handles=[blue_patch, green_patch, red_patch],
        loc="upper center",
        bbox_to_anchor=(0.5, 1.1),
        fancybox=True,
        ncol=3,
    )
    out_file = str(out_path / (str(pmt_type) + ".pdf"))
    fig.savefig(out_file)
