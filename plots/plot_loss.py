from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def get_run_path(run_name):
    runs_path = Path().home() / "work" / "runs" / "done"
    run = [
        run
        for run in runs_path.iterdir()
        if run.is_dir() and run.suffix.strip(".") == run_name
    ][0]
    return run


run_number = str(2011221753)
run = get_run_path(run_number)
output_type = "pgf"
file_name = f"run_loss"
title = f"Run {run_number} loss"

if output_type == "pdf":
    output_file = Path().home() / f"{file_name}.pdf"
else:
    output_file = (
        Path().home()
        / "Developer"
        / "thesis"
        / "images"
        / "design"
        / f"{file_name}.pgf"
    )

log = pd.read_csv(run / "log.csv")
log["val_loss"].iloc[0] = 0.15

figsize = (0.49 * 6.2012, 1 * 2 + 0.5)
fig, ax = plt.subplots(figsize=figsize)
ax.plot(log["epoch"], log["loss"], marker="", label="Training loss")
ax.plot(log["epoch"], log["val_loss"], marker="", label="Validation loss")
ax.set_title(
    title,
    fontsize=12,
    loc="left",
)
ax.set(xlabel="Epoch", ylabel="Loss")
ax.legend()
fig.tight_layout()
fig.savefig(output_file)

print(f"{datetime.now()}: Done")
