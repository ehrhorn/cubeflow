import pickle
import sqlite3
from pathlib import Path

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle
from matplotlib.ticker import MaxNLocator
from scipy.stats import iqr

pd.set_option("mode.use_inf_as_na", True)
mpl.rcParams["axes.unicode_minus"] = False


def get_error_ylabel(metric):
    if metric == "energy_log10":
        ylabel = "$\\log_{10}(E_{\\textup{true}}) - \\log_{10}(E_{\\textup{reco}}) \\, \\left[ E / \\textup{GeV} \\right]$"
    elif metric == "zenith":
        ylabel = "$ \\theta_{\\textup{true}} - \\theta_{\\textup{reco}} $"
    return ylabel


def get_prediction_label(metric):
    if metric == "energy_log10":
        xlabel = (
            "$\\log_{10}(E_{\\textup{true}}) \\, \\left[ E / \\textup{GeV} \\right]$"
        )
        ylabel = (
            "$\\log_{10}(E_{\\textup{reco}}) \\, \\left[ E / \\textup{GeV} \\right]$"
        )
    elif metric == "zenith":
        xlabel = "$ \\theta_{\\textup{true}} \\, [\\textup{deg}]$"
        ylabel = "$ \\theta_{\\textup{reco}} \\, [\\textup{deg}]$"
    return xlabel, ylabel


def percentile_25(x):
    return np.percentile(x, 25)


def percentile_50(x):
    return np.percentile(x, 50)


def percentile_75(x):
    return np.percentile(x, 75)


def percentile_65(x):
    return np.percentile(x, 65)


def percentile_68(x):
    return np.percentile(x, 68)


def custom_iqr(x):
    return iqr(x, scale="normal")


def get_run_path(run_name):
    runs_path = Path().home() / "work" / "erda_runs" / "done"
    run = [
        run
        for run in runs_path.iterdir()
        if run.is_dir() and run.suffix.strip(".") == run_name
    ][0]
    return run


def means_and_errors(x, y, minimum, maximum, metric):
    means, lows, highs = [], [], []
    bins = 18
    _, bin_edges = np.histogram(y, bins=bins, range=(minimum, maximum))
    centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    bin_range = bin_edges.shape[0] - 1
    for iter in range(bin_range):
        if iter == bin_range - 1:
            data = x[np.logical_and(y >= bin_edges[iter], y <= bin_edges[iter + 1])]
        else:
            data = x[np.logical_and(y > bin_edges[iter], y <= bin_edges[iter + 1])]
        mean = percentile_50(data)
        means.append(mean)
        low = percentile_25(data)
        lows.append(abs(means[iter] - low))
        high = percentile_75(data)
        highs.append(abs(means[iter] - high))
    errors = np.array((lows, highs))
    return centers, means, errors


limit = -1
metric = "energy_log10"
run_name = str(2012242224)
# metric = "zenith"
# run_name = str(2011221753)

retro_db_path = Path().home() / "work" / "erda_datasets" / "retro_reco_2.db"
retro_db_uri = f"file:{retro_db_path}?mode=ro"

run = get_run_path(run_name)
dataset_name = run.name.split(".")[0]
truth_db_path = (
    Path().home()
    / "work"
    / "erda_datasets"
    / dataset_name
    / "predictions"
    / "predictions.db"
)
truth_db_uri = f"file:{truth_db_path}?mode=ro"
sets_path = (
    Path().home() / "work" / "erda_datasets" / dataset_name / "meta" / "sets.pkl"
)
with open(sets_path, "rb") as f:
    sets = pickle.load(f)

out_file = Path().home() / "plots" / f"{metric.strip('_log10')}_2d.pgf"

nrows = 2
ncols = 2
figsize = (6.2012, 5.5)
fig, axes = plt.subplots(
    nrows=nrows, ncols=ncols, figsize=figsize, sharex=False, sharey="row"
)

if metric == "energy_log10":
    xlabel = "$\\log_{10}(E_{\\textup{true}}) - \\log_{10}(E_{\\textup{reco}}) \\, \\left[ E / \\textup{GeV} \\right]$"
elif metric == "zenith":
    xlabel = "$\\theta_{\\textup{truth}} - \\theta_{\\textup{prediction}} \\, [\\textup{deg}]$"

suptitle = f"{metric.strip('_log10').capitalize()} error distribution in selected bins"

if metric == "energy_log10":
    query = f"select event_no, {metric} from truth limit {limit}"
else:
    query = f"select event_no, energy_log10 as energy_log10_true, {metric} from truth limit {limit}"
with sqlite3.connect(truth_db_uri, uri=True) as con:
    truth = pd.read_sql(query, con)
truth = truth[truth["event_no"].isin(sets["val"])]
if metric == "zenith" or metric == "azimuth":
    if truth[metric].max() < 7:
        truth[metric] = truth[metric].apply(np.rad2deg)
stringified_events_nos = ", ".join(
    [str(event_no) for event_no in truth["event_no"].values.flatten().tolist()]
)

query = f"select event_no, {metric} from '{run.name}' where event_no in({stringified_events_nos})"
with sqlite3.connect(truth_db_uri, uri=True) as con:
    predictions = pd.read_sql(query, con)
if metric == "zenith" or metric == "azimuth":
    if predictions[metric].max() < 7:
        predictions[metric] = predictions[metric].apply(np.rad2deg)

query = f"select event_no, {metric} from 'retro_reco' where event_no in({stringified_events_nos})"
with sqlite3.connect(retro_db_path, uri=True) as con:
    retro = pd.read_sql(query, con)
if metric == "zenith" or metric == "azimuth":
    if retro[metric].max() < 7:
        retro[metric] = retro[metric].apply(np.rad2deg)

predictions_merged = truth.merge(
    predictions, on="event_no", suffixes=("_true", "_reco")
)
if metric == "energy_log10":
    predictions_merged["error"] = (
        predictions_merged[f"{metric}_true"] - predictions_merged[f"{metric}_reco"]
    )
elif metric == "zenith":
    # predictions_merged["error"] = np.cos(
    #     np.deg2rad(predictions_merged[f"{metric}_true"])
    # ) - np.cos(np.deg2rad(predictions_merged[f"{metric}_reco"]))
    predictions_merged["error"] = (
        predictions_merged[f"{metric}_true"] - predictions_merged[f"{metric}_reco"]
    )

retro_merged = truth.merge(retro, on="event_no", suffixes=("_true", "_reco"))
if metric == "energy_log10":
    retro_merged["error"] = (
        retro_merged[f"{metric}_true"] - retro_merged[f"{metric}_reco"]
    )
elif metric == "zenith":
    # retro_merged["error"] = np.cos(np.deg2rad(retro_merged[f"{metric}_true"])) - np.cos(
    #     np.deg2rad(retro_merged[f"{metric}_reco"])
    # )
    retro_merged["error"] = (
        retro_merged[f"{metric}_true"] - retro_merged[f"{metric}_reco"]
    )
retro_merged.dropna(inplace=True)
non_na_event_nos = retro_merged["event_no"].values.flatten().tolist()
predictions_merged = predictions_merged[
    predictions_merged["event_no"].isin(non_na_event_nos)
]

no_events = len(retro_merged)
prediction_Hs = []
error_Hs = []

for iteration, inference_type in enumerate([predictions_merged, retro_merged]):
    min_e, max_e = (0.0, 3.0)
    if metric == "energy_log10":
        minimum, maximum = (0.0, 3.0)
    elif metric == "zenith":
        minimum, maximum = (0, 180)
    x = inference_type[f"{metric}_true"].values.flatten()
    y = inference_type[f"{metric}_reco"].values.flatten()
    x = np.clip(x, minimum, maximum)
    y = np.clip(y, minimum, maximum)
    H, _, _ = np.histogram2d(y, x, bins=100)
    prediction_Hs.append(H)
    if metric == "energy_log10":
        y_minimum = -0.8
        y_maximum = 0.8
    elif metric == "zenith":
        y_minimum = -100
        y_maximum = 100
    x = inference_type["energy_log10_true"].values.flatten()
    y = inference_type["error"].values.flatten()
    x = np.clip(x, min_e, max_e)
    y = np.clip(y, y_minimum, y_maximum)
    H, _, _ = np.histogram2d(y, x, bins=100)
    error_Hs.append(H)

prediction_H = np.amax(prediction_Hs)
error_H = np.amax(error_Hs)

for iteration, inference_type in enumerate([predictions_merged, retro_merged]):
    min_e, max_e = (0.0, 3.0)
    if metric == "energy_log10":
        minimum, maximum = (0.0, 3.0)
    elif metric == "zenith":
        minimum, maximum = (0, 180)
    x = inference_type[f"{metric}_true"].values.flatten()
    y = inference_type[f"{metric}_reco"].values.flatten()
    no_clipped_x = np.sum(np.logical_or(x < minimum, x > maximum))
    no_clipped_y = np.sum(np.logical_or(y < minimum, y > maximum))
    no_clipped = no_clipped_x + no_clipped_y
    pct_clipped = (no_clipped / no_events) * 100
    x = np.clip(x, minimum, maximum)
    y = np.clip(y, minimum, maximum)
    line = np.linspace(minimum, maximum, 100)

    centers, means, errors = means_and_errors(y, x, minimum, maximum, metric)

    counts, xedges, yedges, im1 = axes[0, iteration].hist2d(
        x,
        y,
        bins=100,
        cmin=1,
        range=[[minimum, maximum], [minimum, maximum]],
        cmap="viridis",
        vmax=prediction_H,
    )
    axes[0, iteration].plot(line, line, marker="", color="tab:red", linestyle="--")
    axes[0, iteration].errorbar(
        centers,
        means,
        xerr=(centers[1] - centers[0]) / 2,
        yerr=errors,
        linestyle="",
        color="black",
        capsize=1,
        marker=".",
        label="IQR",
    )
    axes[0, iteration].legend()

    x = inference_type["energy_log10_true"].values.flatten()
    y = inference_type["error"].values.flatten()
    if metric == "energy_log10":
        y_minimum = -0.8
        y_maximum = 0.8
    elif metric == "zenith":
        y_minimum = -100
        y_maximum = 100
    no_clipped_x = np.sum(np.logical_or(x < min_e, x > max_e))
    no_clipped_y = np.sum(np.logical_or(y < y_minimum, y > y_maximum))
    no_clipped = no_clipped_x + no_clipped_y
    pct_clipped = (no_clipped / no_events) * 100
    x = np.clip(x, min_e, max_e)
    y = np.clip(y, y_minimum, y_maximum)

    centers, means, errors = means_and_errors(y, x, min_e, max_e, metric)

    counts, xedges, yedges, im2 = axes[1, iteration].hist2d(
        x,
        y,
        bins=100,
        range=[[min_e, max_e], [y_minimum, y_maximum]],
        cmin=1,
        cmap="Oranges",
        vmax=error_H,
    )
    # axes[1, iteration].grid(b=True, which="both", axis="y")
    axes[1, iteration].axhline(
        y=0, marker="", color="grey", linewidth=1, linestyle="dashed"
    )
    axes[1, iteration].plot(
        centers,
        means,
        linestyle="-",
        linewidth=1,
        color="red",
        marker="",
        label="Median",
    )
    axes[1, iteration].plot(
        centers,
        np.array(means) - errors[0, :],
        linestyle="dotted",
        linewidth=1,
        color="red",
        marker="",
        label="$1 \\sigma$",
    )
    axes[1, iteration].plot(
        centers,
        np.array(means) + errors[1, :],
        linestyle="dotted",
        linewidth=1,
        color="red",
        marker="",
    )
    axes[1, iteration].legend()

    axes[0, 0].set(ylabel=get_prediction_label(metric)[1])
    axes[0, 0].set(xlabel=get_prediction_label(metric)[0])
    axes[0, 1].set(xlabel=get_prediction_label(metric)[0])
    axes[1, 0].set(ylabel=get_error_ylabel(metric))
    axes[1, 0].set(xlabel=get_prediction_label("energy_log10")[0])
    axes[1, 1].set(xlabel=get_prediction_label("energy_log10")[0])
    axes[0, 0].set_title(f"CubeFlow", loc="left", fontsize=10)
    axes[0, 1].set_title(f"Retro Reco", loc="left", fontsize=10)

for ax in axes.flatten():
    # ax.set_aspect("equal")
    # ax.xaxis.set_tick_params(labelbottom=True)
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))

fig.suptitle(
    f"{metric.strip('_log10').capitalize()} reconstruction and error histograms",
    fontsize=12,
    x=0.1,
    ha="left",
)
fig.subplots_adjust(hspace=0.2)
fig.align_labels()
fig.tight_layout()

cbar1 = fig.colorbar(im1, ax=[axes[0, 0], axes[0, 1]])
cbar1.set_label("Events")
cbar2 = fig.colorbar(im2, ax=[axes[1, 0], axes[1, 1]])
cbar2.set_label("Events")

# fig.savefig(str(out_file), bbox_inches="tight")
fig.savefig(str(out_file))
