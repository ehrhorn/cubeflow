from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np

output_file = (
    Path().home()
    / "Developer"
    / "thesis"
    / "images"
    / "machine_learning"
    / "activation_functions.pgf"
)

x = np.linspace(-6, 6, 1000)
sigmoid = np.exp(x) / (np.exp(x) + 1)
rectifier = np.maximum(x, 0)

fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(6.2012, 2))
axes[0].plot(x, sigmoid, marker="", label="Sigmoid")
axes[0].set(title="Sigmoid activation function", xlabel="$x$", ylabel="$S(x)$")
axes[0].grid(alpha=0.2)
axes[1].plot(x, rectifier, marker="", label="Rectifier")
axes[1].set(title="Rectifier activation function", xlabel="$x$", ylabel="$f(x)$")
axes[1].grid(alpha=0.2)
fig.tight_layout()
fig.savefig(output_file)
