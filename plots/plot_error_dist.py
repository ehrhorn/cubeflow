import sqlite3
from pathlib import Path

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats as stats
from bootstrap_stat import bootstrap_stat as bp
from distfit import distfit
from matplotlib.font_manager import FontProperties
from scipy.stats import iqr, kstest

font = FontProperties()
font.set_family("monospace")
# warnings.filterwarnings("ignore", category=FutureWarning)
# warnings.filterwarnings("ignore", category=UserWarning)
mpl.rcParams["axes.unicode_minus"] = False


def smallest_angle_difference(x, y):
    first = (360) - np.absolute(x - y)
    second = np.absolute(x - y)
    out = np.minimum(first, second)
    return out


def custom_iqr(x):
    return iqr(x, scale="normal")


def get_suptitle(run, variable):
    suptitle = f"Run {run} zenith error distribution in selected energy bins"
    return suptitle


def get_title(energy_bin, no_events):
    title = f"Bin [{energy_bin[0]}, {energy_bin[1]}), {no_events:,} events"
    return title


def get_xlabel(variable):
    if variable == "energy_log10":
        xlabel = "$ \\log_{10} \\left( E_{\\textup{true}} \\right) - \\log_{10} \\left( E_{\\textup{reco}} \\right) \\, \\left[ E / \\textup{GeV} \\right] $"
    elif variable == "zenith":
        xlabel = "$ \\theta_{\\textup{true}} - \\theta_{\\textup{reco}} \\, [\\textup{rad}] $"
    elif variable == "azimuth":
        xlabel = (
            "$ \\phi_{\\textup{true}} - \\phi_{\\textup{reco}} \\, [\\textup{rad}] $"
        )
    elif variable == "time":
        xlabel = "$ t_{\\textup{true}} - t_{\\textup{reco}} \\, [\\textup{ns}] $"
    else:
        xlabel = None
    return xlabel


# runs = ["2012011907", "2012011907"]
runs = ["2011300821"]
variables = ["zenith"]
out_type = "pgf"
energy_bins = [(1.0, 1.2), (1.7, 1.8)]

if out_type == "pdf":
    output_file = Path().home() / "plots" / "error_distribution.pdf"
else:
    output_file = Path().home() / "plots" / "error_distribution.pgf"

fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(6.2012, 1 * 2.5), sharey="row")

for row, (run, variable) in enumerate(zip(runs, variables)):
    runs_path = Path().home() / "work" / "erda_runs" / "done"
    run_path = [f for f in runs_path.iterdir() if f.is_dir() and run in f.name]
    run_name = run_path[0].name

    dataset_name = run_name.split(".")[0]
    dataset_root = Path().home() / "work" / "erda_datasets" / dataset_name
    db_path = dataset_root / "predictions" / f"predictions.db"
    db_uri = f"file:{db_path}?mode=ro"

    query = f"select event_no from '{run_name}'"
    with sqlite3.connect(db_uri, uri=True) as con:
        event_nos = pd.read_sql(query, con).values.flatten().tolist()

    stringified_events_nos = ", ".join([str(event_no) for event_no in event_nos])

    for iteration, energy_bin in enumerate(energy_bins):
        if iteration > 0:
            ylabel = None
            label_data = None
            label_fit = None
        else:
            ylabel = "Density"
            label_data = "Data"
            label_fit = "Fit"
        if row > 0:
            label_data = None
            label_fit = None
        query = f"select event_no, {variable} from truth where event_no in ({stringified_events_nos}) and energy_log10 >= {energy_bin[0]} and energy_log10 < {energy_bin[1]}"
        with sqlite3.connect(db_uri, uri=True) as con:
            truth = pd.read_sql(query, con)
        if variable == "zenith" or variable == "azimuth":
            truth[variable] = truth[variable].apply(np.rad2deg)

        temp_stringified_events_nos = ", ".join(
            [str(event_no) for event_no in truth["event_no"].tolist()]
        )
        query = f"select event_no, {variable} from '{run_name}' where event_no in ({temp_stringified_events_nos})"
        with sqlite3.connect(db_uri, uri=True) as con:
            reco = pd.read_sql(query, con)

        merged = truth.merge(reco, on="event_no", suffixes=("_true", "_reco"))
        if variable == "zenith":
            errors = merged[f"{variable}_true"] - merged[f"{variable}_reco"]
        elif variable == "azimuth":
            errors = smallest_angle_difference(
                merged[f"{variable}_true"], merged[f"{variable}_reco"]
            )
        no_events = len(errors)

        # dist = bp.EmpiricalDistribution(errors)
        # mean = custom_iqr(errors)
        # ci_low, ci_high = bp.bcanon_interval(dist, custom_iqr, errors, num_threads=20)
        if variable == "zenith":
            percentile_low = np.percentile(errors, 25)
            percentile_high = np.percentile(errors, 75)
            axes[iteration].text(
                0.0,
                0.005,
                "IQR",
                horizontalalignment="center",
                verticalalignment="center",
                # transform=axes[iteration].transAxes,
            )
        elif variable == "azimuth":
            percentile_low = 0
            percentile_high = np.percentile(errors, 68)
            axes[iteration].text(
                10,
                0.002,
                "68th pctl",
                horizontalalignment="left",
                verticalalignment="center",
                # transform=axes[iteration].transAxes,
            )

        axes[iteration].axvspan(
            percentile_low, percentile_high, alpha=0.2, facecolor="grey"
        )
        axes[iteration].hist(
            errors, bins="auto", histtype="step", density=True, label=label_data
        )
        axes[iteration].set(
            xlabel=get_xlabel(variable),
            ylabel=ylabel,
        )
        axes[iteration].set_title(
            label=get_title(energy_bin, no_events),
            loc="left",
            fontsize=10,
            # fontproperties=font,
        )
        if variable == "zenith":
            x = np.linspace(errors.min(), errors.max(), 1000)
            dist = distfit(distr="norm")
            fitted_dist = dist.fit_transform(errors)
            loc, scale = fitted_dist["model"]["loc"], fitted_dist["model"]["scale"]
            fit = stats.norm.pdf(x, loc, scale)
            statistic, pvalue = kstest(
                rvs=errors.values.flatten().tolist(), cdf="norm", args=(loc, scale)
            )
            # axes[iteration].plot(
            #     x, fit, marker="", ls="dashed", c="black", label=label_fit
            # )

# fig.legend()
fig.suptitle(get_suptitle(run, variable), fontsize=12, x=0.1, ha="left")
fig.tight_layout()
fig.savefig(str(output_file))
