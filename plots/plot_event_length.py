import sqlite3
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from KDEpy import FFTKDE
from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle
from matplotlib.ticker import MaxNLocator
from scipy.stats import iqr

pd.set_option("mode.use_inf_as_na", True)

limit = -1
dataset_name = "dev_genie_numu_cc_train_retro_004"
output_type = "pdf"
file_name = "event_length"

db_path = (
    Path().home()
    / "work"
    / "erda_datasets"
    / dataset_name
    / "data"
    / f"{dataset_name}.db"
)
db_uri = f"file:{db_path}?mode=ro"

if output_type == "pdf":
    output_file = Path().home() / "plots" / f"{file_name}.pdf"
else:
    output_file = Path().home() / "plots" / f"{file_name}.pgf"

figsize = (0.49 * 6.2012, 1 * 2 + 0.5)
fig, ax = plt.subplots(figsize=figsize)

query = f"select sum(SplitInIcePulses) as raw, sum(SRTInIcePulses) as clean from features group by event_no limit {limit}"
with sqlite3.connect(db_uri, uri=True) as con:
    lengths = pd.read_sql(query, con)

median1 = np.median(lengths["raw"].values.flatten())
median2 = np.median(lengths["clean"].values.flatten())
x1, y1 = FFTKDE(kernel="gaussian", bw=1).fit(lengths["raw"].values.flatten()).evaluate()
x2, y2 = (
    FFTKDE(kernel="gaussian", bw=1).fit(lengths["clean"].values.flatten()).evaluate()
)
# ax.hist(
#     lengths["raw"],
#     bins="auto",
#     histtype="step",
#     label="Raw",
#     density=True,
# )
# ax.hist(
#     lengths["clean"],
#     bins="auto",
#     histtype="step",
#     label="Clean",
#     density=True,
# )
ax.plot(x1, y1, marker="", label=f"Raw, median={median1}")
ax.plot(x2, y2, marker="", label=f"Cleaned, median={median2}")
ax.set_title(
    f"Event lengths",
    fontsize=12,
    loc="left",
)
# fig.suptitle(
#     suptitle,
#     fontsize=12,
#     x=0.1,
#     ha="left",
# )
ax.set_ylabel(ylabel="Density")
ax.set_xlabel(xlabel="Event length")
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
ax.yaxis.set_major_locator(MaxNLocator(integer=True))
ax.legend()

fig.tight_layout()
fig.savefig(str(output_file))
