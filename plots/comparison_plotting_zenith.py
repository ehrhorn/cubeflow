from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

metric = "zenith"
output_type = "pdf"
file_name = f"{metric}_performance_upgrade"
title = f"{metric.capitalize()} performance, Upgrade"

if output_type == "pdf":
    output_file = Path().home() / f"{file_name}.pdf"
else:
    output_file = (
        Path().home()
        / "Developer"
        / "thesis"
        / "images"
        / "design"
        / f"{file_name}.pgf"
    )

print(f"{datetime.now()}: Calculating performances on selected runs")

runs_root = Path().home() / "work" / "plots" / "runs"

runs = [
    # "dev_upgrade_train_step4_002.direction.model_1.SRTInIcePulses.Adam.logcosh.None.5.256.200.2011120723",
    # "dev_upgrade_train_step4_005.direction.model_1.SRTInIcePulses.Adam.logcosh.None.5.256.200.2011140753",
    # "dev_upgrade_train_step4_005.direction.model_1.SRTInIcePulses.Adam.cosinesim_unitpen.None.5.256.200.2011151822",
    "dev_upgrade_train_step4_005.angle.model_1.SRTInIcePulses.Adam.logcosh.None.5.256.200.2011181726",
    "dev_upgrade_numu_cc_train_000.zenith.model_2.SRTInIcePulses.Adam.logcosh.weight_uniform.5.256.200.2012051559",
]
labels = [
    # "model_1, dataset 002",
    # "model_1, dataset 005",
    # "model_1, dataset 005, cosine_unitpen",
    "model_1, dataset 005, angle",
    "model_2, dataset 000, zenith",
]

# runs = [loss_runs]
# labels = [loss_labels]
# nrows = int(np.ceil(len(runs) / 2.0))
# ncols = 1 if len(runs) == 1 else 2
ncols = 1
nrows = 1
figsize = (1 * 6.2012, 1 * 2 + 0.5)

fig, ax = plt.subplots(ncols=ncols, nrows=nrows, figsize=figsize)
for run, label in zip(runs, labels):
    performance_file = runs_root / run / f"{metric}_performance.csv"
    performance = pd.read_csv(performance_file)
    ax.errorbar(
        performance["centers"].values,
        performance["means"].values,
        yerr=np.array((performance["lows"].values, performance["highs"].values)),
        # xerr=(performance["centers"].values[1] - performance["centers"].values[0]) / 2,
        # linestyle="",
        marker="",
        linewidth=1.5,
        capsize=1,
        label=label,
        alpha=0.9,
    )
xlabel = "$\\log_{10}(E_{\\textup{true}}) \\, \\left[ E / \\textup{GeV} \\right]$"
if metric == "energy":
    ylabel = "IQR / 1.349 $\\left[ E / \\textup{GeV} \\right]$"
if (
    metric == "direction"
    or metric == "angle"
    or metric == "zenith"
    or metric == "azimuth"
):
    if metric == "angle" or metric == "azimuth":
        ylabel = "68th %ile $\\left[ \\textup{deg} \\right]$"
    else:
        ylabel = "IQR / 1.349 $\\left[ \\textup{deg} \\right]$"
ax.set(xlabel=xlabel, ylabel=ylabel)
# ax.set_title(title, loc="left")
ax.legend()
ax.set_title(
    title,
    fontsize=12,
    loc="left",
)
ax.grid(b=True, which="both", axis="both", linestyle="dotted", linewidth=0.5, alpha=0.5)
fig.tight_layout()
fig.savefig(str(output_file))
plt.close()
