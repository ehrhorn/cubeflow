import sqlite3
from datetime import datetime
from os import PathLike
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from KDEpy import FFTKDE

limit = -1
dataset_name = "dev_upgrade_train_step4_002"
dataset_db = (
    Path().home() / "work" / "datasets" / dataset_name / "data" / (dataset_name + ".db")
)
meta_db = Path().home() / "work" / "datasets" / "meta.db"

output_path = Path().home() / "plots" / "datasets" / dataset_name
output_path.mkdir(exist_ok=True, parents=True)
output_file = output_path / "dom_distribution.pdf"

query = f"select event_no from truth limit {limit}"
print(f"{datetime.now()}: Fetching event numbers from dataset")
uri = f"file:{str(dataset_db)}?mode=ro"
with sqlite3.connect(uri, uri=True) as con:
    event_nos = pd.read_sql(query, con)

stringed_event_nos = ", ".join(
    [str(event_no[0]) for event_no in event_nos.values.tolist()]
)

query = (
    f"select event_no, raw_doms, clean_doms from meta where event_no in ({stringed_event_nos})"
)
print(f"{datetime.now()}: Fetching doms from meta")
with sqlite3.connect(str(meta_db)) as con:
    doms = pd.read_sql(query, con)
print(f"{datetime.now()}: Plotting")

no_events = len(doms)

fig, ax = plt.subplots()
for clean_type, clean_name in zip(['raw_doms', 'clean_doms'], ['Raw', 'SRT']):
    x, y = FFTKDE(bw="silverman").fit(doms[clean_type].values).evaluate(2 ** 10)
    ax.plot(
        x, y, marker="", label=clean_name
    )
ax.set(
    xlabel="Number of DOMs in event", ylabel="Density",
)
ax.set_ylim(ymin=0)
ax.set_title(
    label=(f"{no_events:,} events"), loc="left",
)
ax.legend()
fig.suptitle(f"{dataset_name} DOM distribution", fontsize=16, x=0.1, ha="left")
fig.savefig(str(output_file))

print(f"{datetime.now()}: Done")
