import pickle
import sqlite3
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def transform_data(transformers_path, data):
    with open(str(transformers_path), "rb") as f:
        transformers = pickle.load(f)
    for column in data.columns:
        if column in transformers["truth"]:
            data[column] = transformers["truth"][column].inverse_transform(
                data[column].values.reshape(-1, 1)
            )
    return data


dataset_name = "dev_genie_numu_cc_train_retro_005"
weight_type = "weight_uniform"
output_type = "pgf"
file_name = f"{dataset_name}_weighted_distribution"
title = f"Weighted energy distribution"

if output_type == "pdf":
    output_file = Path().home() / "plots" / f"{file_name}.pdf"
else:
    output_file = Path().home() / "plots" / f"{file_name}.pgf"

print(f"{datetime.now()}: Calculating distribution plots")

dataset_root = Path().home() / "work" / "erda_datasets" / dataset_name
db_path = dataset_root / "data" / f"{dataset_name}.db"
db_uri = f"file:{db_path}?mode=ro"
meta_root = dataset_root / "meta"
weight_path = meta_root / f"{weight_type}.pkl"
transformers_path = dataset_root / "meta" / "transformers.pkl"
with open(weight_path, "rb") as f:
    weights = pickle.load(f)
weights = pd.DataFrame({"event_no": weights[:, 0], "weight": weights[:, 1]})

query = "select event_no, energy_log10 from truth"
with sqlite3.connect(db_uri, uri=True) as con:
    energy = pd.read_sql(query, con)
energy = transform_data(transformers_path, energy)

data = energy.merge(weights, on="event_no")

figsize = (0.49 * 6.2012, 1 * 2 + 0.5)

fig, ax = plt.subplots(figsize=figsize)
ax.hist(
    data["energy_log10"].values,
    bins=200,
    histtype="step",
    label="Unweighted",
    density=True,
)
ax.hist(
    data["energy_log10"].values,
    bins=200,
    histtype="step",
    label="Weighted",
    weights=data["weight"].values,
    density=True,
)
xlabel = "$\\log_{10}(E_{\\textup{true}}) \\, \\left[ E / \\textup{GeV} \\right]$"
ylabel = "Density"
ax.set(xlabel=xlabel, ylabel=ylabel)
ax.set_title(title, loc="left")
ax.legend()
ax.grid(b=True, which="both", axis="both", linestyle="dotted", linewidth=0.5, alpha=0.5)
fig.tight_layout()
fig.savefig(str(output_file))
plt.close()
