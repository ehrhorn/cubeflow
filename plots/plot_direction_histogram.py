import pickle
import sqlite3
import warnings
from datetime import datetime
from pathlib import Path

import healpy as hp
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.neighbors import KernelDensity

warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=UserWarning)


def spherical2hpx(azimuth, zenith, nside):
    npix = hp.nside2npix(nside)
    indices = hp.ang2pix(nside, zenith, azimuth)
    idx, counts = np.unique(indices, return_counts=True)
    hpx_map = np.zeros(npix, dtype=int)
    hpx_map[idx] = counts
    return hpx_map


dataset_name = "dev_upgrade_train_step4_002"
table = "truth"
x = "azimuth"
y = "zenith"
limit = -1
query = f"select event_no, {x}, {y} from {table} limit {limit}"

title = f"Neutrino directions"
xlabel = f"{x.capitalize()} (rad)"
ylabel = f"{y.capitalize()} (rad)"

db = (
    Path().home() / "work" / "datasets" / dataset_name / "data" / (dataset_name + ".db")
)
transformer_file = (
    Path().home() / "work" / "datasets" / dataset_name / "meta" / "transformers.pkl"
)
with open(transformer_file, "rb") as f:
    transformers = pickle.load(f)
output_path = Path().home() / "plots" / "datasets" / dataset_name
output_path.mkdir(exist_ok=True, parents=True)

print(f"{datetime.now()}: Fetching events")
uri = f"file:{str(db)}?mode=ro"
with sqlite3.connect(uri, uri=True) as con:
    dataframe = pd.read_sql(query, con)

no_events = len(dataframe)

for metric in [x, y]:
    try:
        print(f"{datetime.now()}: Transforming {metric}")
        transformed_metric = (
            transformers[table][metric]
            .inverse_transform(dataframe[metric].values.reshape(-1, 1))
            .flatten()
        )
        dataframe[metric] = transformed_metric
    except Exception:
        print(f"{datetime.now()}: No transformers for {metric}. Proceeding...")

xmin, xmax = dataframe[x].min(), dataframe[x].max()
ymin, ymax = dataframe[y].min(), dataframe[y].max()

hpx_map = spherical2hpx(dataframe[x], dataframe[y], nside=64)

fig, axes = plt.subplots(nrows=2, ncols=1)
plt.axes(axes.flatten()[0])
hp.mollview(
    hpx_map,
    cmap="viridis",
    title=f"{no_events:,} events",
    unit="",
    norm="hist",
    cbar=False,
    hold=True,
)
plt.axes(axes.flatten()[1])
hp.orthview(
    hpx_map,
    rot=[0, 90],
    cmap="viridis",
    title="",
    unit="",
    norm="hist",
    cbar=False,
    hold=True,
)
hp.graticule(color="black")
axes.flatten()[0].set(
    title=title + f" ({no_events:,} events)", xlabel=xlabel, ylabel=ylabel,
)
fig.suptitle(
    f"{dataset_name} direction distribution", fontsize=16, x=0.5, ha="center",
)
fig.savefig(output_path / ("direction_distribution.pdf"))

print(f"{datetime.now()}: Done.")
