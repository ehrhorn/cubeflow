import pickle
import sqlite3
import warnings
from datetime import datetime
from pathlib import Path

import healpy as hp
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.font_manager import FontProperties
from sklearn.neighbors import KernelDensity

font = FontProperties()
font.set_family("monospace")
warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=UserWarning)


def stringify_expression(input_list):
    output_string = ", ".join([str(entry) for entry in input_list])
    return output_string


def get_xlabel(variable):
    if variable == "energy_log10":
        xlabel = "$ \\log_{10} \\left( E_{\\textup{true}} \\right) \\, \\left[ E / \\textup{GeV} \\right] $"
    elif variable == "zenith":
        xlabel = "$ \\theta_{\\textup{true}} \\, [\\textup{rad}] $"
    elif variable == "azimuth":
        xlabel = "$ \\phi_{\\textup{true}} \\, [\\textup{rad}] $"
    elif variable == "time":
        xlabel = "$t_{\\textup{true}} \\, [\\textup{ns}] $"
    else:
        xlabel = None
    return xlabel


def get_title(dataset_name, no_events):
    if "genie" in dataset_name:
        title_name = "DeepCore dataset"
    elif "upgrade" in dataset_name:
        title_name = "Upgrade dataset"
    title = f"{title_name} truth distributions, {no_events:,} events"
    return title


def get_file_name(dataset_name, file_type):
    if "genie" in dataset_name:
        file_name = f"deepcore_truth_distributions.{file_type}"
    elif "upgrade" in dataset_name:
        file_name = f"upgrade_truth_distributions.{file_type}"
    return file_name


def transform_data(transformers_path, data):
    with open(str(transformers_path), "rb") as f:
        transformers = pickle.load(f)
    for column in data.columns:
        if column in transformers["truth"]:
            data[column] = transformers["truth"][column].inverse_transform(
                data[column].values.reshape(-1, 1)
            )
    return data


limit = -1
variables = ["energy_log10", "time", "zenith", "azimuth"]
file_type = "pdf"

dataset_name = "dev_genie_numu_cc_train_retro_004"
dataset_root = Path().home() / "work" / "datasets" / dataset_name
dataset_db = dataset_root / "data" / (dataset_name + ".db")
transformers_path = dataset_root / "meta" / "transformers.pkl"

output_path = Path().home() / "plots" / "datasets" / dataset_name
output_path.mkdir(exist_ok=True, parents=True)
output_file = output_path / get_file_name(dataset_name, file_type)

query = f"select {stringify_expression(variables)} from truth limit {limit}"
print(f"{datetime.now()}: Fetching data")
uri = f"file:{str(dataset_db)}?mode=ro"
with sqlite3.connect(uri, uri=True) as con:
    data = pd.read_sql(query, con)

data = transform_data(transformers_path, data)

no_events = len(data)

if len(variables) > 1:
    ncols = 2
    nrows = int(np.ceil(len(variables) / ncols))
else:
    ncols = 1
    nrows = 1

height = 2 * nrows + 0.5
fig, axes = plt.subplots(
    ncols=ncols, nrows=nrows, sharey=False, figsize=(6.2012, height)
)
if len(variables) == 1:
    axes = np.array(axes)
for iteration, (variable, ax) in enumerate(zip(variables, axes.flatten())):
    if iteration % 2 == 0:
        ylabel = "Events"
    else:
        ylabel = "Events"
    ax.hist(data[variable].values, bins="auto", histtype="step", density=False)
    ax.set(
        xlabel=get_xlabel(variable),
        ylabel=ylabel,
    )
    ax.set_title(label=(f"{variable}"), loc="left", fontproperties=font)
fig.suptitle(get_title(dataset_name, no_events), fontsize=12, x=0.1, ha="left")
fig.tight_layout()
fig.savefig(str(output_file))

print(f"{datetime.now()}: Done")
