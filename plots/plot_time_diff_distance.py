from datetime import datetime
from operator import sub
from pathlib import Path
import pickle
import sqlite3
import warnings

import matplotlib.pyplot as plt
import numpy as np
from numpy.core.defchararray import split
import pandas as pd
import scipy.stats as st
from sklearn.neighbors import KernelDensity

warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=UserWarning)


def kde2D(x, y, bandwidth, xbins=100j, ybins=100j, **kwargs):
    """Build 2D kernel density estimate (KDE)."""

    # create grid of sample locations (default: 100x100)
    xx, yy = np.mgrid[x.min() : x.max() : xbins, y.min() : y.max() : ybins]

    xy_sample = np.vstack([yy.ravel(), xx.ravel()]).T
    xy_train = np.vstack([y, x]).T

    kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
    kde_skl.fit(xy_train)

    # score_samples() returns the log-likelihood of the samples
    z = np.exp(kde_skl.score_samples(xy_sample))
    return xx, yy, np.reshape(z, xx.shape)


dataset_name = "dev_nue_train_l5_000"
limit = int(-1)

title = f"Distance vs. time"
xlabel = "$t_{\\text{DOM}} - t_{\\text{int}}$"
ylabel = "dist(DOM, int)"

db = (
    Path().home() / "work" / "datasets" / dataset_name / "data" / (dataset_name + ".db")
)
transformer_file = (
    Path().home() / "work" / "datasets" / dataset_name / "meta" / "transformers.pkl"
)
with open(transformer_file, "rb") as f:
    transformers = pickle.load(f)
plot_out_path = Path().home() / "plots" / dataset_name
plot_out_path.mkdir(exist_ok=True)

truth_query = f"select event_no, time, position_x, position_y, position_z from truth limit {limit}"

with sqlite3.connect(str(db)) as con:
    truth_dataframe = pd.read_sql(truth_query, con)

for key in truth_dataframe.columns:
    try:
        transformed_metric = (
            transformers["truth"][key]
            .inverse_transform(truth_dataframe[key].values.reshape(-1, 1))
            .flatten()
        )
        truth_dataframe[key] = transformed_metric
    except Exception:
        continue

no_events = len(truth_dataframe["event_no"].unique())
splits = int(no_events // 150)
events = truth_dataframe["event_no"].values
split_events = np.array_split(events, splits)
x = []
y = []
done_events = 0

for j, sublist in enumerate(split_events):
    joined_events = ", ".join([str(event_no) for event_no in sublist])
    feature_query = f"select event_no, dom_x, dom_y, dom_z, time from features where event_no in ({joined_events})"
    with sqlite3.connect(str(db)) as con:
        feature_dataframe = pd.read_sql(feature_query, con)
    for key in feature_dataframe.columns:
        try:
            transformed_metric = (
                transformers["features"][key]
                .inverse_transform(feature_dataframe[key].values.reshape(-1, 1))
                .flatten()
            )
            feature_dataframe[key] = transformed_metric
        except Exception:
            continue
    for i, event in enumerate(sublist):
        temp_features = feature_dataframe[feature_dataframe["event_no"] == event].copy()
        temp_truth = truth_dataframe[truth_dataframe["event_no"] == event].copy()
        position_x = temp_truth["position_x"].values[0]
        position_y = temp_truth["position_y"].values[0]
        position_z = temp_truth["position_z"].values[0]
        int_time = temp_truth["time"].values[0]
        temp_features["position_x"] = position_x
        temp_features["position_y"] = position_y
        temp_features["position_z"] = position_z
        temp_features["int_time"] = int_time
        time_diff = temp_features["time"].values - temp_features["int_time"].values
        x.extend(time_diff.tolist())
        dist = np.linalg.norm(
            temp_features[["dom_x", "dom_y", "dom_z"]].values
            - temp_features[["position_x", "position_y", "position_z"]].values,
            axis=1,
        )
        y.extend(dist.tolist())
        done_events += 1
    if j % int(1e2) == 0:
        print(
            f"{datetime.now()}: Handled {done_events} events, {no_events - done_events} to go..."
        )


x = np.array(x)
y = np.array(y)

icecube_strings = np.arange(1, 79).tolist()
deep_core_inner = np.arange(79, 87).tolist() + [36]
deep_core_outer = deep_core_inner + [26, 27, 35, 37, 45, 46]
upgrade_inner = np.arange(87, 94).tolist()
upgrade_outer = upgrade_inner + deep_core_outer
all_strings = np.arange(1, 94).tolist()

volumes = [all_strings]
names = ["Whole detector"]
colors = ["blue"]
plot_types = ["hist", "kde"]

for plot_type in plot_types:
    fig, ax = plt.subplots()
    for volume, name, color in zip(volumes, names, colors):
        if plot_type == "hist":
            print(f"{datetime.now()}: Plotting 2D histogram...")
            xmin, xmax = min(x), max(x)
            ymin, ymax = min(y), max(y)
            ax.hist2d(x, y, bins=200, cmin=1)
        elif plot_type == "kde":
            print(f"{datetime.now()}: Plotting KDE...")
            xmin, xmax = -1500, 2500
            ymin, ymax = -200, 400
            xx, yy = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]
            positions = np.vstack([xx.ravel(), yy.ravel()])
            values = np.vstack([x, y])
            kernel = st.gaussian_kde(values)
            f = np.reshape(kernel(positions).T, xx.shape)
            zz = (f - f.min()) / (f.max() - f.min())
            cfset = ax.contourf(xx, yy, zz, cmap="Blues")
            cset = ax.contour(xx, yy, zz, colors="k")
            # ax.scatter(x, y, s=2, facecolor="white")
            # ax.pcolormesh(xx, yy, zz)
            ax.clabel(cset, inline=1, fontsize=4)
    ax.set(
        title=title + f" ({no_events:,} events)",
        xlabel=xlabel,
        ylabel=ylabel,
    )
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    # ax.text(
    #     0.1,
    #     0.9,
    #     f"Events: {no_events:,}",
    #     transform=ax.transAxes,
    #     color="black",
    #     bbox=dict(facecolor="none", edgecolor="black", boxstyle="round,pad=1"),
    # )
    fig.savefig(plot_out_path / ("time_vs_distance_" + plot_type + ".pdf"))

print(f"{datetime.now()}: Done.")