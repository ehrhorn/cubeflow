import argparse
import copy
import json
import pickle
import sqlite3
from datetime import datetime
from pathlib import Path
from shutil import copyfile

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import vg
from bootstrap_stat import bootstrap_stat as bp
from matplotlib import cm
from matplotlib.pyplot import plot
from scipy.stats import iqr

pd.set_option("use_inf_as_na", True)


def convert_spherical_to_cartesian(zenith, azimuth):
    """Convert spherical coordinates to Cartesian coordinates.

    Assumes unit length.

    Zenith: theta
    Azimuth: phi

    Args:
        zenith (numpy.ndarray): zenith/polar angle
        azimuth (numpy.ndarray): azimuthal angle

    Returns:
        numpy.ndarray: x, y, z (event, coordinates) vector
    """
    theta = zenith
    phi = azimuth
    x = np.sin(np.deg2rad(theta)) * np.cos(np.deg2rad(phi))
    y = np.sin(np.deg2rad(theta)) * np.sin(np.deg2rad(phi))
    z = np.cos(np.deg2rad(theta))
    vectors = np.array((x, y, z)).T
    return vectors


def smallest_angle_difference(x, y):
    first = (360) - np.absolute(x - y)
    second = np.absolute(x - y)
    out = np.minimum(first, second)
    return out


def get_event_nos(dataset_name, set_type):
    dataset_path = Path().home() / "work" / "erda_datasets" / dataset_name
    sets_path = dataset_path / "meta" / "sets.pkl"
    with open(str(sets_path), "rb") as f:
        sets = pickle.load(f)
    return sets[set_type]


def energy_plotting(
    run, plots, save_path, metric, event_nos, retro_db_uri, truth_db_uri
):
    variables = ["energy_log10"]
    print(f"{datetime.now()}: Fetching data")
    query = (
        f"select event_no, energy_log10 from retro_reco where event_no in ({event_nos})"
    )
    with sqlite3.connect(retro_db_uri, uri=True) as con:
        retro = pd.read_sql(query, con)
    retro.dropna(inplace=True)
    event_nos = retro["event_no"].values.flatten().tolist()
    event_nos = ", ".join([str(event_no) for event_no in event_nos])
    query = f"select event_no, energy_log10 from truth where event_no in ({event_nos})"
    with sqlite3.connect(truth_db_uri, uri=True) as con:
        truth = pd.read_sql(query, con)
    data = truth.merge(retro, on="event_no", suffixes=("_truth", "_prediction"))
    for variable in variables:
        output_path = save_path / variable
        output_path.mkdir(exist_ok=True)
        for plot in plots:
            print(f"{datetime.now()}: Plotting {plot} for {variable}")
            if plot == "prediction_2d_histogram":
                x = data["energy_log10_prediction"].values
                y = data["energy_log10_truth"].values
                prediction_2d_histogram(x, y, output_path, variable, run)
                x = data["energy_log10_prediction"].values
                y = data["energy_log10_truth"].values
                prediction_1d_histogram(x, y, output_path, variable, run)
            elif plot == "resolution_1d_histogram":
                x = (
                    data["energy_log10_truth"].values
                    - data["energy_log10_prediction"].values
                )
                y = data["energy_log10_truth"].values
                centers, means, errors = resolution_1d_histogram(
                    x, y, save_path, variable, run
                )
                out_dict = {
                    "centers": np.array(centers),
                    "means": np.array(means),
                    "lows": errors[0, :],
                    "highs": errors[1, :],
                }
                out_df = pd.DataFrame(data=out_dict)
                out_df.to_csv(str(save_path / "energy_performance.csv"), index=False)
            elif plot == "error_2d_histogram":
                x = (
                    data["energy_log10_truth"].values
                    - data["energy_log10_prediction"].values
                )
                y = data["energy_log10_truth"].values
                error_2d_histogram(x, y, output_path, variable, run)


def direction_plotting(
    run, plots, save_path, metric, event_nos, retro_db_uri, truth_db_uri
):
    variables = ["azimuth", "zenith"]
    print(f"{datetime.now()}: Fetching data")
    query = f"""
SELECT
    event_no,
    azimuth,
    zenith
FROM
    retro_reco
WHERE
    event_no in({event_nos})
    """
    with sqlite3.connect(retro_db_uri, uri=True) as con:
        retro = pd.read_sql(query, con)
    retro.dropna(inplace=True)
    event_nos = retro["event_no"].values.flatten().tolist()
    event_nos = ", ".join([str(event_no) for event_no in event_nos])
    query = f"""
SELECT
    event_no,
    energy_log10 AS energy_log10_truth,
    azimuth,
    zenith
FROM
    truth
WHERE
    event_no in({event_nos})
    """
    with sqlite3.connect(truth_db_uri, uri=True) as con:
        truth = pd.read_sql(query, con)
    data = truth.merge(retro, on="event_no", suffixes=("_truth", "_prediction"))
    if np.amax(data["azimuth_truth"].values) < 7:
        data["azimuth_truth"] = data["azimuth_truth"].apply(np.rad2deg)
    if np.amax(data["zenith_truth"].values) < 4:
        data["zenith_truth"] = data["zenith_truth"].apply(np.rad2deg)
    if np.amax(data["azimuth_prediction"].values) < 7:
        data["azimuth_prediction"] = data["azimuth_prediction"].apply(np.rad2deg)
    if np.amax(data["zenith_prediction"].values) < 4:
        data["zenith_prediction"] = data["zenith_prediction"].apply(np.rad2deg)
    for variable in variables:
        if "energy_log10" in variable:
            continue
        else:
            output_path = save_path / variable
            output_path.mkdir(exist_ok=True)
            for plot in plots:
                print(f"{datetime.now()}: Plotting {plot} for {variable}")
                if plot == "prediction_2d_histogram":
                    x = data[variable + "_prediction"].values
                    y = data[variable + "_truth"].values
                    prediction_2d_histogram(x, y, output_path, variable, run)
                elif plot == "prediction_1d_histogram":
                    extra_data = None
                    x = data[variable + "_prediction"].values
                    y = data[variable + "_truth"].values
                    prediction_1d_histogram(
                        x, y, output_path, variable, run, extra_data=extra_data
                    )
                elif plot == "resolution_1d_histogram":
                    if variable == "azimuth":
                        x = smallest_angle_difference(
                            data[variable + "_truth"].values,
                            data[variable + "_prediction"].values,
                        )
                    else:
                        x = (
                            data[variable + "_truth"].values
                            - data[variable + "_prediction"].values
                        )
                    y = data["energy_log10_truth"].values
                    centers, means, errors = resolution_1d_histogram(
                        x, y, save_path, variable, run
                    )
                    out_dict = {
                        "centers": np.array(centers),
                        "means": np.array(means),
                        "lows": errors[0, :],
                        "highs": errors[1, :],
                    }
                    out_df = pd.DataFrame(data=out_dict)
                    out_df.to_csv(
                        str(save_path / f"{variable}_performance.csv"), index=False
                    )
                elif plot == "error_2d_histogram":
                    x = data[variable + "_prediction"].values
                    if variable == "azimuth":
                        x = smallest_angle_difference(
                            data[variable + "_truth"].values,
                            x,
                        )
                    else:
                        x = data[variable + "_truth"].values - x
                    y = data["energy_log10_truth"].values
                    error_2d_histogram(x, y, output_path, variable, run)
    true_dir_vec = convert_spherical_to_cartesian(
        data["zenith_truth"].values.flatten(), data["azimuth_truth"].values.flatten()
    )
    pred_dir_vec = convert_spherical_to_cartesian(
        data["zenith_prediction"].values.flatten(),
        data["azimuth_prediction"].values.flatten(),
    )
    x = vg.angle(true_dir_vec, pred_dir_vec).flatten()
    y = data["energy_log10_truth"].values
    print(f"{datetime.now()}: Plotting error_2d_histogram for angle")
    output_path = save_path / "angle"
    output_path.mkdir(exist_ok=True)
    error_2d_histogram(x, y, output_path, "angle", run)
    print(f"{datetime.now()}: Plotting resolution_1d_histogram for angle")
    centers, means, errors = resolution_1d_histogram(x, y, save_path, "angle", run)
    out_dict = {
        "centers": np.array(centers),
        "means": np.array(means),
        "lows": errors[0, :],
        "highs": errors[1, :],
    }
    out_df = pd.DataFrame(data=out_dict)
    out_df.to_csv(str(save_path / "angle_performance.csv"), index=False)


def angle_plotting(run, limit, prediction_db_path, plots, save_path, metric, event_nos):
    variables = ["azimuth", "zenith"]
    query = create_query(variables, run, limit, event_nos)
    print(f"{datetime.now()}: Fetching data")
    uri = f"file:{str(prediction_db_path)}?mode=ro"
    with sqlite3.connect(uri, uri=True) as con:
        data = pd.read_sql(query, con)
    if np.amax(data["azimuth_truth"].values) < 7:
        data["azimuth_truth"] = data["azimuth_truth"].apply(np.rad2deg)
    if np.amax(data["zenith_truth"].values) < 4:
        data["zenith_truth"] = data["zenith_truth"].apply(np.rad2deg)
    for variable in variables:
        if "energy_log10" in variable:
            continue
        else:
            output_path = save_path / variable
            output_path.mkdir(exist_ok=True)
            for plot in plots:
                print(f"{datetime.now()}: Plotting {plot} for {variable}")
                if plot == "prediction_2d_histogram":
                    x = data[variable + "_prediction"].values
                    y = data[variable + "_truth"].values
                    prediction_2d_histogram(x, y, output_path, variable, run)
                elif plot == "prediction_1d_histogram":
                    extra_data = None
                    x = data[variable + "_prediction"].values
                    y = data[variable + "_truth"].values
                    prediction_1d_histogram(
                        x, y, output_path, variable, run, extra_data=extra_data
                    )
                elif plot == "resolution_1d_histogram":
                    if variable == "azimuth":
                        x = smallest_angle_difference(
                            data[variable + "_truth"].values,
                            data[variable + "_prediction"].values,
                        )
                    else:
                        x = (
                            data[variable + "_truth"].values
                            - data[variable + "_prediction"].values
                        )
                    y = data["energy_log10_truth"].values
                    centers, means, errors = resolution_1d_histogram(
                        x, y, save_path, variable, run
                    )
                    out_dict = {
                        "centers": np.array(centers),
                        "means": np.array(means),
                        "lows": errors[0, :],
                        "highs": errors[1, :],
                    }
                    out_df = pd.DataFrame(data=out_dict)
                    out_df.to_csv(
                        str(save_path / f"{variable}_performance.csv"), index=False
                    )
                elif plot == "error_2d_histogram":
                    x = data[variable + "_prediction"].values
                    if variable == "azimuth":
                        x = smallest_angle_difference(
                            data[variable + "_truth"].values,
                            x,
                        )
                    else:
                        x = data[variable + "_truth"].values - x
                    y = data["energy_log10_truth"].values
                    error_2d_histogram(x, y, output_path, variable, run)


def zenith_plotting(
    run, limit, prediction_db_path, plots, save_path, metric, event_nos
):
    variables = ["zenith"]
    query = create_query(variables, run, limit, event_nos)
    print(f"{datetime.now()}: Fetching data")
    uri = f"file:{str(prediction_db_path)}?mode=ro"
    with sqlite3.connect(uri, uri=True) as con:
        data = pd.read_sql(query, con)
    if np.amax(data["zenith_truth"].values) < 4:
        data["zenith_truth"] = data["zenith_truth"].apply(np.rad2deg)
    for variable in variables:
        if "energy_log10" in variable:
            continue
        else:
            output_path = save_path / variable
            output_path.mkdir(exist_ok=True)
            for plot in plots:
                print(f"{datetime.now()}: Plotting {plot} for {variable}")
                if plot == "prediction_2d_histogram":
                    x = data[variable + "_prediction"].values
                    y = data[variable + "_truth"].values
                    prediction_2d_histogram(x, y, output_path, variable, run)
                elif plot == "prediction_1d_histogram":
                    extra_data = None
                    x = data[variable + "_prediction"].values
                    y = data[variable + "_truth"].values
                    prediction_1d_histogram(
                        x, y, output_path, variable, run, extra_data=extra_data
                    )
                elif plot == "resolution_1d_histogram":
                    x = (
                        data[variable + "_truth"].values
                        - data[variable + "_prediction"].values
                    )
                    y = data["energy_log10_truth"].values
                    centers, means, errors = resolution_1d_histogram(
                        x, y, save_path, variable, run
                    )
                    out_dict = {
                        "centers": np.array(centers),
                        "means": np.array(means),
                        "lows": errors[0, :],
                        "highs": errors[1, :],
                    }
                    out_df = pd.DataFrame(data=out_dict)
                    out_df.to_csv(
                        str(save_path / f"{variable}_performance.csv"), index=False
                    )
                elif plot == "error_2d_histogram":
                    x = data[variable + "_prediction"].values
                    x = data[variable + "_truth"].values - x
                    y = data["energy_log10_truth"].values
                    error_2d_histogram(x, y, output_path, variable, run)


def percentile_25(x):
    return np.percentile(x, 25)


def percentile_50(x):
    return np.percentile(x, 50)


def percentile_75(x):
    return np.percentile(x, 75)


def percentile_65(x):
    return np.percentile(x, 65)


def percentile_68(x):
    return np.percentile(x, 68)


def custom_iqr(x):
    return iqr(x, scale="normal")


def histogram_1d_prediction(
    data, mean, low, high, plot_name, metric, output_path, iter, bin_edges, bin_range
):
    no_events = len(data)
    if "prediction" in plot_name:
        title = f"{run} {metric} prediction distribution"
    elif "error" in plot_name:
        title = f"{run} {metric} error distribution"
    output_path = output_path / (plot_name + "_data_basis")
    output_path.mkdir(exist_ok=True)
    output_file = output_path / (str(iter) + ".pdf")
    if iter == bin_range - 1:
        print_bins = f"[{round(bin_edges[iter], 1)}, {round(bin_edges[iter + 1], 1)}]"
    else:
        print_bins = f"[{round(bin_edges[iter], 1)}, {round(bin_edges[iter + 1], 1)})"
    fig, ax = plt.subplots()
    ax.hist(data, bins="auto", histtype="step")
    ax.axvline(low, color="red", linestyle="dotted", label=f"25th %ile")
    ax.axvline(mean, color="red", linestyle="solid", label=f"Median")
    ax.axvline(high, color="red", linestyle="dashed", label=f"75th %ile")
    if "prediction" in plot_name:
        ax.axvspan(
            bin_edges[iter], bin_edges[iter + 1], alpha=0.5, color="grey", label="Bin"
        )
    if "prediction" in plot_name:
        if metric == "energy_log10":
            xlabel = "$\\log_{10}(E_{\\textup{prediction}}) \\, \\left[ E / \\textup{GeV} \\right]$"
        elif metric == "direction_x":
            xlabel = "$r_{x \\textup{, prediction}} \\, [\\textup{m}]$"
        elif metric == "direction_y":
            xlabel = "$r_{y \\textup{, prediction}} \\, [\\textup{m}]$"
        elif metric == "direction_z":
            xlabel = "$r_{z \\textup{, prediction}} \\, [\\textup{m}]$"
        elif metric == "azimuth":
            xlabel = "$\\phi_{\\textup{prediction}} \\, [\\textup{deg}]$"
        elif metric == "zenith":
            xlabel = "$\\theta_{\\textup{prediction}} \\, [\\textup{deg}]$"
    elif "error" in plot_name:
        if metric == "energy_log10":
            xlabel = "$\\log_{10}(E_{\\textup{truth}}) - \\log_{10}(E_{\\textup{prediction}}) \\, \\left[ E / \\textup{GeV} \\right]$"
        elif metric == "direction_x":
            xlabel = "$r_{x \\textup{, truth}} - r_{x \\textup{, prediction}} \\, [\\textup{m}]$"
        elif metric == "direction_y":
            xlabel = "$r_{y \\textup{, truth}} - r_{y \\textup{, prediction}} \\, [\\textup{m}]$"
        elif metric == "direction_z":
            xlabel = "$r_{z \\textup{, truth}} - r_{z \\textup{, prediction}} \\, [\\textup{m}]$"
        elif metric == "azimuth":
            xlabel = "$\\phi_{\\textup{truth}} - \\phi_{\\textup{prediction}} \\, [\\textup{deg}]$"
        elif metric == "zenith":
            xlabel = "$\\theta_{\\textup{truth}} - \\theta_{\\textup{prediction}} \\, [\\textup{deg}]$"
        elif metric == "angle":
            xlabel = "$\\arccos\\left[\\bm{r}_{\\textup{truth}} \\cdot \\bm{r}_{\\textup{prediction}} / \\left( \\left| \\bm{r}_{\\textup{truth}} \\right| \\left| \\bm{r}_{\\textup{prediction}} \\right| \\right) \\right] \\, [\\textup{deg}]$"
    ax.set_xlabel(xlabel)
    ax.set_ylabel("Events")
    ax.set_title(
        label=(f"{no_events:,} events, bin {print_bins}"),
        loc="left",
    )
    ax.legend()
    fig.suptitle(
        title,
        fontsize=12,
        x=0.1,
        ha="left",
    )
    fig.savefig(str(output_file))
    plt.close()


def histogram_1d_errors(
    data, mean, low, high, plot_name, metric, output_path, iter, bin_edges, bin_range
):
    no_events = len(data)
    output_path = output_path / (plot_name + "_data_basis")
    output_path.mkdir(exist_ok=True)
    output_file = output_path / (str(iter) + ".pdf")
    if iter == bin_range - 1:
        print_bins = f"[{round(bin_edges[iter], 1)}, {round(bin_edges[iter + 1], 1)}]"
    else:
        print_bins = f"[{round(bin_edges[iter], 1)}, {round(bin_edges[iter + 1], 1)})"
    fig, ax = plt.subplots()
    ax.hist(data, bins="auto", histtype="step")
    ax.axvline(low, color="red", linestyle="dotted", label=f"25th %ile")
    ax.axvline(mean, color="red", linestyle="solid", label=f"Median")
    ax.axvline(high, color="red", linestyle="dashed", label=f"75th %ile")
    if metric == "energy_log10" and plot_name == "prediction_2d_histogram":
        xlabel = "$\\log_{10}(E_{\\textup{prediction}}) \\, \\left[ E / \\textup{GeV} \\right]$"
    ax.set_xlabel(xlabel)
    ax.set_ylabel("Events")
    ax.set_title(
        label=(f"{no_events:,} events, bin {print_bins}"),
        loc="left",
    )
    fig.suptitle(
        f"{run} {metric} error distribution",
        fontsize=12,
        x=0.1,
        ha="left",
    )
    ax.legend()
    fig.savefig(str(output_file))
    plt.close()


def means_and_errors(x, y, minimum, maximum, plot_name, metric, output_path):
    means, lows, highs = [], [], []
    if "prediction" in plot_name and metric == "azimuth":
        bins = 18
    elif "prediction" in plot_name and metric == "zenith":
        bins = 18
    else:
        bins = 18
    _, bin_edges = np.histogram(y, bins=bins, range=(minimum, maximum))
    centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    bin_range = bin_edges.shape[0] - 1
    for iter in range(bin_range):
        print(
            f"{datetime.now()}: Finding means and error in bin {iter + 1} out of {bin_range}"
        )
        if iter == bin_range - 1:
            data = x[np.logical_and(y >= bin_edges[iter], y <= bin_edges[iter + 1])]
        else:
            data = x[np.logical_and(y > bin_edges[iter], y <= bin_edges[iter + 1])]
        mean = percentile_50(data)
        means.append(mean)
        low = percentile_25(data)
        lows.append(abs(means[iter] - low))
        high = percentile_75(data)
        highs.append(abs(means[iter] - high))
        histogram_1d_prediction(
            data,
            mean,
            low,
            high,
            plot_name,
            metric,
            output_path,
            iter,
            bin_edges,
            bin_range,
        )
    errors = np.array((lows, highs))
    return centers, means, errors


def interquartile(x, y, minimum, maximum, plot_name, metric, output_path):
    means, lows, highs, heights = [], [], [], []
    bins = 18
    _, bin_edges = np.histogram(y, bins=bins, range=(minimum, maximum))
    centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    bin_range = bin_edges.shape[0] - 1
    for iter in range(bin_range):
        print(f"{datetime.now()}: Finding IQR in bin {iter + 1} out of {bin_range}")
        if iter == bin_range - 1:
            data = x[np.logical_and(y >= bin_edges[iter], y <= bin_edges[iter + 1])]
        else:
            data = x[np.logical_and(y > bin_edges[iter], y <= bin_edges[iter + 1])]
        dist = bp.EmpiricalDistribution(data)
        if metric == "angle" or metric == "azimuth":
            mean = percentile_68(data)
            ci_low, ci_high = bp.bcanon_interval(
                dist, percentile_68, data, num_threads=20
            )
        else:
            mean = custom_iqr(data)
            ci_low, ci_high = bp.bcanon_interval(dist, custom_iqr, data, num_threads=20)
        means.append(mean)
        low = abs(means[iter] - ci_low)
        high = abs(means[iter] - ci_high)
        lows.append(low)
        highs.append(high)
        heights.append(len(data))
    errors = np.array((lows, highs))
    return centers, means, errors, heights


def prediction_1d_histogram(x, y, output_path, metric, run, extra_data=None, bins=100):
    no_events = len(x)
    minimum = np.around(np.amin(y), 1)
    maximum = np.around(np.amax(y), 1)
    no_clipped = np.sum(np.logical_or(x < minimum, x > maximum))
    pct_clipped = (no_clipped / no_events) * 100
    x = np.clip(x, minimum, maximum)
    fig, ax = plt.subplots()
    ax.hist(x, bins="auto", histtype="step", label="Prediction", density=True)
    ax.hist(y, bins="auto", histtype="step", label="Truth", density=True)
    if extra_data is not None:
        ax.hist(
            extra_data, bins="auto", histtype="step", label="Normalized", density=True
        )
    ylabel = "Density"
    if metric == "energy_log10":
        xlabel = "$\\log_{10}(E) \\, \\left[ E / \\textup{GeV} \\right]$"
    elif metric == "direction_x":
        xlabel = "$r_{x} \\, [\\textup{m}]$"
    elif metric == "direction_y":
        xlabel = "$r_{y} \\, [\\textup{m}]$"
    elif metric == "direction_z":
        xlabel = "$r_{z} \\, [\\textup{m}]$"
    elif metric == "azimuth":
        xlabel = "$\\phi \\, [\\textup{deg}]$"
    elif metric == "zenith":
        xlabel = "$\\theta \\, [\\textup{deg}]$"
    ax.set(
        xlabel=xlabel,
        ylabel=ylabel,
    )
    ax.set_title(
        f"{no_events:,} events, overflow $\\approx$ {np.round(pct_clipped, 1)} %",
        loc="left",
    )
    ax.legend()
    fig.suptitle(
        f"{run} {metric} distributions",
        fontsize=12,
        x=0.1,
        ha="left",
    )
    output_file = output_path / (metric + "_" + "prediction_1d_histogram" + ".pdf")
    fig.savefig(str(output_file))
    plt.close()


def resolution_1d_histogram(x, y, output_path, metric, run, bins=100):
    no_events = len(x)
    minimum = np.around(np.amin(y), 1)
    maximum = np.around(np.amax(y), 1)
    centers, means, errors, heights = interquartile(
        x, y, minimum, maximum, "resolution_1d_histogram", metric, output_path
    )
    fig, ax = plt.subplots()
    events_ax = ax.twinx()
    ax.errorbar(
        centers,
        means,
        yerr=errors,
        xerr=(centers[1] - centers[0]) / 2,
        linestyle="",
        color="tab:red",
        capsize=2,
        marker=".",
        label="90% CI",
    )
    bars = events_ax.bar(
        centers, heights, width=centers[1] - centers[0], alpha=0.5, color="grey"
    )
    for rectangle in bars:
        rectangle.set_y(0.1)
        rectangle.set_height(rectangle.get_height() - 0.1)
    if metric == "energy_log10":
        ylabel = "IQR / 1.349 $\\left[ E / \\textup{GeV} \\right]$"
    if metric == "direction_x":
        ylabel = "IQR / 1.349 $\\left[ \\textup{m} \\right]$"
    if metric == "direction_y":
        ylabel = "IQR / 1.349 $\\left[ \\textup{m} \\right]$"
    if metric == "direction_z":
        ylabel = "IQR / 1.349 $\\left[ \\textup{m} \\right]$"
    if metric == "zenith":
        ylabel = "IQR / 1.349 $\\left[ \\textup{deg} \\right]$"
    if metric == "angle":
        ylabel = "68th %ile $\\left[ \\textup{deg} \\right]$"
    if metric == "azimuth":
        ylabel = "68th %ile $\\left[ \\textup{deg} \\right]$"
    ax.set(
        xlabel="$\\log_{10}(E_{\\textup{truth}}) \\, \\left[ E / \\textup{GeV} \\right]$",
        ylabel=ylabel,
    )
    ax.set_title(
        label=(f"{no_events:,} events"),
        loc="left",
    )
    ax.legend()
    fig.suptitle(
        f"{run} {metric} resolution",
        fontsize=12,
        x=0.1,
        ha="left",
    )
    ax.set_ylim(0)
    events_ax.set_ylim(1)
    events_ax.set_yscale("log")
    events_ax.set(ylabel="Events")
    output_file = output_path / (metric + "_" + "resolution_1d_histogram" + ".pdf")
    fig.savefig(str(output_file))
    plt.close()
    return centers, means, errors


def prediction_2d_histogram(x, y, output_path, metric, run, bins=100):
    no_events = len(x)
    if metric == "azimuth":
        minimum = 0
        maximum = 360
    elif metric == "zenith":
        minimum = 0
        maximum = 180
    elif metric == "direction_x":
        minimum = -1
        maximum = 1
    elif metric == "direction_y":
        minimum = -1
        maximum = 1
    elif metric == "direction_z":
        minimum = -1
        maximum = 1
    else:
        minimum = np.around(np.amin(y), 1)
        maximum = np.around(np.amax(y), 1)
    centers, means, errors = means_and_errors(
        x, y, minimum, maximum, "prediction_2d_histogram", metric, output_path
    )
    no_clipped_x = np.sum(np.logical_or(x < minimum, x > maximum))
    no_clipped_y = np.sum(np.logical_or(y < minimum, y > maximum))
    no_clipped = no_clipped_x + no_clipped_y
    pct_clipped = (no_clipped / no_events) * 100
    x = np.clip(x, minimum, maximum)
    y = np.clip(y, minimum, maximum)
    line = np.linspace(minimum, maximum, 100)
    if metric == "energy_log10":
        xlabel = "$\\log_{10}(E_{\\textup{prediction}}) \\, \\left[ E / \\textup{GeV} \\right]$"
        ylabel = (
            "$\\log_{10}(E_{\\textup{truth}}) \\, \\left[ E / \\textup{GeV} \\right]$"
        )
    elif metric == "direction_x":
        xlabel = "$r_{x \\textup{, prediction}} \\, [\\textup{m}]$"
        ylabel = "$r_{x \\textup{, truth}} \\, [\\textup{m}]$"
    elif metric == "direction_y":
        xlabel = "$r_{y \\textup{, prediction}} \\, [\\textup{m}]$"
        ylabel = "$r_{y \\textup{, truth}} \\, [\\textup{m}]$"
    elif metric == "direction_z":
        xlabel = "$r_{z \\textup{, prediction}} \\, [\\textup{m}]$"
        ylabel = "$r_{z \\textup{, truth}} \\, [\\textup{m}]$"
    elif metric == "azimuth":
        xlabel = "$\\phi_{\\textup{prediction}} \\, [\\textup{deg}]$"
        ylabel = "$\\phi_{\\textup{truth}} \\, [\\textup{deg}]$"
    elif metric == "zenith":
        xlabel = "$\\theta_{\\textup{prediction}} \\, [\\textup{deg}]$"
        ylabel = "$\\theta_{\\textup{truth}} \\, [\\textup{deg}]$"
    fig, ax = plt.subplots()
    print(f"{datetime.now()}: Drawing plot")
    counts, xedges, yedges, im = ax.hist2d(
        x,
        y,
        bins=bins,
        range=[[minimum, maximum], [minimum, maximum]],
        cmin=1,
        cmap="viridis",
    )
    ax.plot(line, line, marker="", color="tab:red", linestyle="--")
    ax.errorbar(
        means,
        centers,
        xerr=errors,
        yerr=(centers[1] - centers[0]) / 2,
        linestyle="",
        color="tab:red",
        capsize=2,
        marker=".",
        label="IQR",
    )
    ax.legend()
    ax.set(xlabel=xlabel, ylabel=ylabel)
    ax.set_title(
        label=(
            f"{no_events:,} events, overflow $\\approx$ {np.round(pct_clipped, 1)} %"
        ),
        loc="left",
    )
    fig.suptitle(
        f"{run} {metric} predictions",
        fontsize=12,
        x=0.1,
        ha="left",
    )
    cbar = fig.colorbar(im, ax=ax)
    cbar.set_label("Events")
    output_file = output_path / (metric + "_" + "prediction_2d_histogram" + ".pdf")
    fig.savefig(str(output_file))
    plt.close()


def error_2d_histogram(x, y, output_path, metric, run, bins=100):
    no_events = len(x)
    x_minimum = np.around(np.amin(y), 1)
    x_maximum = np.around(np.amax(y), 1)
    y_minimum = np.percentile(x, 1)
    y_maximum = np.percentile(x, 99)
    centers, means, errors = means_and_errors(
        x, y, x_minimum, x_maximum, "error_2d_histogram", metric, output_path
    )
    no_clipped_x = np.sum(np.logical_or(x < y_minimum, x > y_maximum))
    no_clipped_y = np.sum(np.logical_or(y < x_minimum, y > x_maximum))
    no_clipped = no_clipped_x + no_clipped_y
    pct_clipped = (no_clipped / no_events) * 100
    x = np.clip(x, y_minimum, y_maximum)
    y = np.clip(y, x_minimum, x_maximum)
    xlabel = "$\\log_{10}(E_{\\textup{truth}}) \\, \\left[ E / \\textup{GeV} \\right]$"
    if metric == "energy_log10":
        ylabel = "$\\log_{10}(E_{\\textup{truth}}) - \\log_{10}(E_{\\textup{prediction}}) \\, \\left[ E / \\textup{GeV} \\right]$"
    elif metric == "direction_x":
        ylabel = (
            "$r_{x \\textup{, truth}} - r_{x \\textup{, prediction}} \\, [\\textup{m}]$"
        )
    elif metric == "direction_y":
        ylabel = (
            "$r_{y \\textup{, truth}} - r_{y \\textup{, prediction}} \\, [\\textup{m}]$"
        )
    elif metric == "direction_z":
        ylabel = (
            "$r_{z \\textup{, truth}} - r_{z \\textup{, prediction}} \\, [\\textup{m}]$"
        )
    elif metric == "azimuth":
        ylabel = "$\\phi_{\\textup{truth}} - \\phi_{\\textup{prediction}} \\, [\\textup{deg}]$"
    elif metric == "zenith":
        ylabel = "$\\theta_{\\textup{truth}} - \\theta_{\\textup{prediction}} \\, [\\textup{deg}]$"
    elif metric == "angle":
        ylabel = "$\\arccos\\left(\\frac{\\bm{r}_{\\textup{truth}} \\cdot \\bm{r}_{\\textup{prediction}}}{\\left| \\bm{r}_{\\textup{truth}} \\right| \\left| \\bm{r}_{\\textup{prediction}} \\right|} \\right) \\, [\\textup{deg}]$"
    fig, ax = plt.subplots()
    print(f"{datetime.now()}: Drawing plot")
    counts, xedges, yedges, im = ax.hist2d(
        y,
        x,
        bins=bins,
        range=[[x_minimum, x_maximum], [y_minimum, y_maximum]],
        cmin=1,
        cmap="Oranges",
    )
    ax.axhline(y=0, marker="", color="black", linestyle="--", linewidth=1)
    ax.plot(
        centers,
        means,
        linestyle="-",
        linewidth=2,
        color="tab:red",
        marker="",
        label="Median",
    )
    ax.plot(
        centers,
        np.array(means) - errors[0, :],
        linestyle="dotted",
        linewidth=2,
        color="tab:red",
        marker="",
        label="$1 \\sigma$",
    )
    ax.plot(
        centers,
        np.array(means) + errors[1, :],
        linestyle="dotted",
        linewidth=2,
        color="tab:red",
        marker="",
        # label="$1 \\sigma$",
    )
    ax.legend()
    ax.set(
        xlabel=xlabel,
        ylabel=ylabel,
    )
    ax.set_title(
        label=(
            f"{no_events:,} events, overflow $\\approx$ {np.round(pct_clipped, 1)} %"
        ),
        loc="left",
    )
    fig.suptitle(
        f"{run} {metric} errors",
        fontsize=12,
        x=0.1,
        ha="left",
    )
    cbar = fig.colorbar(im, ax=ax)
    cbar.set_label("Events")
    output_file = output_path / (metric + "_" + "error_2d_histogram" + ".pdf")
    fig.savefig(str(output_file))
    plt.close()


parser = argparse.ArgumentParser(description="Pretty plots for CubeFlow")
parser.add_argument("--dataset", dest="dataset")
parser.add_argument("--set_type", dest="set_type")
args = parser.parse_args()

dataset_name = args.dataset
set_type = args.set_type
run = dataset_name + "_retro"

retro_db_path = Path().home() / "work" / "erda_datasets" / "retro_reco_2.db"
retro_db_uri = f"file:{retro_db_path}?mode=ro"
truth_db_path = (
    Path().home()
    / "work"
    / "erda_datasets"
    / dataset_name
    / "predictions"
    / "predictions.db"
)
truth_db_uri = f"file:{truth_db_path}?mode=ro"

event_nos = get_event_nos(dataset_name, set_type)
# limit = 1000
# event_nos = event_nos[0:limit]
stringified_events_nos = ", ".join([str(event_no) for event_no in event_nos])
output_path = Path().home() / "plots" / "runs" / run
output_path.mkdir(exist_ok=True, parents=True)
plots = [
    "prediction_2d_histogram",
    "prediction_1d_histogram",
    "error_2d_histogram",
    "resolution_1d_histogram",
]
metrics = ["energy", "direction"]

print(f"{datetime.now()}: Plotting set {run}")
for metric in metrics:
    print(f"{datetime.now()}: Plotting metric {metric}")
    if metric == "energy":
        pass
        energy_plotting(
            run,
            plots,
            output_path,
            metric,
            stringified_events_nos,
            retro_db_uri,
            truth_db_uri,
        )
    elif metric == "direction":
        direction_plotting(
            run,
            plots,
            output_path,
            metric,
            stringified_events_nos,
            retro_db_uri,
            truth_db_uri,
        )


print(f"{datetime.now()}: Done")
