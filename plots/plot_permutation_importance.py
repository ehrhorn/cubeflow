import pickle
from pathlib import Path

import matplotlib.pyplot as plt


def perm_imd_plot(ax, run, perm_imp, output_path, figsize, metric):
    baseline = perm_imp[1][0]
    del perm_imp[1][0]
    del perm_imp[0][0]
    ax.barh(y=perm_imp[0], width=perm_imp[1])
    ax.axvline(x=baseline, color="red", linestyle="dashed", label="Baseline")
    ax.set(xlabel="Permutation importance")
    ax.set_title(f"Run {run.split('.')[-1]}, {metric}", fontsize=12, loc="left")


output_type = "pdf"
file_name = f"permutation_importance_1"
if output_type == "pdf":
    output_file = Path().home() / f"{file_name}.pdf"
else:
    output_file = (
        Path().home()
        / "Developer"
        / "thesis"
        / "images"
        / "results"
        / f"{file_name}.pgf"
    )
figsize = (1 * 6.2012, 1 * 3 + 0.5)
runs_root = Path().home() / "work" / "runs" / "done"
runs = [
    "dev_genie_numu_cc_train_retro_004.zenith.model_2.SRTInIcePulses.Adam.mse.None.5.256.400.2012231441",
    "dev_genie_numu_cc_train_retro_004.zenith.tcn_model_35.SRTInIcePulses.Adam.logcosh.weight_uniform.5.128.255.2012101124"
    # "dev_genie_numu_cc_train_retro_005.zenith.model_2.SRTInIcePulses.Adam.mse.None.5.256.200.2012241359",
    # "dev_genie_numu_cc_train_retro_005.energy.model_2.SRTInIcePulses.Adam.mse.None.5.256.200.2012242224",
]

fig, axes = plt.subplots(ncols=2, sharey=True, figsize=figsize)
for run, ax in zip(runs, axes):
    metric = run.split(".")[1]
    importance_file = runs_root / run / f"perm_imp.pkl"
    with open(importance_file, "rb") as f:
        importance = pickle.load(f)
    perm_imd_plot(ax, run, importance, output_file, figsize, metric)

lines, labels = axes[0].get_legend_handles_labels()
axes[0].legend()
fig.tight_layout()
fig.savefig(str(output_file))
plt.close()
