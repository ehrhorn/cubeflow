from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np

out_file = Path().home() / "logarithmic_time.pdf"

x = np.linspace(1, 20, 1000)
y = np.log10(x)

fig, ax = plt.subplots()
ax.plot(x, y, marker="")
ax.plot(x, x, marker="")
fig.savefig(out_file)
