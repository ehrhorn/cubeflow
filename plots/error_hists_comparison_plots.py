import pickle
import sqlite3
from pathlib import Path

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle
from matplotlib.ticker import MaxNLocator
from scipy.stats import iqr

pd.set_option("mode.use_inf_as_na", True)
mpl.rcParams["axes.unicode_minus"] = False


def get_run_path(run_name):
    runs_path = Path().home() / "work" / "erda_runs" / "done"
    run = [
        run
        for run in runs_path.iterdir()
        if run.is_dir() and run.suffix.strip(".") == run_name
    ][0]
    return run


bins_range = np.arange(0, 4)
_, bins = np.histogram(bins_range, bins=18)

limit = -1
metric = "energy_log10"
run_name = str(2012242224)
# metric = "zenith"
# run_name = str(2011221753)

retro_db_path = Path().home() / "work" / "erda_datasets" / "retro_reco_2.db"
retro_db_uri = f"file:{retro_db_path}?mode=ro"
bins_list = [
    (bins[0], bins[1]),
    (bins[4], bins[5]),
    (bins[8], bins[9]),
    (bins[11], bins[12]),
    (bins[14], bins[15]),
    (bins[17], bins[18]),
]
run = get_run_path(run_name)
dataset_name = run.name.split(".")[0]
truth_db_path = (
    Path().home()
    / "work"
    / "erda_datasets"
    / dataset_name
    / "predictions"
    / "predictions.db"
)
truth_db_uri = f"file:{truth_db_path}?mode=ro"
sets_path = (
    Path().home() / "work" / "erda_datasets" / dataset_name / "meta" / "sets.pkl"
)
with open(sets_path, "rb") as f:
    sets = pickle.load(f)

out_file = Path().home() / "plots" / f"{metric.strip('_log10')}_bins.pgf"

nrows = int(np.ceil(len(bins_list) / 2.0))
ncols = 1 if len(bins_list) == 1 else 2
figsize = (6.2012, nrows * 2 + 0.5)
fig, axes = plt.subplots(
    nrows=nrows, ncols=ncols, figsize=figsize, sharex=True, sharey=False
)

if len(bins_list) == 1:
    axes = [axes]
else:
    axes = axes.flatten()

if metric == "energy_log10":
    xlabel = "$\\log_{10}(E_{\\textup{true}}) - \\log_{10}(E_{\\textup{reco}}) \\, \\left[ E / \\textup{GeV} \\right]$"
elif metric == "azimuth":
    xlabel = (
        "$\\phi_{\\textup{truth}} - \\phi_{\\textup{prediction}} \\, [\\textup{deg}]$"
    )
elif metric == "zenith":
    xlabel = "$\\theta_{\\textup{truth}} - \\theta_{\\textup{prediction}} \\, [\\textup{deg}]$"

suptitle = f"{metric.strip('_log10').capitalize()} error distribution in selected bins"

row = 0

for iteration, (bins, ax) in enumerate(zip(bins_list, axes)):
    query = f"select event_no, {metric} from truth where energy_log10 >= {bins[0]}  and energy_log10 < {bins[1]} limit {limit}"
    with sqlite3.connect(truth_db_uri, uri=True) as con:
        truth = pd.read_sql(query, con)
    truth = truth[truth["event_no"].isin(sets["val"])]
    if metric == "zenith" or metric == "azimuth":
        if truth[metric].max() < 7:
            truth[metric] = truth[metric].apply(np.rad2deg)
    stringified_events_nos = ", ".join(
        [str(event_no) for event_no in truth["event_no"].values.flatten().tolist()]
    )

    query = f"select event_no, {metric} from '{run.name}' where event_no in({stringified_events_nos})"
    with sqlite3.connect(truth_db_uri, uri=True) as con:
        predictions = pd.read_sql(query, con)
    if metric == "zenith" or metric == "azimuth":
        if predictions[metric].max() < 7:
            predictions[metric] = predictions[metric].apply(np.rad2deg)

    query = f"select event_no, {metric} from 'retro_reco' where event_no in({stringified_events_nos})"
    with sqlite3.connect(retro_db_path, uri=True) as con:
        retro = pd.read_sql(query, con)
    if metric == "zenith" or metric == "azimuth":
        if retro[metric].max() < 7:
            retro[metric] = retro[metric].apply(np.rad2deg)

    predictions_merged = truth.merge(
        predictions, on="event_no", suffixes=("_true", "_reco")
    )
    predictions_merged["error"] = (
        predictions_merged[f"{metric}_true"] - predictions_merged[f"{metric}_reco"]
    )
    retro_merged = truth.merge(retro, on="event_no", suffixes=("_true", "_reco"))
    retro_merged["error"] = (
        retro_merged[f"{metric}_true"] - retro_merged[f"{metric}_reco"]
    )
    retro_merged.dropna(inplace=True)
    non_na_event_nos = retro_merged["event_no"].values.flatten().tolist()
    predictions_merged = predictions_merged[
        predictions_merged["event_no"].isin(non_na_event_nos)
    ]

    if iteration % 2 == 0:
        row += 1
    if bins[1] == 3.0:
        print_bins = f"[{round(bins[0], 2)}, {round(bins[1], 2)}]"
    else:
        print_bins = f"[{round(bins[0], 2)}, {round(bins[1], 2)})"
    no_events = len(predictions_merged)
    cubeflow_iqr = iqr(predictions_merged["error"].values, scale="normal")
    retro_iqr = iqr(retro_merged["error"].values, scale="normal")

    if row == nrows:
        xlabel_text = xlabel
    else:
        xlabel_text = None
    if iteration % 2 == 0:
        ylabel = "Density"
    else:
        ylabel = "Density"

    # if iteration == 0:
    cubeflow_label = f"IQR = {round(cubeflow_iqr, 2)}"
    retro_label = f"IQR = {round(retro_iqr, 2)}"
    # else:
    #     cubeflow_label = None
    #     retro_label = None

    _, hist_bins, _ = ax.hist(
        predictions_merged["error"],
        bins="auto",
        histtype="step",
        label=cubeflow_label,
        density=True,
    )
    ax.hist(
        retro_merged["error"],
        bins=hist_bins,
        histtype="step",
        label=retro_label,
        density=True,
    )
    ax.set_title(
        f"Bin {print_bins}, {no_events:,} events",
        fontsize=10,
        loc="left",
    )
    fig.suptitle(
        suptitle,
        fontsize=12,
        x=0.1,
        ha="left",
    )
    ax.set_ylabel(ylabel=ylabel, fontsize=10)
    ax.set_xlabel(xlabel=xlabel_text, fontsize=10)
    ax.xaxis.set_tick_params(labelbottom=True)
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    ax.legend()

custom_lines = [
    Rectangle((0, 0), 1, 1, fill=None, ec="#50514F", fc=None),
    Rectangle((0, 0), 1, 1, fill=None, ec="#F25F5C", fc=None, ls="dotted"),
]
fig.legend(custom_lines, ["CubeFlow", "Retro Reco"])
fig.tight_layout()
fig.savefig(str(out_file))
