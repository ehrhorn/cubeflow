from pathlib import Path

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import MaxNLocator

mpl.rcParams["axes.unicode_minus"] = False
output_type = "pgf"

if output_type == "pdf":
    output_file = Path().home() / "loss_functions.pdf"
else:
    output_file = (
        Path().home()
        / "Developer"
        / "thesis"
        / "images"
        / "design"
        / "loss_functions.pgf"
    )

x = np.linspace(-1.5, 1.5, 1000)
mse = x ** 2
mae = abs(x)
logcosh = np.log10(np.cosh(x))

huber = []
delta = 1
for value in x:
    if abs(value) <= delta:
        huber.append(0.5 * value ** 2)
    else:
        huber.append(delta * (abs(value) - 0.5 * delta))

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(6.2012, 1 * 2.5))
ax.plot(x, mse, marker="", label="MSE", c="black")
ax.plot(x, mae, marker="", label="MAE", c="black")
ax.plot(x, logcosh, marker="", label="logcosh", c="black")
# ax.plot(x, huber, marker="", label="Huber", c="black")
ax.set(xlabel="$x$", ylabel="$f(x)$")
ax.grid(alpha=0.2)
ax.set_title(label="Loss functions", loc="left")
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
ax.yaxis.set_major_locator(MaxNLocator(integer=True))
ax.legend()
fig.tight_layout()
fig.savefig(output_file)
