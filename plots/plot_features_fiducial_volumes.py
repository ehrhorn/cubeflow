import pickle
import sqlite3
import warnings
from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from KDEpy import FFTKDE

warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=UserWarning)

dataset_name = "dev_upgrade_train_step4_002"
metric = "dom_z"
table = "features"
limit = 1e8
query = f"select event_no, pmt_type, {metric} from {table} limit {limit}"

if metric == "dom_z":
    title = "DOM $z$-positions in different fiducial volumes"
    xlabel = "DOM $z$-position (m)"

ylabel = f"Density"

db = (
    Path().home() / "work" / "datasets" / dataset_name / "data" / (dataset_name + ".db")
)
transformer_file = (
    Path().home() / "work" / "datasets" / dataset_name / "meta" / "transformers.pkl"
)
with open(transformer_file, "rb") as f:
    transformers = pickle.load(f)
plot_out_path = Path().home() / "plots" / "datasets" / dataset_name
plot_out_path.mkdir(exist_ok=True)

print(f"{datetime.now()}: Fetching pulses")
uri = f"file:{str(db)}?mode=ro"
with sqlite3.connect(uri, uri=True) as con:
    dataframe = pd.read_sql(query, con)

try:
    print(f"{datetime.now()}: Transforming {metric}")
    transformed_metric = (
        transformers[table][metric]
        .inverse_transform(dataframe[metric].values.reshape(-1, 1))
        .flatten()
    )
    dataframe[metric] = transformed_metric
except Exception:
    print("No transformers for metric. Proceeding...")

min_z = dataframe[metric].min()
max_z = dataframe[metric].max()
x_plot = np.linspace(min_z, max_z, 1000)

no_events = len(dataframe["event_no"].unique())
no_pulses = len(dataframe)

icecube_strings = np.arange(1, 79).tolist()
deep_core_inner = np.arange(79, 87).tolist() + [36]
deep_core_outer = deep_core_inner + [26, 27, 35, 37, 45, 46]
upgrade_inner = np.arange(87, 94).tolist()
upgrade_outer = upgrade_inner + deep_core_outer

# volumes = [icecube_strings, deep_core_inner, upgrade_inner]
# names = ["IceCube", "DeepCore", "Upgrade"]
# colors = ["blue", "green", "red"]
# plot_types = ["hist", "kde"]
volumes = [20, 110, 120, 130]
names = ["IceCube", "PDOM", "D-Egg", "mDOM"]
colors = ["blue", "green", "red", "black"]
plot_types = ["kde"]

print(f"{datetime.now()}: Plotting")
for plot_type in plot_types:
    fig, ax = plt.subplots()
    for volume, name, color in zip(volumes, names, colors):
        events = dataframe[dataframe["pmt_type"] == volume][metric].values
        if plot_type == "kde":
            x, y = FFTKDE(bw="silverman").fit(events).evaluate(2 ** 10)
            ax.plot(
                x, y, marker="", linestyle="solid", label=name, color=color,
            )
        elif plot_type == "hist":
            print(f"{datetime.now()}: Plotting histogram")
            ax.hist(
                events,
                bins="fd",
                density=True,
                color=color,
                histtype="stepfilled",
                alpha=0.5,
                label=name,
                range=(min_z, max_z),
            )
    ax.set_ylim(ymin=0)
    ax.set(
        xlabel="DOM $z$-position", ylabel="Density",
    )
    ax.set_title(
        label=(f"{no_events:,} events, {no_pulses:,} pulses"), loc="left",
    )
    fig.suptitle(
        f"{dataset_name} $z$-position by type", fontsize=16, x=0.1, ha="left",
    )
    ax.legend()
    fig.savefig(plot_out_path / ("dom_z_distribution.pdf"))
