import sqlite3
from datetime import datetime
from os import PathLike
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

limit = -1
dataset_name = "dev_upgrade_train_step4_002"
dataset_db = (
    Path().home() / "work" / "datasets" / dataset_name / "data" / (dataset_name + ".db")
)
meta_db = Path().home() / "work" / "datasets" / "meta.db"

output_path = Path().home() / "plots" / "datasets" / dataset_name
output_path.mkdir(exist_ok=True, parents=True)
output_file = output_path / "string_distribution.pdf"

query = f"select event_no from truth limit {limit}"
print(f"{datetime.now()}: Fetching event numbers from dataset")
with sqlite3.connect(str(dataset_db)) as con:
    event_nos = pd.read_sql(query, con)

stringed_event_nos = ", ".join(
    [str(event_no[0]) for event_no in event_nos.values.tolist()]
)

query = (
    f"select event_no, raw_strings, clean_strings from meta where event_no in ({stringed_event_nos})"
)
print(f"{datetime.now()}: Fetching raw_strings from meta")
with sqlite3.connect(str(meta_db)) as con:
    strings = pd.read_sql(query, con)
print(f"{datetime.now()}: Plotting")

no_events = len(strings)

fig, ax = plt.subplots()
for clean_type, clean_name in zip(['raw_strings', 'clean_strings'], ['Raw', 'SRT']):
    ax.hist(
        strings[clean_type], bins="auto", histtype="step", label=clean_name
    )
ax.set(
    xlabel="Number of strings in event", ylabel="Count",
)
ax.set_title(
    label=(f"{no_events:,} events"), loc="left",
)
ax.legend()
fig.suptitle(f"{dataset_name} string distribution", fontsize=16, x=0.1, ha="left")
fig.savefig(str(output_file))

print(f"{datetime.now()}: Done")
