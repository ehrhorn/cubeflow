from datetime import datetime
from pathlib import Path
import pickle
import sqlite3
import warnings

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats as st

warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=UserWarning)

dataset_name = "dev_nue_train_l5_000"
limit = int(-1)

title = f"Energy vs. DOMs"
xlabel = "Number of DOMs activated"
ylabel = "$\\log{(E_{\\text{truth}})} \\ (\\log{(E)}/E)$"

meta_db = Path().home() / "work" / "datasets" / ("meta.db")
truth_db = (
    Path().home() / "work" / "datasets" / dataset_name / "data" / (dataset_name + ".db")
)
transformer_file = (
    Path().home() / "work" / "datasets" / dataset_name / "meta" / "transformers.pkl"
)
with open(transformer_file, "rb") as f:
    transformers = pickle.load(f)
plot_out_path = Path().home() / "plots" / dataset_name
plot_out_path.mkdir(exist_ok=True)

truth_query = f"select event_no from truth limit {limit}"
print(f"{datetime.now()}: Fetching truth...")
with sqlite3.connect(str(truth_db)) as con:
    truth_dataframe = pd.read_sql(truth_query, con)

for key in truth_dataframe.columns:
    try:
        transformed_metric = (
            transformers["truth"][key]
            .inverse_transform(truth_dataframe[key].values.reshape(-1, 1))
            .flatten()
        )
        truth_dataframe[key] = transformed_metric
    except Exception:
        continue

no_events = len(truth_dataframe["event_no"].unique())
events = truth_dataframe["event_no"].values
joined_events = ", ".join([str(event_no) for event_no in events])
done_events = 0

meta_query = (
    f"select raw_doms, energy_log10 from meta where event_no in ({joined_events})"
)
print(f"{datetime.now()}: Fetching meta...")
with sqlite3.connect(str(meta_db)) as con:
    meta_dataframe = pd.read_sql(meta_query, con)

x = meta_dataframe["raw_doms"].values
y = meta_dataframe["energy_log10"].values

d = np.diff(np.unique(x)).min()
left_of_first_bin = x.min() - float(d) / 2
right_of_last_bin = x.max() + float(d) / 2
dom_bins = np.arange(left_of_first_bin, right_of_last_bin + d, d)

_, energy_bins = np.histogram(y, bins="auto")

plot_types = ["hist", "kde"]

line_x = np.linspace(x.min(), x.max(), 100)
line_y = np.linspace(y.min(), y.max(), 100)

for plot_type in plot_types:
    fig, ax = plt.subplots()
    if plot_type == "hist":
        print(f"{datetime.now()}: Plotting 2D histogram...")
        xmin, xmax = min(x), max(x)
        ymin, ymax = min(y), max(y)
        ax.hist2d(x, y, bins=[dom_bins, energy_bins], cmin=1)
        ax.plot(line_x, line_y, marker="", linestyle="--", c="red")
    elif plot_type == "kde":
        print(f"{datetime.now()}: Plotting KDE...")
        xmin, xmax = -1500, 2500
        ymin, ymax = -200, 400
        xx, yy = np.mgrid[
            xmin : xmax : complex(0, len(dom_bins)),
            ymin : ymax : complex(0, len(energy_bins)),
        ]
        positions = np.vstack([xx.ravel(), yy.ravel()])
        values = np.vstack([x, y])
        kernel = st.gaussian_kde(values)
        f = np.reshape(kernel(positions).T, xx.shape)
        zz = (f - f.min()) / (f.max() - f.min())
        cfset = ax.contourf(xx, yy, zz, cmap="Blues")
        cset = ax.contour(xx, yy, zz, colors="k")
        # ax.scatter(x, y, s=2, facecolor="white")
        # ax.pcolormesh(xx, yy, zz)
        ax.clabel(cset, inline=1, fontsize=4)
    ax.set(
        title=title + f" ({no_events:,} events)",
        xlabel=xlabel,
        ylabel=ylabel,
    )
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    # ax.text(
    #     0.1,
    #     0.9,
    #     f"Events: {no_events:,}",
    #     transform=ax.transAxes,
    #     color="black",
    #     bbox=dict(facecolor="none", edgecolor="black", boxstyle="round,pad=1"),
    # )
    fig.savefig(plot_out_path / ("energy_vs_doms_" + plot_type + ".pdf"))

print(f"{datetime.now()}: Done.")