import argparse
import copy
import json
import pickle
import sqlite3
from datetime import datetime
from pathlib import Path
from shutil import copyfile

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import vg
from bootstrap_stat import bootstrap_stat as bp
from matplotlib import cm
from matplotlib.pyplot import plot
from scipy.stats import iqr


def smallest_angle_difference(x, y):
    first = (360) - np.absolute(x - y)
    second = np.absolute(x - y)
    out = np.minimum(first, second)
    return out


def get_done_runs():
    runs_path = Path().home() / "plots" / "runs"
    runs = [run for run in runs_path.iterdir() if run.is_dir()]
    return runs


def get_run_path(run_name):
    runs_path = Path().home() / "work" / "erda_runs" / "done"
    run = [
        run
        for run in runs_path.iterdir()
        if run.is_dir() and run.suffix.strip(".") == run_name
    ][0]
    return run


def get_prediction_db_paths(dataset_names):
    datasets_path = Path().home() / "work" / "erda_datasets"
    prediction_db_paths = [
        datasets_path / dataset_name / "predictions" / "predictions.db"
        for dataset_name in dataset_names
    ]
    return prediction_db_paths


def create_query(variables, run, limit, event_nos):
    stringified_events_nos = ", ".join([str(event_no) for event_no in event_nos])
    prediction_variables_string = ", ".join(
        [
            "prediction." + variable + " as " + variable + "_prediction"
            for variable in variables
        ]
    )
    if "energy_log10" not in variables:
        variables += ["energy_log10"]
    truth_variables_string = ", ".join(
        ["truth." + variable + " as " + variable + "_truth" for variable in variables]
    )
    query = (
        f"select prediction.event_no, {prediction_variables_string}, {truth_variables_string} "
        f"from '{run.name}' as prediction "
        f"inner join truth on prediction.event_no = truth.event_no "
        f"where prediction.event_no in ({stringified_events_nos}) "
        f"limit {limit}"
    )
    return query


def angle_between(v1: np.ndarray, v2: np.ndarray) -> np.ndarray:
    """Returns the angle in radians between vectors 'v1' and 'v2'.

    Accounts for opposite vectors using numpy.clip.

    Args:
        v1 (numpy.ndarray): vector 1
        v2 (numpy.ndarray): vector 2

    Returns:
        numpy.ndarray: angles between vectors 1 and 2
    """
    p1 = np.einsum("ij,ij->i", v1, v2)
    p2 = np.einsum("ij,ij->i", v1, v1)
    p3 = np.einsum("ij,ij->i", v2, v2)
    p4 = p1 / np.sqrt(p2 * p3)
    angles = np.arccos(np.clip(p4, -1.0, 1.0)).reshape(-1, 1)
    return angles


def energy_plotting(
    run,
    limit,
    prediction_db_path,
    plots,
    save_path,
    metric,
    event_nos,
    error_type="log",
):
    variables = ["energy_log10"]
    query = create_query(variables, run, limit, event_nos)
    print(f"{datetime.now()}: Fetching data")
    uri = f"file:{str(prediction_db_path)}?mode=ro"
    with sqlite3.connect(uri, uri=True) as con:
        data = pd.read_sql(query, con)
    for variable in variables:
        output_path = save_path / variable
        output_path.mkdir(exist_ok=True)
        for plot in plots:
            print(f"{datetime.now()}: Plotting {plot} for {variable}")
            if plot == "prediction_2d_histogram":
                x = data["energy_log10_prediction"].values
                y = data["energy_log10_truth"].values
                prediction_2d_histogram(x, y, output_path, variable, run, y)
                x = data["energy_log10_prediction"].values
                y = data["energy_log10_truth"].values
                prediction_1d_histogram(x, y, output_path, variable, run)
            elif plot == "resolution_1d_histogram":
                if error_type == "log":
                    x = (
                        data["energy_log10_truth"].values
                        - data["energy_log10_prediction"].values
                    )
                elif error_type == "rel":
                    x = (
                        10 ** (data["energy_log10_prediction"].values)
                        - 10 ** (data["energy_log10_truth"].values)
                    ) / 10 ** (data["energy_log10_truth"].values)
                y = data["energy_log10_truth"].values
                centers, means, errors = resolution_1d_histogram(
                    x, y, save_path, variable, run
                )
                out_dict = {
                    "centers": np.array(centers),
                    "means": np.array(means),
                    "lows": errors[0, :],
                    "highs": errors[1, :],
                }
                out_df = pd.DataFrame(data=out_dict)
                out_df.to_csv(str(save_path / "energy_performance.csv"), index=False)
            elif plot == "error_2d_histogram":
                if error_type == "log":
                    x = (
                        data["energy_log10_truth"].values
                        - data["energy_log10_prediction"].values
                    )
                elif error_type == "rel":
                    x = (
                        10 ** (data["energy_log10_prediction"].values)
                        - 10 ** (data["energy_log10_truth"].values)
                    ) / 10 ** (data["energy_log10_truth"].values)
                y = data["energy_log10_truth"].values
                error_2d_histogram(x, y, output_path, variable, run)


def direction_plotting(
    run, limit, prediction_db_path, plots, save_path, metric, event_nos
):
    variables = ["direction_x", "direction_y", "direction_z", "azimuth", "zenith"]
    # variables = ["azimuth", "zenith"]
    query = create_query(variables, run, limit, event_nos)
    print(f"{datetime.now()}: Fetching data")
    uri = f"file:{str(prediction_db_path)}?mode=ro"
    with sqlite3.connect(uri, uri=True) as con:
        data = pd.read_sql(query, con)
    if np.amax(data["azimuth_truth"].values) < 7:
        data["azimuth_truth"] = data["azimuth_truth"].apply(np.rad2deg)
    if np.amax(data["zenith_truth"].values) < 4:
        data["zenith_truth"] = data["zenith_truth"].apply(np.rad2deg)
    normed_directions = vg.normalize(
        data[
            [
                "direction_x_prediction",
                "direction_y_prediction",
                "direction_z_prediction",
            ]
        ].values
    )
    for variable in variables:
        if "energy_log10" in variable:
            continue
        else:
            output_path = save_path / variable
            output_path.mkdir(exist_ok=True)
            for plot in plots:
                print(f"{datetime.now()}: Plotting {plot} for {variable}")
                if plot == "prediction_2d_histogram":
                    if variable == "direction_x":
                        x = normed_directions[:, 0]
                    elif variable == "direction_y":
                        x = normed_directions[:, 1]
                    elif variable == "direction_z":
                        x = normed_directions[:, 2]
                    else:
                        x = data[variable + "_prediction"].values
                    y = data[variable + "_truth"].values
                    prediction_2d_histogram(
                        x,
                        y,
                        output_path,
                        variable,
                        run,
                        data["energy_log10_truth"].values,
                    )
                elif plot == "prediction_1d_histogram":
                    if variable == "direction_x":
                        extra_data = normed_directions[:, 0]
                    elif variable == "direction_y":
                        extra_data = normed_directions[:, 1]
                    elif variable == "direction_z":
                        extra_data = normed_directions[:, 2]
                    else:
                        extra_data = None
                    x = data[variable + "_prediction"].values
                    y = data[variable + "_truth"].values
                    prediction_1d_histogram(
                        x, y, output_path, variable, run, extra_data=extra_data
                    )
                elif plot == "resolution_1d_histogram" and (
                    variable == "azimuth" or variable == "zenith"
                ):
                    if variable == "azimuth":
                        x = smallest_angle_difference(
                            data[variable + "_truth"].values,
                            data[variable + "_prediction"].values,
                        )
                    else:
                        x = (
                            data[variable + "_truth"].values
                            - data[variable + "_prediction"].values
                        )
                    y = data["energy_log10_truth"].values
                    centers, means, errors = resolution_1d_histogram(
                        x, y, save_path, variable, run
                    )
                    out_dict = {
                        "centers": np.array(centers),
                        "means": np.array(means),
                        "lows": errors[0, :],
                        "highs": errors[1, :],
                    }
                    out_df = pd.DataFrame(data=out_dict)
                    out_df.to_csv(
                        str(save_path / f"{variable}_performance.csv"), index=False
                    )
                elif plot == "error_2d_histogram":
                    if variable == "direction_x":
                        x = normed_directions[:, 0]
                    elif variable == "direction_y":
                        x = normed_directions[:, 1]
                    elif variable == "direction_z":
                        x = normed_directions[:, 2]
                    else:
                        x = data[variable + "_prediction"].values
                    if variable == "azimuth":
                        x = smallest_angle_difference(
                            data[variable + "_truth"].values,
                            x,
                        )
                    elif variable == "zenith":
                        x = np.cos(np.deg2rad(x)) - np.cos(
                            np.deg2rad(data[variable + "_truth"].values)
                        )
                    else:
                        x = data[variable + "_truth"].values - x
                    y = data["energy_log10_truth"].values
                    error_2d_histogram(x, y, output_path, variable, run)
    true_dir_vec = data[
        ["direction_x_truth", "direction_y_truth", "direction_z_truth"]
    ].values
    pred_dir_vec = data[
        ["direction_x_prediction", "direction_y_prediction", "direction_z_prediction"]
    ].values
    x = vg.angle(true_dir_vec, pred_dir_vec).flatten()
    y = data["energy_log10_truth"].values
    print(f"{datetime.now()}: Plotting error_2d_histogram for angle")
    output_path = save_path / "angle"
    output_path.mkdir(exist_ok=True)
    error_2d_histogram(x, y, output_path, "angle", run)
    print(f"{datetime.now()}: Plotting resolution_1d_histogram for angle")
    centers, means, errors = resolution_1d_histogram(x, y, save_path, "angle", run)
    out_dict = {
        "centers": np.array(centers),
        "means": np.array(means),
        "lows": errors[0, :],
        "highs": errors[1, :],
    }
    out_df = pd.DataFrame(data=out_dict)
    out_df.to_csv(str(save_path / "angle_performance.csv"), index=False)


def angle_plotting(run, limit, prediction_db_path, plots, save_path, metric, event_nos):
    variables = ["azimuth", "zenith"]
    query = create_query(variables, run, limit, event_nos)
    print(f"{datetime.now()}: Fetching data")
    uri = f"file:{str(prediction_db_path)}?mode=ro"
    with sqlite3.connect(uri, uri=True) as con:
        data = pd.read_sql(query, con)
    if np.amax(data["azimuth_truth"].values) < 7:
        data["azimuth_truth"] = data["azimuth_truth"].apply(np.rad2deg)
    if np.amax(data["zenith_truth"].values) < 4:
        data["zenith_truth"] = data["zenith_truth"].apply(np.rad2deg)
    for variable in variables:
        if "energy_log10" in variable:
            continue
        else:
            output_path = save_path / variable
            output_path.mkdir(exist_ok=True)
            for plot in plots:
                print(f"{datetime.now()}: Plotting {plot} for {variable}")
                if plot == "prediction_2d_histogram":
                    x = data[variable + "_prediction"].values
                    y = data[variable + "_truth"].values
                    prediction_2d_histogram(
                        x,
                        y,
                        output_path,
                        variable,
                        run,
                        data["energy_log10_truth"].values,
                    )
                elif plot == "prediction_1d_histogram":
                    extra_data = None
                    x = data[variable + "_prediction"].values
                    y = data[variable + "_truth"].values
                    prediction_1d_histogram(
                        x, y, output_path, variable, run, extra_data=extra_data
                    )
                elif plot == "resolution_1d_histogram":
                    if variable == "azimuth":
                        x = smallest_angle_difference(
                            data[variable + "_truth"].values,
                            data[variable + "_prediction"].values,
                        )
                    else:
                        x = (
                            data[variable + "_truth"].values
                            - data[variable + "_prediction"].values
                        )
                    y = data["energy_log10_truth"].values
                    centers, means, errors = resolution_1d_histogram(
                        x, y, save_path, variable, run
                    )
                    out_dict = {
                        "centers": np.array(centers),
                        "means": np.array(means),
                        "lows": errors[0, :],
                        "highs": errors[1, :],
                    }
                    out_df = pd.DataFrame(data=out_dict)
                    out_df.to_csv(
                        str(save_path / f"{variable}_performance.csv"), index=False
                    )
                elif plot == "error_2d_histogram":
                    x = data[variable + "_prediction"].values
                    if variable == "azimuth":
                        x = smallest_angle_difference(
                            data[variable + "_truth"].values,
                            x,
                        )
                    else:
                        x = np.cos(np.deg2rad(x)) - np.cos(
                            np.deg2rad(data[variable + "_truth"].values)
                        )
                    y = data["energy_log10_truth"].values
                    error_2d_histogram(x, y, output_path, variable, run)


def zenith_plotting(
    run, limit, prediction_db_path, plots, save_path, metric, event_nos
):
    variables = ["zenith"]
    query = create_query(variables, run, limit, event_nos)
    print(f"{datetime.now()}: Fetching data")
    uri = f"file:{str(prediction_db_path)}?mode=ro"
    with sqlite3.connect(uri, uri=True) as con:
        data = pd.read_sql(query, con)
    if np.amax(data["zenith_truth"].values) < 4:
        data["zenith_truth"] = data["zenith_truth"].apply(np.rad2deg)
    for variable in variables:
        if "energy_log10" in variable:
            continue
        else:
            output_path = save_path / variable
            output_path.mkdir(exist_ok=True)
            for plot in plots:
                print(f"{datetime.now()}: Plotting {plot} for {variable}")
                if plot == "prediction_2d_histogram":
                    x = data[variable + "_prediction"].values
                    y = data[variable + "_truth"].values
                    prediction_2d_histogram(
                        x,
                        y,
                        output_path,
                        variable,
                        run,
                        data["energy_log10_truth"].values,
                    )
                elif plot == "prediction_1d_histogram":
                    extra_data = None
                    x = data[variable + "_prediction"].values
                    y = data[variable + "_truth"].values
                    prediction_1d_histogram(
                        x, y, output_path, variable, run, extra_data=extra_data
                    )
                elif plot == "resolution_1d_histogram":
                    x = (
                        data[variable + "_truth"].values
                        - data[variable + "_prediction"].values
                    )
                    y = data["energy_log10_truth"].values
                    centers, means, errors = resolution_1d_histogram(
                        x, y, save_path, variable, run
                    )
                    out_dict = {
                        "centers": np.array(centers),
                        "means": np.array(means),
                        "lows": errors[0, :],
                        "highs": errors[1, :],
                    }
                    out_df = pd.DataFrame(data=out_dict)
                    out_df.to_csv(
                        str(save_path / f"{variable}_performance.csv"), index=False
                    )
                elif plot == "error_2d_histogram":
                    x = data[variable + "_prediction"].values
                    x = np.cos(np.deg2rad(x)) - np.cos(
                        np.deg2rad(data[variable + "_truth"].values)
                    )
                    y = data["energy_log10_truth"].values
                    error_2d_histogram(x, y, output_path, variable, run)


def azimuth_plotting(
    run, limit, prediction_db_path, plots, save_path, metric, event_nos
):
    variables = ["azimuth"]
    query = create_query(variables, run, limit, event_nos)
    print(f"{datetime.now()}: Fetching data")
    uri = f"file:{str(prediction_db_path)}?mode=ro"
    with sqlite3.connect(uri, uri=True) as con:
        data = pd.read_sql(query, con)
    if np.amax(data["azimuth_truth"].values) < 7:
        data["azimuth_truth"] = data["azimuth_truth"].apply(np.rad2deg)
    for variable in variables:
        if "energy_log10" in variable:
            continue
        else:
            output_path = save_path / variable
            output_path.mkdir(exist_ok=True)
            for plot in plots:
                print(f"{datetime.now()}: Plotting {plot} for {variable}")
                if plot == "prediction_2d_histogram":
                    x = data[variable + "_prediction"].values
                    y = data[variable + "_truth"].values
                    prediction_2d_histogram(
                        x,
                        y,
                        output_path,
                        variable,
                        run,
                        data["energy_log10_truth"].values,
                    )
                elif plot == "prediction_1d_histogram":
                    extra_data = None
                    x = data[variable + "_prediction"].values
                    y = data[variable + "_truth"].values
                    prediction_1d_histogram(
                        x, y, output_path, variable, run, extra_data=extra_data
                    )
                elif plot == "resolution_1d_histogram":
                    x = smallest_angle_difference(
                        data[variable + "_truth"].values,
                        data[variable + "_prediction"].values,
                    )
                    y = data["energy_log10_truth"].values
                    centers, means, errors = resolution_1d_histogram(
                        x, y, save_path, variable, run
                    )
                    out_dict = {
                        "centers": np.array(centers),
                        "means": np.array(means),
                        "lows": errors[0, :],
                        "highs": errors[1, :],
                    }
                    out_df = pd.DataFrame(data=out_dict)
                    out_df.to_csv(
                        str(save_path / f"{variable}_performance.csv"), index=False
                    )
                elif plot == "error_2d_histogram":
                    x = smallest_angle_difference(
                        data[variable + "_truth"].values,
                        data[variable + "_prediction"].values,
                    )
                    y = data["energy_log10_truth"].values
                    error_2d_histogram(x, y, output_path, variable, run)


def percentile_25(x):
    return np.percentile(x, 25)


def percentile_50(x):
    return np.percentile(x, 50)


def percentile_75(x):
    return np.percentile(x, 75)


def percentile_65(x):
    return np.percentile(x, 65)


def percentile_68(x):
    return np.percentile(x, 68)


def custom_iqr(x):
    return iqr(x, scale="normal")


def histogram_1d_prediction(
    data, mean, low, high, plot_name, metric, output_path, iter, bin_edges, bin_range
):
    no_events = len(data)
    if "prediction" in plot_name:
        title = f"{run.suffix.split('.')[-1]} {metric} prediction distribution"
        if metric == "energy_log10":
            xmin, xmax = 0, 3
        elif metric == "azimuth":
            xmin, xmax = 0, 360
        elif metric == "zenith":
            xmin, xmax = 0, 180
        else:
            xmin, xmax = min(data), max(data)
    elif "error" in plot_name:
        title = f"{run.suffix.split('.')[-1]} {metric} error distribution"
        xmin, xmax = min(data), max(data)
    output_path = output_path / (plot_name + "_data_basis")
    output_path.mkdir(exist_ok=True)
    output_file = output_path / (str(iter) + ".pdf")
    if iter == bin_range - 1:
        print_bins = f"[{round(bin_edges[iter], 1)}, {round(bin_edges[iter + 1], 1)}]"
    else:
        print_bins = f"[{round(bin_edges[iter], 1)}, {round(bin_edges[iter + 1], 1)})"
    fig, ax = plt.subplots(figsize=figsize)
    ax.hist(data, bins="auto", histtype="step", range=[xmin, xmax])
    ax.axvline(low, color="red", linestyle="dotted", label=f"25th %ile")
    ax.axvline(mean, color="red", linestyle="solid", label=f"Median")
    ax.axvline(high, color="red", linestyle="dashed", label=f"75th %ile")
    if "prediction" in plot_name:
        ax.axvspan(
            bin_edges[iter], bin_edges[iter + 1], alpha=0.5, color="grey", label="Bin"
        )
    if "prediction" in plot_name:
        if metric == "energy_log10":
            xlabel = "$\\log_{10}(E_{\\textup{prediction}}) \\, \\left[ E / \\textup{GeV} \\right]$"
        elif metric == "direction_x":
            xlabel = "$r_{x \\textup{, prediction}} \\, [\\textup{m}]$"
        elif metric == "direction_y":
            xlabel = "$r_{y \\textup{, prediction}} \\, [\\textup{m}]$"
        elif metric == "direction_z":
            xlabel = "$r_{z \\textup{, prediction}} \\, [\\textup{m}]$"
        elif metric == "azimuth":
            xlabel = "$\\phi_{\\textup{prediction}} \\, [\\textup{deg}]$"
        elif metric == "zenith":
            xlabel = "$\\theta_{\\textup{prediction}} \\, [\\textup{deg}]$"
    elif "error" in plot_name:
        if metric == "energy_log10":
            xlabel = "$\\log_{10}(E_{\\textup{truth}}) - \\log_{10}(E_{\\textup{prediction}}) \\, \\left[ E / \\textup{GeV} \\right]$"
        elif metric == "direction_x":
            xlabel = "$r_{x \\textup{, truth}} - r_{x \\textup{, prediction}} \\, [\\textup{m}]$"
        elif metric == "direction_y":
            xlabel = "$r_{y \\textup{, truth}} - r_{y \\textup{, prediction}} \\, [\\textup{m}]$"
        elif metric == "direction_z":
            xlabel = "$r_{z \\textup{, truth}} - r_{z \\textup{, prediction}} \\, [\\textup{m}]$"
        elif metric == "azimuth":
            xlabel = "$\\phi_{\\textup{truth}} - \\phi_{\\textup{prediction}} \\, [\\textup{deg}]$"
        elif metric == "zenith":
            xlabel = "$\\cos{\\left( \\theta_{\\textup{zenith,reco}} \\right)} - \\cos{\\left( \\theta_{\\textup{zenith,true}} \\right)}$"
        elif metric == "angle":
            xlabel = "$\\arccos\\left[\\bm{r}_{\\textup{truth}} \\cdot \\bm{r}_{\\textup{prediction}} / \\left( \\left| \\bm{r}_{\\textup{truth}} \\right| \\left| \\bm{r}_{\\textup{prediction}} \\right| \\right) \\right] \\, [\\textup{deg}]$"
    ax.set_xlabel(xlabel)
    ax.set_ylabel("Events")
    ax.set_title(
        label=(f"{no_events:,} events, bin {print_bins}"),
        loc="left",
    )
    ax.legend()
    fig.suptitle(
        title,
        fontsize=16,
        x=0.1,
        ha="left",
    )
    fig.tight_layout()
    fig.savefig(str(output_file))
    plt.close()


def histogram_1d_errors(
    data, mean, low, high, plot_name, metric, output_path, iter, bin_edges, bin_range
):
    no_events = len(data)
    output_path = output_path / (plot_name + "_data_basis")
    output_path.mkdir(exist_ok=True)
    output_file = output_path / (str(iter) + ".pdf")
    if iter == bin_range - 1:
        print_bins = f"[{round(bin_edges[iter], 1)}, {round(bin_edges[iter + 1], 1)}]"
    else:
        print_bins = f"[{round(bin_edges[iter], 1)}, {round(bin_edges[iter + 1], 1)})"
    fig, ax = plt.subplots(figsize=figsize)
    ax.hist(data, bins="auto", histtype="step")
    ax.axvline(low, color="red", linestyle="dotted", label=f"25th %ile")
    ax.axvline(mean, color="red", linestyle="solid", label=f"Median")
    ax.axvline(high, color="red", linestyle="dashed", label=f"75th %ile")
    if metric == "energy_log10" and plot_name == "prediction_2d_histogram":
        xlabel = "$\\log_{10}(E_{\\textup{prediction}}) \\, \\left[ E / \\textup{GeV} \\right]$"
    ax.set_xlabel(xlabel)
    ax.set_ylabel("Events")
    ax.set_title(
        label=(f"{no_events:,} events, bin {print_bins}"),
        loc="left",
    )
    fig.suptitle(
        f"{run.suffix.split('.')[-1]} {metric} error distribution",
        fontsize=16,
        x=0.1,
        ha="left",
    )
    ax.legend()
    fig.tight_layout()
    fig.savefig(str(output_file))
    plt.close()


def bootstrapping(x, y, minimum, maximum, plot_name, metric, output_path):
    means, lows, highs = [], [], []
    if "prediction" in plot_name and metric == "azimuth":
        bins = 18
    elif "prediction" in plot_name and metric == "zenith":
        bins = 18
    else:
        bins = 18
    _, bin_edges = np.histogram(y, bins=bins, range=(minimum, maximum))
    centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    bin_range = bin_edges.shape[0] - 1
    for iter in range(bin_range):
        print(f"{datetime.now()}: Bootstrapping {iter + 1} out of {bin_range}")
        if iter == bin_range - 1:
            data = x[np.logical_and(y >= bin_edges[iter], y <= bin_edges[iter + 1])]
        else:
            data = x[np.logical_and(y > bin_edges[iter], y <= bin_edges[iter + 1])]
        dist = bp.EmpiricalDistribution(data)
        ci_low, ci_high = bp.bcanon_interval(dist, percentile_50, data, num_threads=20)
        mean = (ci_high + ci_low) / 2
        means.append(mean)
        ci_low, ci_high = bp.bcanon_interval(dist, percentile_25, data, num_threads=20)
        low = (ci_high + ci_low) / 2
        lows.append(abs(means[iter] - low))
        ci_low, ci_high = bp.bcanon_interval(dist, percentile_75, data, num_threads=20)
        high = (ci_high + ci_low) / 2
        highs.append(abs(means[iter] - high))
        histogram_1d_prediction(
            data,
            mean,
            low,
            high,
            plot_name,
            metric,
            output_path,
            iter,
            bin_edges,
            bin_range,
        )
    errors = np.array((lows, highs))
    return centers, means, errors


def means_and_errors(x, y, minimum, maximum, plot_name, metric, output_path):
    means, lows, highs = [], [], []
    range_span = maximum - minimum
    if "prediction" in plot_name and metric == "azimuth":
        bins = 18
    elif "prediction" in plot_name and metric == "zenith":
        bins = 18
    else:
        bins = 18
    _, bin_edges = np.histogram(y, bins=bins, range=(minimum, maximum))
    centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    bin_range = bin_edges.shape[0] - 1
    for iter in range(bin_range):
        print(
            f"{datetime.now()}: Finding means and error in bin {iter + 1} out of {bin_range}"
        )
        if iter == bin_range - 1:
            data = x[np.logical_and(y >= bin_edges[iter], y <= bin_edges[iter + 1])]
        else:
            data = x[np.logical_and(y > bin_edges[iter], y <= bin_edges[iter + 1])]
        mean = percentile_50(data)
        means.append(mean)
        low = percentile_25(data)
        lows.append(abs(means[iter] - low))
        high = percentile_75(data)
        highs.append(abs(means[iter] - high))
        histogram_1d_prediction(
            data,
            mean,
            low,
            high,
            plot_name,
            metric,
            output_path,
            iter,
            bin_edges,
            bin_range,
        )
    errors = np.array((lows, highs))
    return centers, means, errors


def interquartile(x, y, minimum, maximum, plot_name, metric, output_path):
    means, lows, highs, heights = [], [], [], []
    bins = 18
    _, bin_edges = np.histogram(y, bins=bins, range=(minimum, maximum))
    centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    bin_range = bin_edges.shape[0] - 1
    for iter in range(bin_range):
        print(f"{datetime.now()}: Finding IQR in bin {iter + 1} out of {bin_range}")
        if iter == bin_range - 1:
            data = x[np.logical_and(y >= bin_edges[iter], y <= bin_edges[iter + 1])]
        else:
            data = x[np.logical_and(y > bin_edges[iter], y <= bin_edges[iter + 1])]
        dist = bp.EmpiricalDistribution(data)
        if metric == "angle" or metric == "azimuth":
            mean = percentile_68(data)
            ci_low, ci_high = bp.bcanon_interval(
                dist, percentile_68, data, num_threads=20
            )
        else:
            mean = custom_iqr(data)
            ci_low, ci_high = bp.bcanon_interval(dist, custom_iqr, data, num_threads=20)
        # mean = (ci_low + ci_high) / 2
        means.append(mean)
        low = abs(means[iter] - ci_low)
        high = abs(means[iter] - ci_high)
        lows.append(low)
        highs.append(high)
        heights.append(len(data))
        # histogram_1d_errors(
        #     data,
        #     np.mean(data),
        #     np.percentile(data, 25),
        #     np.percentile(data, 75),
        #     plot_name,
        #     metric,
        #     output_path,
        #     iter,
        #     bin_edges,
        #     bin_range,
        # )
    errors = np.array((lows, highs))
    return centers, means, errors, heights


def prediction_1d_histogram(x, y, output_path, metric, run, extra_data=None, bins=100):
    no_events = len(x)
    minimum = np.around(np.amin(y), 1)
    maximum = np.around(np.amax(y), 1)
    no_clipped = np.sum(np.logical_or(x < minimum, x > maximum))
    pct_clipped = (no_clipped / no_events) * 100
    x = np.clip(x, minimum, maximum)
    fig, ax = plt.subplots(figsize=figsize)
    ax.hist(x, bins="auto", histtype="step", label="Prediction", density=True)
    ax.hist(y, bins="auto", histtype="step", label="Truth", density=True)
    if extra_data is not None:
        ax.hist(
            extra_data, bins="auto", histtype="step", label="Normalized", density=True
        )
    ax.axvline(x=np.median(y), color="green", label="Truth median")
    ylabel = "Density"
    if metric == "energy_log10":
        xlabel = "$\\log_{10}(E) \\, \\left[ E / \\textup{GeV} \\right]$"
    elif metric == "direction_x":
        xlabel = "$r_{x} \\, [\\textup{m}]$"
    elif metric == "direction_y":
        xlabel = "$r_{y} \\, [\\textup{m}]$"
    elif metric == "direction_z":
        xlabel = "$r_{z} \\, [\\textup{m}]$"
    elif metric == "azimuth":
        xlabel = "$\\phi \\, [\\textup{deg}]$"
    elif metric == "zenith":
        xlabel = "$\\theta \\, [\\textup{deg}]$"
    ax.set(
        xlabel=xlabel,
        ylabel=ylabel,
    )
    ax.set_title(
        f"{no_events:,} events, overflow $\\approx$ {np.round(pct_clipped, 1)} %",
        loc="left",
    )
    ax.legend()
    fig.suptitle(
        f"{run.suffix.split('.')[-1]} {metric} distributions",
        fontsize=16,
        x=0.1,
        ha="left",
    )
    output_file = output_path / (metric + "_" + "prediction_1d_histogram" + ".pdf")
    fig.tight_layout()
    fig.savefig(str(output_file))
    plt.close()


def resolution_1d_histogram(x, y, output_path, metric, run, bins=100):
    no_events = len(x)
    minimum = np.around(np.amin(y), 1)
    maximum = np.around(np.amax(y), 1)
    centers, means, errors, heights = interquartile(
        x, y, minimum, maximum, "resolution_1d_histogram", metric, output_path
    )
    fig, ax = plt.subplots(figsize=figsize)
    events_ax = ax.twinx()
    ax.errorbar(
        centers,
        means,
        yerr=errors,
        xerr=(centers[1] - centers[0]) / 2,
        linestyle="",
        color="tab:red",
        capsize=2,
        marker=".",
        label="90% CI",
    )
    bars = events_ax.bar(
        centers, heights, width=centers[1] - centers[0], alpha=0.5, color="grey"
    )
    for rectangle in bars:
        rectangle.set_y(0.1)
        rectangle.set_height(rectangle.get_height() - 0.1)
    if metric == "energy_log10":
        ylabel = "IQR / 1.349 $\\left[ E / \\textup{GeV} \\right]$"
    if metric == "direction_x":
        ylabel = "IQR / 1.349 $\\left[ \\textup{m} \\right]$"
    if metric == "direction_y":
        ylabel = "IQR / 1.349 $\\left[ \\textup{m} \\right]$"
    if metric == "direction_z":
        ylabel = "IQR / 1.349 $\\left[ \\textup{m} \\right]$"
    if metric == "zenith":
        ylabel = "IQR / 1.349 $\\left[ \\textup{deg} \\right]$"
    if metric == "angle":
        ylabel = "68th %ile $\\left[ \\textup{deg} \\right]$"
    if metric == "azimuth":
        ylabel = "68th %ile $\\left[ \\textup{deg} \\right]$"
    ax.set(
        xlabel="$\\log_{10}(E_{\\textup{truth}}) \\, \\left[ E / \\textup{GeV} \\right]$",
        ylabel=ylabel,
    )
    ax.set_title(
        label=(f"{no_events:,} events"),
        loc="left",
    )
    ax.legend()
    fig.suptitle(
        f"{run.suffix.split('.')[-1]} {metric} resolution",
        fontsize=16,
        x=0.1,
        ha="left",
    )
    ax.set_ylim(0)
    events_ax.set_ylim(1)
    events_ax.set_yscale("log")
    events_ax.set(ylabel="Events")
    output_file = output_path / (metric + "_" + "resolution_1d_histogram" + ".pdf")
    fig.tight_layout()
    fig.savefig(str(output_file))
    plt.close()
    return centers, means, errors


def prediction_2d_histogram(x, y, output_path, metric, run, true_energy, bins=100):
    no_events = len(x)
    if metric == "azimuth":
        minimum = 0
        maximum = 360
    elif metric == "zenith":
        minimum = 0
        maximum = 180
    elif metric == "direction_x":
        minimum = -1
        maximum = 1
    elif metric == "direction_y":
        minimum = -1
        maximum = 1
    elif metric == "direction_z":
        minimum = -1
        maximum = 1
    else:
        minimum = np.around(np.amin(y), 1)
        maximum = np.around(np.amax(y), 1)
    if args.bootstrap:
        centers, means, errors = bootstrapping(
            x, y, minimum, maximum, "prediction_2d_histogram", metric, output_path
        )
    else:
        centers, means, errors = means_and_errors(
            x, y, minimum, maximum, "prediction_2d_histogram", metric, output_path
        )
    _ = means_and_errors(
        x, true_energy, 0, 3, "prediction_binned_in_energy", metric, output_path
    )
    no_clipped_x = np.sum(np.logical_or(x < minimum, x > maximum))
    no_clipped_y = np.sum(np.logical_or(y < minimum, y > maximum))
    no_clipped = no_clipped_x + no_clipped_y
    pct_clipped = (no_clipped / no_events) * 100
    x = np.clip(x, minimum, maximum)
    y = np.clip(y, minimum, maximum)
    line = np.linspace(minimum, maximum, 100)
    if metric == "energy_log10":
        xlabel = "$\\log_{10}(E_{\\textup{prediction}}) \\, \\left[ E / \\textup{GeV} \\right]$"
        ylabel = (
            "$\\log_{10}(E_{\\textup{truth}}) \\, \\left[ E / \\textup{GeV} \\right]$"
        )
    elif metric == "direction_x":
        xlabel = "$r_{x \\textup{, prediction}} \\, [\\textup{m}]$"
        ylabel = "$r_{x \\textup{, truth}} \\, [\\textup{m}]$"
    elif metric == "direction_y":
        xlabel = "$r_{y \\textup{, prediction}} \\, [\\textup{m}]$"
        ylabel = "$r_{y \\textup{, truth}} \\, [\\textup{m}]$"
    elif metric == "direction_z":
        xlabel = "$r_{z \\textup{, prediction}} \\, [\\textup{m}]$"
        ylabel = "$r_{z \\textup{, truth}} \\, [\\textup{m}]$"
    elif metric == "azimuth":
        xlabel = "$\\phi_{\\textup{prediction}} \\, [\\textup{deg}]$"
        ylabel = "$\\phi_{\\textup{truth}} \\, [\\textup{deg}]$"
    elif metric == "zenith":
        xlabel = "$\\theta_{\\textup{prediction}} \\, [\\textup{deg}]$"
        ylabel = "$\\theta_{\\textup{truth}} \\, [\\textup{deg}]$"
    fig, ax = plt.subplots(figsize=figsize)
    print(f"{datetime.now()}: Drawing plot")
    counts, xedges, yedges, im = ax.hist2d(
        x,
        y,
        bins=bins,
        range=[[minimum, maximum], [minimum, maximum]],
        cmin=1,
        cmap="viridis",
    )
    ax.plot(line, line, marker="", color="tab:red", linestyle="--")
    ax.errorbar(
        means,
        centers,
        xerr=errors,
        yerr=(centers[1] - centers[0]) / 2,
        linestyle="",
        color="tab:red",
        capsize=2,
        marker=".",
        label="IQR",
    )
    ax.legend()
    ax.set(xlabel=xlabel, ylabel=ylabel)
    ax.set_title(
        label=(
            f"{no_events:,} events, overflow $\\approx$ {np.round(pct_clipped, 1)} %"
        ),
        loc="left",
    )
    fig.suptitle(
        f"{run.suffix.split('.')[-1]} {metric} predictions",
        fontsize=16,
        x=0.1,
        ha="left",
    )
    cbar = fig.colorbar(im, ax=ax)
    cbar.set_label("Events")
    # ax.text(
    #     0.5,
    #     0.2,
    #     "Test",
    #     color="white",
    #     transform=ax.transAxes,
    #     bbox=dict(facecolor="black", edgecolor="black", boxstyle="round,pad=1"),
    # )
    output_file = output_path / (metric + "_" + "prediction_2d_histogram" + ".pdf")
    fig.tight_layout()
    fig.savefig(str(output_file))
    plt.close()


def error_2d_histogram(x, y, output_path, metric, run, bins=100):
    no_events = len(x)
    x_minimum = np.around(np.amin(y), 1)
    x_maximum = np.around(np.amax(y), 1)
    y_minimum = np.percentile(x, 1)
    y_maximum = np.percentile(x, 99)
    # y_minimum = x.min()
    # y_maximum = x.max()
    if metric == "zenith":
        bins = [15, 8]
        y_minimum = -1
        y_maximum = 1
    if args.bootstrap:
        centers, means, errors = bootstrapping(
            x, y, x_minimum, x_maximum, "error_2d_histogram", metric, output_path
        )
    else:
        centers, means, errors = means_and_errors(
            x, y, x_minimum, x_maximum, "error_2d_histogram", metric, output_path
        )
    no_clipped_x = np.sum(np.logical_or(x < y_minimum, x > y_maximum))
    no_clipped_y = np.sum(np.logical_or(y < x_minimum, y > x_maximum))
    no_clipped = no_clipped_x + no_clipped_y
    pct_clipped = (no_clipped / no_events) * 100
    x = np.clip(x, y_minimum, y_maximum)
    y = np.clip(y, x_minimum, x_maximum)
    xlabel = "$\\log_{10}(E_{\\textup{true}}) \\, \\left[ E / \\textup{GeV} \\right]$"
    if metric == "energy_log10":
        ylabel = "$\\log_{10}(E_{\\textup{truth}}) - \\log_{10}(E_{\\textup{prediction}}) \\, \\left[ E / \\textup{GeV} \\right]$"
    elif metric == "direction_x":
        ylabel = (
            "$r_{x \\textup{, truth}} - r_{x \\textup{, prediction}} \\, [\\textup{m}]$"
        )
    elif metric == "direction_y":
        ylabel = (
            "$r_{y \\textup{, truth}} - r_{y \\textup{, prediction}} \\, [\\textup{m}]$"
        )
    elif metric == "direction_z":
        ylabel = (
            "$r_{z \\textup{, truth}} - r_{z \\textup{, prediction}} \\, [\\textup{m}]$"
        )
    elif metric == "azimuth":
        ylabel = "$\\phi_{\\textup{truth}} - \\phi_{\\textup{prediction}} \\, [\\textup{deg}]$"
    elif metric == "zenith":
        ylabel = "$\\cos{\\left( \\theta_{\\textup{zenith,reco}} \\right)} - \\cos{\\left( \\theta_{\\textup{zenith,true}} \\right)}$"
    elif metric == "angle":
        ylabel = "$\\arccos\\left(\\frac{\\bm{r}_{\\textup{truth}} \\cdot \\bm{r}_{\\textup{prediction}}}{\\left| \\bm{r}_{\\textup{truth}} \\right| \\left| \\bm{r}_{\\textup{prediction}} \\right|} \\right) \\, [\\textup{deg}]$"
    fig, ax = plt.subplots(figsize=figsize)
    print(f"{datetime.now()}: Drawing plot")
    counts, xedges, yedges, im = ax.hist2d(
        y,
        x,
        bins=bins,
        range=[[x_minimum, x_maximum], [y_minimum, y_maximum]],
        cmin=1,
        cmap="Oranges",
    )
    ax.grid(b=True, which="both", axis="y")
    ax.axhline(y=0, marker="", color="grey", linewidth=2)
    ax.plot(
        centers,
        means,
        linestyle="-",
        linewidth=2,
        color="tab:red",
        marker="",
        label="Median",
    )
    ax.plot(
        centers,
        np.array(means) - errors[0, :],
        linestyle="dotted",
        linewidth=2,
        color="tab:red",
        marker="",
        label="$1 \\sigma$",
    )
    ax.plot(
        centers,
        np.array(means) + errors[1, :],
        linestyle="dotted",
        linewidth=2,
        color="tab:red",
        marker="",
        # label="$1 \\sigma$",
    )
    ax.legend()
    ax.set(
        xlabel=xlabel,
        ylabel=ylabel,
    )
    ax.set_title(
        label=(
            f"{no_events:,} events, overflow $\\approx$ {np.round(pct_clipped, 1)} %"
        ),
        loc="left",
    )
    fig.suptitle(
        f"{run.suffix.split('.')[-1]} {metric} errors",
        fontsize=16,
        x=0.1,
        ha="left",
    )
    cbar = fig.colorbar(im, ax=ax)
    cbar.set_label("Events")
    # ax.text(
    #     0.5,
    #     0.2,
    #     "Test",
    #     color="white",
    #     transform=ax.transAxes,
    #     bbox=dict(facecolor="black", edgecolor="black", boxstyle="round,pad=1"),
    # )
    output_file = output_path / (metric + "_" + "error_2d_histogram" + ".pdf")
    fig.tight_layout()
    fig.savefig(str(output_file))
    plt.close()


def loss_plot(run, loss, output_path):
    fig, ax = plt.subplots(figsize=figsize)
    ax.plot(loss["epoch"].values, loss["loss"].values, label="Training loss", marker="")
    ax.plot(
        loss["epoch"].values,
        loss["val_loss"].values,
        label="Validation loss",
        marker="",
    )
    ax.set(xlabel="Epoch", ylabel="Loss")
    ax.legend()
    fig.suptitle(
        f"{run.suffix.split('.')[-1]} loss",
        fontsize=16,
        x=0.1,
        ha="left",
    )
    output_file = output_path / ("loss.pdf")
    fig.tight_layout()
    fig.savefig(str(output_file))
    plt.close()


def lr_plot(run, lr, output_path):
    x = np.arange(0, len(lr))
    fig, ax = plt.subplots(figsize=figsize)
    ax.plot(x, lr, marker="")
    ax.set(xlabel="Epoch", ylabel="Learning rate")
    ax.set_yscale("log")
    fig.suptitle(
        f"{run.suffix.split('.')[-1]} learning rate",
        fontsize=16,
        x=0.1,
        ha="left",
    )
    output_file = output_path / ("lr.pdf")
    fig.tight_layout()
    fig.savefig(str(output_file))
    plt.close()


def perm_imd_plot(run, perm_imp, output_path):
    baseline = perm_imp[1][0]
    del perm_imp[1][0]
    del perm_imp[0][0]
    fig, ax = plt.subplots(figsize=figsize)
    ax.barh(y=perm_imp[0], width=perm_imp[1])
    ax.axvline(x=baseline, color="red", linestyle="dashed", label="Baseline")
    ax.set(xlabel="Permutation importance")
    ax.legend()
    fig.suptitle(
        f"{run.suffix.split('.')[-1]} permutation importance",
        fontsize=16,
        x=0.1,
        ha="left",
    )
    output_file = output_path / ("perm_imp.pdf")
    fig.tight_layout()
    fig.savefig(str(output_file))
    plt.close()


parser = argparse.ArgumentParser(description="Pretty plots for CubeFlow")
parser.add_argument("--bootstrap", dest="bootstrap", action="store_true")
parser.add_argument("--no-bootstrap", dest="bootstrap", action="store_false")
parser.add_argument("--limit", dest="limit")
parser.add_argument("--run", dest="run")
parser.add_argument("--only_val", dest="only_val")
parser.set_defaults(bootstrap=False)
args = parser.parse_args()

figsize = (0.49 * 6.2012, 0.49 * 6.2012)

limit = args.limit
run_name = args.run
only_val = bool(int(args.only_val))

run = get_run_path(run_name)
params_file = run / "params.json"
log_file = run / "log.csv"
lr_history = run / "lr_history.pkl"
summary = run / "summary.txt"
perm_imp_file = run / "perm_imp.pkl"
with open(str(params_file), "rb") as f:
    params = json.load(f)
log = pd.read_csv(str(log_file))
with open(str(lr_history), "rb") as f:
    lr = pickle.load(f)
with open(str(perm_imp_file), "rb") as f:
    perm_imp = pickle.load(f)
output_path = Path().home() / "plots" / "runs" / run.name
output_path.mkdir(exist_ok=True, parents=True)
copyfile(str(params_file), str(output_path / "params.json"))
copyfile(str(log_file), str(output_path / "log.csv"))
copyfile(str(summary), str(output_path / "summary.txt"))
prediction_sets = params["predict_datasets"]
metrics = [params["type"]]
prediction_db_paths = get_prediction_db_paths(prediction_sets)
plots = [
    "prediction_2d_histogram",
    "prediction_1d_histogram",
    "error_2d_histogram",
    "resolution_1d_histogram",
]

for prediction_db_path in prediction_db_paths:
    sets_path = prediction_db_path.parent.parent / "meta" / "sets.pkl"
    with open(str(sets_path), "rb") as f:
        sets = pickle.load(f)
    if only_val:
        event_nos = sets["val"]
    else:
        event_nos = sets["test"]
    loss_plot(run, log, output_path)
    lr_plot(run, lr, output_path)
    perm_imd_plot(run, perm_imp, output_path)
    print(f"{datetime.now()}: Plotting set {str(prediction_db_path).split('/')[-3]}")
    for metric in metrics:
        print(f"{datetime.now()}: Plotting metric {metric}")
        if metric == "energy":
            energy_plotting(
                run,
                limit,
                prediction_db_path,
                plots,
                output_path,
                metric,
                event_nos,
            )
        elif metric == "direction":
            direction_plotting(
                run, limit, prediction_db_path, plots, output_path, metric, event_nos
            )
        elif metric == "angle":
            angle_plotting(
                run, limit, prediction_db_path, plots, output_path, metric, event_nos
            )
        elif metric == "zenith":
            zenith_plotting(
                run, limit, prediction_db_path, plots, output_path, metric, event_nos
            )
        elif metric == "azimuth":
            azimuth_plotting(
                run, limit, prediction_db_path, plots, output_path, metric, event_nos
            )

print(f"{datetime.now()}: Done")
