from pathlib import Path

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import MaxNLocator

mpl.rcParams["axes.unicode_minus"] = False

output_file = Path().home() / "Developer" / "thesis" / "images" / "data" / "mc_pi.pgf"
# output_file = Path().home() / "mc_pi.pdf"
numbers = [int(1e2), int(5e3)]
fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(6.2012, 2))

for i, number in enumerate(numbers):
    xy = np.random.uniform(-1, 1, 2 * number).reshape((2, number))
    in_marker = xy[0] ** 2 + xy[1] ** 2 <= 1
    pi = np.sum(in_marker) / number * 4
    pi_error = abs(round(((pi - np.pi) / np.pi) * 100, 3))
    print(f"π ~ {round(pi, 3)}")
    print(f"π error ~ {pi_error}")
    in_xy = xy[:, in_marker]
    out_xy = xy[:, ~in_marker]
    axes[i].scatter(*in_xy, c="b", s=0.8)
    axes[i].scatter(*out_xy, c="r", s=0.8)
    axes[i].set_aspect("equal")
    axes[i].set(title=f"{number} samples, error $ \\approx $ {pi_error} \\%")
    axes[i].yaxis.set_major_locator(MaxNLocator(integer=True))
fig.savefig(output_file)
