import pickle
import sqlite3
from datetime import datetime
from pathlib import Path

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

mpl.rcParams["axes.unicode_minus"] = False


def transform_data(transformers_path, data):
    with open(str(transformers_path), "rb") as f:
        transformers = pickle.load(f)
    for column in data.columns:
        if column in transformers["truth"]:
            data[column] = transformers["truth"][column].inverse_transform(
                data[column].values.reshape(-1, 1)
            )
    return data


metric = "zenith"
output_type = "pdf"
file_name = f"{metric}_resolution_upgrade"
title = f"{metric.capitalize()} performance"

if output_type == "pdf":
    output_file = Path().home() / "plots" / f"{file_name}.pdf"
else:
    output_file = Path().home() / "plots" / f"{file_name}.pgf"

dataset_name = "dev_upgrade_numu_cc_train_000"
truth_db_path = (
    Path().home()
    / "work"
    / "erda_datasets"
    / dataset_name
    / "data"
    / f"{dataset_name}.db"
)
truth_db_uri = f"file:{truth_db_path}?mode=ro"
transformers_path = (
    Path().home()
    / "work"
    / "erda_datasets"
    / dataset_name
    / "meta"
    / "transformers.pkl"
)
sets_path = (
    Path().home() / "work" / "erda_datasets" / dataset_name / "meta" / "sets.pkl"
)
with open(sets_path, "rb") as f:
    sets = pickle.load(f)

stringified_events_nos = ", ".join([str(event_no) for event_no in sets["test"]])
query = f"select energy_log10 from truth"
with sqlite3.connect(truth_db_uri, uri=True) as con:
    energy = pd.read_sql(query, con)
energy = transform_data(transformers_path, energy)
energy = energy["energy_log10"].values.flatten()
hist, bin_edges = np.histogram(energy, bins=18)
centers = (bin_edges[:-1] + bin_edges[1:]) / 2
width = centers[1] - centers[0]

print(f"{datetime.now()}: Calculating performances on selected runs")

runs_root = Path().home() / "plots" / "runs"

runs = [
    "dev_upgrade_numu_cc_train_000.zenith.model_2.SRTInIcePulses.Adam.logcosh.weight_uniform.5.256.200.2012051559",
]
labels = [
    "CubeFlow",
]

ncols = 1
nrows = 1
figsize = (1 * 6.2012, 1 * 3)

fig, ax = plt.subplots(
    ncols=ncols,
    nrows=nrows,
    figsize=figsize,
)
events_ax = ax.twinx()
events_ax.bar(
    centers, hist, width, bottom=1, alpha=0.2, color="grey", label="Training events"
)
events_ax.set_yscale("log")
events_ax.set_ylabel("Events")
performances = []
for run, label in zip(runs, labels):
    performance_file = runs_root / run / f"{metric}_performance.csv"
    performance = pd.read_csv(performance_file)
    performances.append(performance["means"].values.flatten())
    ax.errorbar(
        performance["centers"].values,
        performance["means"].values,
        yerr=np.array((performance["lows"].values, performance["highs"].values)),
        xerr=(performance["centers"].values[1] - performance["centers"].values[0]) / 2,
        linestyle="",
        marker="",
        linewidth=1.5,
        capsize=1,
        label=label,
        alpha=0.9,
    )

lines, labels = events_ax.get_legend_handles_labels()
lines2, labels2 = ax.get_legend_handles_labels()
xlabel = "$\\log_{10}(E_{\\textup{true}}) \\, \\left[ E / \\textup{GeV} \\right]$"
if metric == "energy":
    ylabel = "IQR / 1.349 $\\left[ E / \\textup{GeV} \\right]$"
if (
    metric == "direction"
    or metric == "angle"
    or metric == "zenith"
    or metric == "azimuth"
):
    if metric == "angle" or metric == "azimuth":
        ylabel = "68th %ile $\\left[ \\textup{deg} \\right]$"
    else:
        ylabel = "IQR / 1.349 $\\left[ \\textup{deg} \\right]$"
ax.set(xlabel=xlabel)
ax.set(ylabel=ylabel)
ax.set_title(
    title,
    fontsize=12,
    loc="left",
)
ax.legend(lines + lines2, labels + labels2)
ax.grid(b=True, which="both", axis="both", linestyle="dotted", linewidth=0.5, alpha=0.5)
fig.tight_layout()
fig.savefig(str(output_file))
plt.close()
