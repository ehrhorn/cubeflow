from datetime import datetime
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

metric = "energy"
output_type = "pdf"
file_name = f"{metric}_performance_upgrade"
title = f"{metric.capitalize()} performance, Upgrade"

if output_type == "pdf":
    output_file = Path().home() / f"{file_name}.pdf"
else:
    output_file = (
        Path().home()
        / "Developer"
        / "thesis"
        / "images"
        / "design"
        / f"{file_name}.pgf"
    )

print(f"{datetime.now()}: Calculating performances on selected runs")

runs_root = Path().home() / "work" / "plots" / "runs"
runs = [
    "dev_upgrade_train_step4_002.energy.model_1.SRTInIcePulses.Adam.logcosh.None.5.256.200.2011121226"
]
labels = [
    "1",
]

fig, ax = plt.subplots(figsize=(6.2012, 3))
for run, label in zip(runs, labels):
    performance_file = runs_root / run / f"{metric}_performance.csv"
    performance = pd.read_csv(performance_file)
    ax.errorbar(
        performance["centers"].values,
        performance["means"].values,
        yerr=np.array((performance["lows"].values, performance["highs"].values)),
        # xerr=(performance["centers"].values[1] - performance["centers"].values[0]) / 2,
        # linestyle="",
        marker="",
        linewidth=1.5,
        capsize=1,
        label=label,
        alpha=0.9,
    )
xlabel = "$\\log_{10}(E_{\\textup{true}}) \\, \\left[ E / \\textup{GeV} \\right]$"
if metric == "energy":
    title = "Log energy"
    ylabel = "IQR / 1.349 $\\left[ E / \\textup{GeV} \\right]$"
if (
    metric == "direction"
    or metric == "angle"
    or metric == "zenith"
    or metric == "azimuth"
):
    title = f"{metric.capitalize()} performance"
    if metric == "angle" or metric == "azimuth":
        ylabel = "68th %ile $\\left[ \\textup{deg} \\right]$"
    else:
        ylabel = "IQR / 1.349 $\\left[ \\textup{deg} \\right]$"
ax.set(xlabel=xlabel, ylabel=ylabel)
# ax.set_title(title, loc="left")
ax.legend()
ax.set_title(
    title,
    fontsize=12,
    loc="left",
)
ax.grid(b=True, which="both", axis="both", linestyle="dotted", linewidth=0.5, alpha=0.5)
fig.tight_layout()
fig.savefig(str(output_file))
plt.close()
