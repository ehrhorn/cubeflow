#!/bin/bash
FILES=$HOME/work/cubeflow/params/*.json
for f in $FILES
do
  echo "Processing $f file..."
  nohup python -u $HOME/work/cubeflow/training.py -p $f
done