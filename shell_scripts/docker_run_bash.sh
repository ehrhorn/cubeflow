#!/bin/bash
docker run -v /home/ehrhorn/data:/home/icecube/data \
	-v /home/ehrhorn/work:/home/icecube/work \
	-v /home/ehrhorn/raw_files:/home/icecube/raw_files \
	-it ehrhorn/cubedev:0.2