import argparse
import sys
from datetime import datetime
from pathlib import Path

import cubeflow.modules.helper_functions as helper_functions
import numpy as np
from cubeflow.modules.data_generator import DataGenerator
from cubeflow.modules.helper_functions import print_message_with_time


def create_numpy_file(params, train, predict_dataset_name=None, batch_size=1000):
    if params["type"] == "energy":
        params["targets"] = ["energy_log10"]
    elif params["type"] == "time":
        params["targets"] = ["time"]
    elif params["type"] == "direction":
        params["targets"] = ["direction_x", "direction_y", "direction_z"]
    elif params["type"] == "vertex":
        params["targets"] = ["position_x", "position_y", "position_z"]
    elif params["type"] == "classification":
        params["targets"] = ["pid"]
    elif params["type"] == "zenith":
        params["targets"] = ["zenith"]
    elif params["type"] == "angle":
        params["targets"] = ["azimuth", "zenith"]
    if predict_dataset_name is None:
        dataset_name = params["train_dataset"]
    else:
        dataset_name = predict_dataset_name
    paths = helper_functions.get_dataset_paths(dataset_name)
    sets = helper_functions.open_pickle_file(paths["sets"])
    numpy_datasets = {}
    out_dir = Path().home().joinpath("data").joinpath("numpy").joinpath(dataset_name)
    out_file = out_dir.joinpath(params["type"] + ".pkl")
    if out_file.is_file():
        now = datetime.now().strftime("%H:%M:%S")
        print("{}: NumPy dataset exists".format(now))
        return out_file
    else:
        out_dir.mkdir(parents=True, exist_ok=True)
        for set_name, events in sets.items():
            now = datetime.now().strftime("%H:%M:%S")
            print(
                "{}: Beginning NumPy dataset creation for set {}".format(now, set_name)
            )
            generator = DataGenerator(
                params,
                paths,
                events,
                batch_size,
                to_numpy=True,
                shuffle=False,
            )
            X = np.zeros((len(events), params["max_length"], len(params["features"])))
            y = np.zeros((len(events), len(params["targets"])))
            weights = np.zeros(len(events))
            for i, item in enumerate(generator):
                if i % 100 == 0:
                    now = datetime.now().strftime("%H:%M:%S")
                    print("{}: Creating batch {} / {}".format(now, i, len(generator)))
                insert_point_start = i * batch_size
                insert_point_end = insert_point_start + batch_size
                X[insert_point_start:insert_point_end, :, :] = item[0]
                y[insert_point_start:insert_point_end, :] = item[1]
                weights[insert_point_start:insert_point_end] = item[2]
            numpy_datasets[set_name] = {}
            numpy_datasets[set_name]["X"] = X
            numpy_datasets[set_name]["y"] = y
            numpy_datasets[set_name]["weights"] = weights
        helper_functions.save_pickle_file(out_file, numpy_datasets)
        now = datetime.now().strftime("%H:%M:%S")
        print("{}: Created NumPy dataset".format(now))
    return out_file
