import sqlite3
from pathlib import Path

import pandas as pd

dataset_name = "dev_upgrade_train_step4_002"
predictions_path = (
    Path().home()
    / "work"
    / "erda_datasets"
    / dataset_name
    / "predictions"
    / "predictions.db"
)
predictions_uri = f"file:{predictions_path}?mode=ro"

query = "select energy_log10 from truth"
with sqlite3.connect(predictions_uri, uri=True) as con:
    energy = pd.read_sql(query, con)

print(energy["energy_log10"].mean())
