from datetime import datetime
import json
import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"

import cubeflow.modules.helper_functions as helper_functions
from cubeflow.modules.data_generator import DataGenerator as d1
from cubeflow.modules.data_generator_2 import DataGenerator as d2

params_path = "params/params_1.json"
params = helper_functions.open_json_file(params_path)
run_name = (
    params["train_dataset"]
    + "."
    + params["type"]
    + "."
    + params["model"]
    + "."
    + params["cleaning"]
    + "."
    + params["optimizer"]
    + "."
    + params["loss"]
    + "."
    + str(params["weight_name"])
    + "."
    + str(params["patience"])
    + "."
    + str(params["train_batch_size"])
    + "."
    + str(params["max_length"])
    + "."
    + datetime.now().strftime("%y%m%d%H%M")
)
paths = helper_functions.get_dataset_paths("dev_muongun_000")
run_paths = helper_functions.create_run_paths(run_name)

params["targets"] = ["energy_log10"]

script_start = datetime.now()

sets = helper_functions.open_pickle_file(paths["sets"])

set1 = list(sets["train"].keys())
set2 = sets["train"]

params["features"] = ["x", "y", "z", "time", "charge_log10"]

train_generator1 = d1(
    params,
    paths,
    set1,
    params["train_batch_size"],
    to_numpy=False,
    shuffle=True,
)
train_generator2 = d2(
    params,
    paths,
    set2,
    params["train_batch_size"],
    to_numpy=False,
    shuffle=True,
)

events_per_second = []
n = 100

for generator in [train_generator1, train_generator2]:
    start = datetime.now()
    for i, entry in enumerate(generator):
        if i == n:
            end = datetime.now()
            break
    delta = (end - start).total_seconds()
    events_per_second.append(n * params["train_batch_size"] / delta)

script_end = datetime.now()
script_time = (script_end - script_start).total_seconds()

for i, entry in enumerate(events_per_second):
    print("Speed was {} events per second for generator {}".format(int(entry), i))
print("Handled {} events".format(n * params["train_batch_size"]))
print("Script took {} seconds to complete".format(round(script_time, 2)))
